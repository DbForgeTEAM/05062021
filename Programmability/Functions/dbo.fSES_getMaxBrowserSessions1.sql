﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fSES_getMaxBrowserSessions1]
(
)
RETURNS INT
WITH ENCRYPTION
AS
BEGIN

  DECLARE @sPassPhrase  VARCHAR(20)   = 'u0vm2sb5ni'
  DECLARE @nRetVal      INT           = 0;
  DECLARE @sSysParamEnc VARCHAR(100)  = '';
  DECLARE @sSysParamBin VARBINARY(49) = 0x00; -- Im Systemparemter wird der Binärwert in der Form 0xFF... dargestellt => max. 49 Binärstellen

  SELECT @sSysParamEnc = SypValue
  FROM  SYP.tParameter tp
  WHERE tp.SypName = 'DLG_SES_MAXSESSCOUNT';

  SET @sSysParamBin = TRY_CONVERT(VARBINARY(49), @sSysParamEnc, 1);

  SET @nRetVal = TRY_CAST( TRY_CAST( DECRYPTBYPASSPHRASE(@sPassPhrase, @sSysParamBin) AS VARCHAR(19) ) AS INT );

  -- Falls es bei der Ermittlung zu Problemen kommt, 1 zurückgeben, damit zumindest ein Login möglich ist
  SET @nRetVal = ISNULL( @nRetVal, 1 );

  RETURN @nRetVal;

END
GO