﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SES_startBrowserWindow]
(
  @fsUser                 AS VARCHAR(20),
  -- BrowserSession-Werte
  @fsBrowserSessionId     AS VARCHAR(26),
  @fsBrowser              AS VARCHAR(100),
  @fsOperatingSystem      AS VARCHAR(100),
  @fsIPAddress            AS VARCHAR(45),
  @fsIPProtocol           AS VARCHAR(4),
  @fsHostname             AS VARCHAR(255),
  @fsUserAgent            AS VARCHAR(8000),
  @fsServerIPAddress      AS VARCHAR(45),
  @fsServerProtocol       AS VARCHAR(100),
  -- BrowserWindow-Werte
  @fsWindowSessionId      AS VARCHAR(16),
  @fsUserName             AS VARCHAR(20),
  @fnProdLocNbr           AS SMALLINT,
  @fsLang                 AS VARCHAR(2),
  @fsDialog               AS VARCHAR(100),
  @fsProjectVersion       AS VARCHAR(20),
  @fsHeartbeatType        AS VARCHAR(10)
)
WITH ENCRYPTION
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @nDoLogging            SMALLINT      = 800;

  DECLARE @nRetVal               INT           = 0;
  DECLARE @sMsgTxt               VARCHAR(MAX);
  DECLARE @sFctName              VARCHAR(50)   = OBJECT_SCHEMA_NAME(@@procid) + '.' +
                                                 OBJECT_NAME(@@procid);
  DECLARE @nFlgI18nInit          TINYINT       = 0;
  DECLARE @sSchema               VARCHAR(3);
  DECLARE @sFunktion             VARCHAR(50);
  DECLARE @sRetValMode           VARCHAR(10)   = 'GENERATE';
  DECLARE @sRetValJsonTxt        NVARCHAR(MAX) = NULL;

  DECLARE @nRetValInt            INT           = 0;
  DECLARE @nRetValInt_BS         INT           = 0;
  DECLARE @nRetValInt_BW         INT           = 0;

  DECLARE @nCntDevPermUser       INT           = 0;
  DECLARE @nCntDevPermGroup      INT           = 0;
  DECLARE @nFlgDeveloperAccess   TINYINT       = 0;
  DECLARE @nOpenSessionCheckCnt  INT           = 2; -- Wie oft wird geprüft, ob noch eine Sitzung frei ist
  DECLARE @nOpenSessionCnt       INT           = 0;
  DECLARE @nRecCntBrowserSession INT           = 0;
  DECLARE @nBrowserSessionRecId  BIGINT        = 0;
  DECLARE @nRecCntBrowserWindow  INT           = 0;
  DECLARE @nProdLocNbrInUse      INT           = 0;
  DECLARE @nMaxBrowserSessionCnt INT           = 0;

  -- Daten zu anderer Browsersitzung (falls Arbeitsplatz bereits in Nutzung)
  DECLARE @sOtherBrowserSession  VARCHAR(26)   = '';
  DECLARE @vOtherSessUserName    SQL_VARIANT   = '';
  DECLARE @vOtherSessHostName    SQL_VARIANT   = '';
  DECLARE @vOtherSessIpAddress   SQL_VARIANT   = '';

  BEGIN TRY

    EXEC @nDoLogging = LOG.getLogLevel @sLogFct = @sFctName, @nLogLev = @nDoLogging, @nFlgI18nInit = @nFlgI18nInit OUTPUT;

    IF ( @nDoLogging >= 800 )
    BEGIN
      SET @sMsgTxt = '--> BrowserSessionId[' + GEN.fToString ( @fsBrowserSessionId ) + 
                     '] Browser['            + GEN.fToString ( @fsBrowser          ) + 
                     '] OperatingSystem['    + GEN.fToString ( @fsOperatingSystem  ) + 
                     '] IPAddress['          + GEN.fToString ( @fsIPAddress        ) + 
                     '] IPProtocol['         + GEN.fToString ( @fsIPProtocol       ) + 
                     '] Hostname['           + GEN.fToString ( @fsHostname         ) + 
                     '] UserAgent['          + GEN.fToString ( @fsUserAgent        ) + 
                     '] ServerIPAddress['    + GEN.fToString ( @fsServerIPAddress  ) + 
                     '] ServerProtocol['     + GEN.fToString ( @fsServerProtocol   ) + 
                     '] WindowSessionId['    + GEN.fToString ( @fsWindowSessionId  ) + 
                     '] UserName['           + GEN.fToString ( @fsUserName         ) + 
                     '] ProdLocNbr['         + GEN.fToString ( @fnProdLocNbr       ) + 
                     '] Lang['               + GEN.fToString ( @fsLang             ) + 
                     '] Dialog['             + GEN.fToString ( @fsDialog           ) + 
                     '] ProjectVersion['     + GEN.fToString ( @fsProjectVersion   ) + 
                     '] HeartbeatType['      + GEN.fToString ( @fsHeartbeatType    ) + 
                     '] User['               + GEN.fToString ( @fsUser             ) + 
                     ']';                  
      EXEC LOG.info @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
    END

    IF ( ( @nFlgI18nInit = 0 ) OR ( @fsUser = 'UBH_INIT' ) ) BEGIN
      SET @sSchema   = SUBSTRING(@sFctName,1,3);
      SET @sFunktion = SUBSTRING(@sFctName,5,45);

      EXEC DLG.I18_createReturnValue @sFctName,      -- Positive Rückmeldungen
                                                 1, 'Sitzung gestartet.',
                                                     -- Negative Rückmeldungen
                                                -1, 'Sitzung nicht angegeben.',
                                                -2, 'Sitzung existiert bereits.',
                                                -3, 'Maximales Limit von [$$MaxBrowserSessionCnt$$] Sitzungen erreicht. Bitte schließen Sie Sitzungen an einem anderen Arbeitsplatz.',
                                                -4, 'Der gewünschte Arbeitsplatz [$$ProdLocNbr|ProdLocNbrDlg$$] wird bereits von einer anderen Sitzung verwendet. (Benutzername[$$OtherSessUserName$$], Rechner[$$OtherSessHostName$$], IP-Adresse[$$OtherSessIpAddress$$])',
                                                
                                               -10, 'Sitzung konnte nicht gestartet werden.';

      EXEC @nDoLogging = LOG.getLogLevel @sLogFct = @sFctName, @nFlgI18nInit = @nFlgI18nInit OUTPUT;
      IF ( @fsUser = 'UBH_INIT' ) BEGIN
        SET @sMsgTxt = '|~~ initialized.';
        EXEC LOG.test @fsUser = @fsUser, @fsLogFunction = @sFctName, @fsLogText = @sMsgTxt
        RETURN @nFlgI18nInit;
      END
    END

    --------------------------------------------------------------------------------------------------------------------------------
    -- Auslesen der Anzahl max. Sessions direkt anstatt über leicht austauschbare Funktion  -- DLG.fSES_getMaxBrowserSessions();  --
    --------------------------------------------------------------------------------------------------------------------------------
    DECLARE @sPassPhrase  VARCHAR(20)   = 'u0vm2sb5ni'
    DECLARE @sSysParamEnc VARCHAR(100)  = '';
    DECLARE @sSysParamBin VARBINARY(49) = 0x00; -- Im Systemparemter wird der Binärwert in der Form 0xFF... dargestellt => max. 49 Binärstellen

    SELECT @sSysParamEnc = SypValue
    FROM  SYP.tParameter tp
    WHERE tp.SypName = 'DLG_SES_MAXSESSCOUNT';

    SET @sSysParamBin = TRY_CONVERT(VARBINARY(49), @sSysParamEnc, 1);

    SET @nMaxBrowserSessionCnt = TRY_CAST( TRY_CAST( DECRYPTBYPASSPHRASE( @sPassPhrase, @sSysParamBin ) AS VARCHAR(19) ) AS INT );

    -- Falls es bei der Ermittlung zu Problemen kommt, 1 zurückgeben, damit zumindest ein Login möglich ist
   SET @nMaxBrowserSessionCnt = ISNULL( @nMaxBrowserSessionCnt, 1 );
    --------------------------------------------------------------------------------------------------------------------------------

    SELECT    @nCntDevPermUser = COUNT(*)
    FROM      DLG.tUSR_GroupUserPermRel gup
    WHERE     gup.UserGroup  = ''
    AND       gup.UserName   = @fsUserName
    AND       gup.Perm       = 'Developer_Access';

    SELECT    @nCntDevPermGroup = COUNT(*)
    FROM      DLG.tUSR_GroupUserPermRel gup
    LEFT JOIN DLG.tUSR_GroupUserRel     gu  ON gu.UserGroup = gup.UserGroup 
    WHERE     gu.UserName  = @fsUserName
    AND       gup.Perm     = 'Developer_Access';

    -- Hat der Benutzer oder einer Gruppe, in der der Benutzer Mitglied ist, Entwicklerzugriff?
    SET @nFlgDeveloperAccess = SIGN( @nCntDevPermUser + @nCntDevPermGroup );

    WHILE ( @nOpenSessionCheckCnt > 0 ) BEGIN

      SET @nOpenSessionCheckCnt = @nOpenSessionCheckCnt - 1;

      SELECT @nOpenSessionCnt      = COUNT(DISTINCT BrowserSessionId)
           , @nProdLocNbrInUse     = SUM(CASE WHEN @fnProdLocNbr > 0 AND ProdLocNbr = @fnProdLocNbr THEN 1
                                              ELSE                                                       0
                                         END)
           , @sOtherBrowserSession = MAX(CASE WHEN @fnProdLocNbr > 0 AND ProdLocNbr = @fnProdLocNbr THEN BrowserSessionId
                                              ELSE                                                       ''
                                         END)
      FROM DLG.tSES_BrowserWindow
      WHERE SessionStatus < 90
      AND   FlgDeveloperAccess = 0
      AND   BrowserSessionId <> @fsBrowserSessionId

      SELECT @nRecCntBrowserSession = COUNT(*)
           , @nBrowserSessionRecId  = ISNULL(MAX(bs.RecId), -1)
      FROM DLG.tSES_BrowserSession bs
      WHERE bs.BrowserSessionId = @fsBrowserSessionId
      AND   bs.SessionStatus    < 90;

      SELECT @nRecCntBrowserWindow = COUNT(*)
      FROM DLG.tSES_BrowserWindow bw
      WHERE bw.BrowserSessionId = @fsBrowserSessionId
      AND   bw.WindowSessionId  = @fsWindowSessionId;

      -- Falls keine freie Sitzung vorhanden, prüfen, ob wir eine freigeben können und nochmal die Anzahl der Sitzungen abfragen
      IF ( @nRecCntBrowserSession = 0 AND @nFlgDeveloperAccess = 0 AND ( @nOpenSessionCnt + 1 ) > @nMaxBrowserSessionCnt ) BEGIN
        EXEC @nRetValInt = DLG.SES_closeOldSessions @fsUser = @fsUser;
      END
      -- Falls Arbeitsplatz schon in Benutzung, prüfen, ob wir eine freigeben können und nochmal die Anzahl der Sitzungen abfragen
      ELSE IF ( @nFlgDeveloperAccess = 0 AND @nProdLocNbrInUse > 0 ) BEGIN
        EXEC @nRetValInt = DLG.SES_closeOldSessions @fsUser = @fsUser;
      END 
      ELSE BEGIN
        BREAK;
      END;
    END;

    -- Wenn immer noch kein Arbeitsplatz frei ist, dann auslesen, wer den anderen belegt
    IF ( @nFlgDeveloperAccess = 0 AND @nProdLocNbrInUse > 0 ) BEGIN
      SELECT TOP (1) 
             @vOtherSessUserName  = tsbw.UserName
           , @vOtherSessHostName  = tsbs.Hostname
           , @vOtherSessIpAddress = tsbs.IPAddress
      FROM DLG.tSES_BrowserSession tsbs
      JOIN DLG.tSES_BrowserWindow  tsbw ON tsbw.BrowserSessionId = tsbs.BrowserSessionId
      WHERE tsbs.BrowserSessionId   = @sOtherBrowserSession
      AND   tsbw.SessionStatus      < 90
      AND   tsbw.FlgDeveloperAccess =  0
      AND   tsbw.ProdLocNbr         = @fnProdLocNbr
      ORDER BY tsbw.HeartbeatTms DESC;
    END;

    IF ( ISNULL(@fsBrowserSessionId,'') = '' OR ISNULL(@fsWindowSessionId,'') = '' ) BEGIN
      SET @nRetVal = -1;

      IF ( @nDoLogging >= 400 ) BEGIN
        SET @sMsgTxt = 'BrowserSessionId['  + GEN.fToString ( @fsBrowserSessionId )  +
                       '] WindowSessionId[' + GEN.fToString ( @fsWindowSessionId  )  + 
                       '] must not be empty. ';
        EXEC LOG.error @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
      END;
    END 
    ELSE IF ( @nRecCntBrowserWindow > 0 ) BEGIN
      SET @nRetVal = -2;

      IF ( @nDoLogging >= 400 ) BEGIN
        SET @sMsgTxt = 'BrowserSessionId['  + GEN.fToString ( @fsBrowserSessionId )  +
                       '] WindowSessionId[' + GEN.fToString ( @fsWindowSessionId  )  + 
                       '] already exists. ';
        EXEC LOG.error @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
      END;
    END 
    ELSE IF ( @nRecCntBrowserSession = 0 AND @nFlgDeveloperAccess = 0 AND ( @nOpenSessionCnt + 1 ) > @nMaxBrowserSessionCnt ) BEGIN
      SET @nRetVal = -3;

      IF ( @nDoLogging >= 400 ) BEGIN
        SET @sMsgTxt = 'BrowserSessionId['                      + GEN.fToString ( @fsBrowserSessionId    ) +
                       '] FlgDeveloperAccess['                  + GEN.fToString ( @nFlgDeveloperAccess   ) +
                       '] would exceed maximum session count. ' +
                       'MaxBrowserSessionCnt['                  + GEN.fToString ( @nMaxBrowserSessionCnt ) +
                       '] OpenSessionCnt('                      + GEN.fToString ( @nOpenSessionCnt       ) +
                       ')';
        EXEC LOG.error @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
      END;
    END
    ELSE IF ( @nFlgDeveloperAccess = 0 AND @nProdLocNbrInUse > 0 ) BEGIN
      SET @nRetVal = -4;

      IF ( @nDoLogging >= 400 ) BEGIN
        SET @sMsgTxt = 'BrowserSessionId['                  + GEN.fToString ( @fsBrowserSessionId  ) +
                       '] ProdLocNbr['                      + GEN.fToString ( @fnProdLocNbr        ) +
                       '] FlgDeveloperAccess['              + GEN.fToString ( @nFlgDeveloperAccess ) +
                       '] not possible. ProdLocNbr already in use. ' +
                       ' ProdLocNbrInUse('                  + GEN.fToString ( @nProdLocNbrInUse    ) +
                       ') OtherBrowserSession['             + GEN.fToString ( @sOtherBrowserSession ) +
                       ']';
        EXEC LOG.error @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
      END;
    END
    ELSE BEGIN

      -- Falls noch keine Browsersitzung angelegt ist (dies ist das erste Fenster), 
      -- dann entsprechenden Satz anlegen.
      IF ( @nRecCntBrowserSession = 0 ) BEGIN

        INSERT INTO DLG.tSES_BrowserSession 
          ( BrowserSessionId
          , SessionStatus
          , Browser
          , OperatingSystem
          , IPAddress
          , IPProtocol
          , Hostname
          , UserAgent
          , ServerIPAddress
          , ServerProtocol
          , LogoutTms
          , LogoutType
          , CrtTms
          , CrtUsr
          , ModTms
          , ModUsr
          )
        VALUES 
          ( @fsBrowserSessionId
          , 10
          , @fsBrowser
          , @fsOperatingSystem
          , @fsIPAddress
          , @fsIPProtocol
          , @fsHostname
          , @fsUserAgent
          , @fsServerIPAddress
          , @fsServerProtocol
          , NULL
          , NULL
          , DEFAULT
          , @fsUser
          , DEFAULT
          , @fsUser
          );

        SET @nRetValInt_BS = @@rowcount;

        SET @nBrowserSessionRecId = SCOPE_IDENTITY();

      END
      ELSE BEGIN
        SET @nRetValInt_BS = 2;
      END;

      -- Wenn die Sitzung angelegt wurde oder bereits existiert hat kann das Fenster angelegt werden
      IF ( @nRetValInt_BS > 0 ) BEGIN

        INSERT INTO DLG.tSES_BrowserWindow 
          ( BrowserSessionRecId
          , BrowserSessionId
          , WindowSessionId
          , SessionStatus
          , UserName
          , FlgDeveloperAccess
          , ProdLocNbr
          , Lang
          , Dialog
          , ProjectVersion
          , LoginTms
          , LoginType
          , HeartbeatTms
          , HeartbeatType
          , LogoutTms
          , LogoutType
          , CrtTms
          , CrtUsr
          , ModTms
          , ModUsr
          )
        VALUES 
          ( @nBrowserSessionRecId
          , @fsBrowserSessionId
          , @fsWindowSessionId
          , 10
          , @fsUserName
          , @nFlgDeveloperAccess
          , (CASE WHEN @fnProdLocNbr = 0 THEN NULL ELSE @fnProdLocNbr END)
          , @fsLang
          , @fsDialog
          , @fsProjectVersion
          , GETDATE()
          , @fsHeartbeatType
          , GETDATE()
          , @fsHeartbeatType
          , NULL
          , NULL
          , DEFAULT
          , @fsUser
          , DEFAULT
          , @fsUser
          );

          SET @nRetValInt_BW = @@rowcount;
      END
      ELSE BEGIN
        SET @nRetValInt_BS = -1;
      END;

      IF ( @nRetValInt_BS > 0 AND @nRetValInt_BW > 0 ) BEGIN
        SET @nRetVal = 1;

        IF ( @nDoLogging >= 800 ) BEGIN
          SET @sMsgTxt = 'BrowserSessionId['           + GEN.fToString ( @fsBrowserSessionId    )  +
                         '] WindowSessionId['          + GEN.fToString ( @fsWindowSessionId     )  + 
                         '] FlgDeveloperAccess['       + GEN.fToString ( @nFlgDeveloperAccess   )  + 
                         '] was created successfully'  + 
                         ' RecCntBrowserSession['      + GEN.fToString ( @nRecCntBrowserSession )  + 
                         '] RetValInt_BS('             + GEN.fToString ( @nRetValInt_BS         )  + 
                         ') RetValInt_BW('             + GEN.fToString ( @nRetValInt_BW         )  + 
                         ')';
          EXEC LOG.info @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
        END;
      END
      ELSE BEGIN
        SET @nRetVal = -10;

        IF ( @nDoLogging >= 400 ) BEGIN
          SET @sMsgTxt = 'BrowserSessionId['       + GEN.fToString ( @fsBrowserSessionId )  +
                         '] WindowSessionId['      + GEN.fToString ( @fsWindowSessionId  )  + 
                         '] could not be created'  + 
                         ' RecCntBrowserSession['  + GEN.fToString ( @nRecCntBrowserSession )  + 
                         '] RetValInt_BS('         + GEN.fToString ( @nRetValInt_BS      )  + 
                         ') RetValInt_BW('         + GEN.fToString ( @nRetValInt_BW      )  + 
                         ')';
          EXEC LOG.error @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser; 
        END;
      END;

    END 

  END TRY

  BEGIN CATCH
      EXEC GEN.errorHandler @fsFunction = @sFctName, @fsUser = @fsUser;
      SET @nRetVal = -99;
  END CATCH

  IF ( @nDoLogging >= 800 )
  BEGIN
    SET @sMsgTxt  = '<-['+GEN.fToString (@nRetVal)+'] '; 
    EXEC LOG.info @fsLogFunction = @sFctName , @fsLogText = @sMsgTxt, @fsUser = @fsUser;
  END

  EXEC DLG.SES_setRetValParam @fsUser          = @fsUser
                             ,@fsRetFctName    = @sFctName
                             ,@fnRetVal        = @nRetVal
                             ,@fsRetValMode    = @sRetValMode
                             ,@fsRetValJsonTxt = @sRetValJsonTxt
                             ,@fsParam01_Key   = 'ProdLocNbr'
                             ,@fvParam01_Val   = @fnProdLocNbr
                             ,@fsParam02_Key   = 'OtherSessUserName'
                             ,@fvParam02_Val   = @vOtherSessUserName
                             ,@fsParam03_Key   = 'OtherSessHostName'
                             ,@fvParam03_Val   = @vOtherSessHostName
                             ,@fsParam04_Key   = 'OtherSessIpAddress'
                             ,@fvParam04_Val   = @vOtherSessIpAddress
                             ,@fsParam05_Key   = 'MaxBrowserSessionCnt'
                             ,@fvParam05_Val   = @nMaxBrowserSessionCnt

  RETURN @nRetVal;
END
GO