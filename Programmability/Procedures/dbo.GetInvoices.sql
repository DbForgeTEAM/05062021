﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetInvoices]
@includePdf bit = 0,
@from datetime = null,
@to datetime = null,
@Id bigint = null,
--@FA	int = null,
@EANToFAVectorId	int = null,
@EAN	nvarchar(50) = null,
@DeliveryAddress_Street	nvarchar(50) = null,
@DeliveryAddress_PostalCode	nvarchar(50) = null,
@DeliveryAddress_City	nvarchar(50) = null,
@MeterNumber	nvarchar(50) = null,
@Provider	nvarchar(50) = null,
@InvoiceNumber	nvarchar(50) = null,
@InvoiceType	nvarchar(50) = null,
@InvoiceSubType	nvarchar(50) = null,
@InvoiceReference	nvarchar(50) = null,
@InvoiceCategory tinyint = null,
@ClientNumber	nvarchar(50) = null,
@ClientName	nvarchar(50) = null,
--@BuildingAddressId	int = null,
@PeriodFrom	datetime = null,
@PeriodTo	datetime = null,
@PeriodLength	int = null,
@InvoiceDate	datetime = null,
@MaxPowerPeak	decimal(18, 3) = null,
@MaxPowerOffPeak	decimal(18, 3) = null,
@MaxPowerGlobal	decimal(18, 3) = null,
@ConsumptionPeak	decimal(18, 3) = null,
@ConsumptionOffPeak	decimal(18, 3) = null,
@ConsumptionGlobalElectric	decimal(18, 3) = null,
@ConsumptionTotal	decimal(18, 3) = null,
@CaloricValue	decimal(18, 3) = null,
@CosPhi	decimal(18, 3) = null,
@MeterIndexPeak	decimal(18, 3) = null,
@MeterIndexOffPeak	decimal(18, 3) = null,
@MeterIndexPeakPrevious	decimal(18, 3) = null,
@MeterIndexOffPeakPrevious	decimal(18, 3) = null,
@MeterIndexGlobal	decimal(18, 3) = null,
@MeterIndexGlobalPrevious	decimal(18, 3) = null,
@ConsumptionGlobalGas	decimal(18, 3) = null,
@PowerCost decimal(18,3) = null,
@ConsumptionPeakCostExcl	decimal(18, 3) = null,
@ConsumptionOffPeakCostExcl	decimal(18, 3) = null,
@ConsumptionGlobalCostExcl	decimal(18, 3) = null,
@ConsumptionTotalCostExcl	decimal(18, 3) = null,
@GuarantyOfOriginCostExcl	decimal(18, 3) = null,
@RenewableEnergyContributionExcl	decimal(18, 3) = null,
@CogenContributionExcl	decimal(18, 3) = null,
@MeterCost	decimal(18, 3) = null,
@OtherEnergyCostExcl	decimal(18, 3) = null,
@TotalEnergyCostExcl	decimal(18, 3) = null,
@DistributionCostExcl	decimal(18, 3) = null,
@TransmitionCostExcl	decimal(18, 3) = null,
@TotalNetCostExcl	decimal(18, 3) = null,
@TotalTaxesExcl	decimal(18, 3) = null,
@TotalCostExclVAT	decimal(18, 3) = null,
@VATPercentage	decimal(18, 3) = null,
@TotalVATInvoice	decimal(18, 3) = null,
@TotalVAT	decimal(18, 3) = null,
@TotalCostInclVAT	decimal(18, 3) = null,
@VATFreeAmountInvoice	decimal(18, 3) = null,
@AmountDownPaymentsIncluding	decimal(18, 3) = null,
@PrepaidDownPaymentsIncl	decimal(18, 3) = null,
@NewPeriodicalDownPaymentsIncl	decimal(18, 3) = null,
@eClick	nvarchar(50) = null,
@FileName nvarchar(100) = null,

@skip int = null,
@take int = null,

@EANToFAVector_FA_SortDirection int = null,
@EANToFAVector_FA_FilterText nvarchar(1024) = null,
@EANToFAVector_FA_FilterOperator int = null,

@EANToFAVector_Vector_SortDirection int = null,
@EANToFAVector_Vector_FilterText nvarchar(1024) = null,
@EANToFAVector_Vector_FilterOperator int = null,

@EANToFAVector_SubVector_SortDirection int = null,
@EANToFAVector_SubVector_FilterText nvarchar(1024) = null,
@EANToFAVector_SubVector_FilterOperator int = null,

@EANToFAVector_State_SortDirection int = null,
@EANToFAVector_State_FilterText nvarchar(1024) = null,
@EANToFAVector_State_FilterOperator int = null,

@EANToFAVector_ClosedOn_SortDirection int = null,
@EANToFAVector_ClosedOn_FilterText nvarchar(1024) = null,
@EANToFAVector_ClosedOn_FilterText2 nvarchar(1024) = null,
@EANToFAVector_ClosedOn_FilterOperator int = null,

@EAN_SortDirection int = null,
@EAN_FilterText nvarchar(1024) = null,
@EAN_FilterOperator int = null,

@DeliveryAddress_Street_SortDirection int = null,
@DeliveryAddress_Street_FilterText nvarchar(1024) = null,
@DeliveryAddress_Street_FilterOperator int = null,

@DeliveryAddress_PostalCode_SortDirection int = null,
@DeliveryAddress_PostalCode_FilterText nvarchar(1024) = null,
@DeliveryAddress_PostalCode_FilterOperator int = null,

@DeliveryAddress_City_SortDirection int = null,
@DeliveryAddress_City_FilterText nvarchar(1024) = null,
@DeliveryAddress_City_FilterOperator int = null,

@MeterNumber_SortDirection int = null,
@MeterNumber_FilterText nvarchar(1024) = null,
@MeterNumber_FilterOperator int = null,

@Provider_SortDirection int = null,
@Provider_FilterText nvarchar(1024) = null,
@Provider_FilterOperator int = null,

@InvoiceNumber_SortDirection int = null,
@InvoiceNumber_FilterText nvarchar(1024) = null,
@InvoiceNumber_FilterOperator int = null,

@InvoiceType_SortDirection int = null,
@InvoiceType_FilterText nvarchar(1024) = null,
@InvoiceType_FilterOperator int = null,

@InvoiceSubType_SortDirection int = null,
@InvoiceSubType_FilterText nvarchar(1024) = null,
@InvoiceSubType_FilterOperator int = null,

@InvoiceReference_SortDirection int = null,
@InvoiceReference_FilterText nvarchar(1024) = null,
@InvoiceReference_FilterOperator int = null,

@InvoiceCategory_SortDirection int = null,
@InvoiceCategory_FilterText nvarchar(1024) = null,
@InvoiceCategory_FilterOperator int = null,

@ClientNumber_SortDirection int = null,
@ClientNumber_FilterText nvarchar(1024) = null,
@ClientNumber_FilterOperator int = null,

@ClientName_SortDirection int = null,
@ClientName_FilterText nvarchar(1024) = null,
@ClientName_FilterOperator int = null,

@BuildingAddress_Name_SortDirection int = null,
@BuildingAddress_Name_FilterText nvarchar(1024) = null,
@BuildingAddress_Name_FilterOperator int = null,

@BuildingAddress_Street_SortDirection int = null,
@BuildingAddress_Street_FilterText nvarchar(1024) = null,
@BuildingAddress_Street_FilterOperator int = null,

@BuildingAddress_PostalCode_SortDirection int = null,
@BuildingAddress_PostalCode_FilterText nvarchar(1024) = null,
@BuildingAddress_PostalCode_FilterOperator int = null,

@BuildingAddress_City_SortDirection int = null,
@BuildingAddress_City_FilterText nvarchar(1024) = null,
@BuildingAddress_City_FilterOperator int = null,

@PeriodFrom_SortDirection int = null,
@PeriodFrom_FilterText nvarchar(1024) = null,
@PeriodFrom_FilterText2 nvarchar(1024) = null,
@PeriodFrom_FilterOperator int = null,

@PeriodTo_SortDirection int = null,
@PeriodTo_FilterText nvarchar(1024) = null,
@PeriodTo_FilterText2 nvarchar(1024) = null,
@PeriodTo_FilterOperator int = null,

@PeriodLength_SortDirection int = null,
@PeriodLength_FilterText nvarchar(1024) = null,
@PeriodLength_FilterOperator int = null,

@InvoiceDate_SortDirection int = null,
@InvoiceDate_FilterText nvarchar(1024) = null,
@InvoiceDate_FilterText2 nvarchar(1024) = null,
@InvoiceDate_FilterOperator int = null,

@MaxPowerPeak_SortDirection int = null,
@MaxPowerPeak_FilterText nvarchar(1024) = null,
@MaxPowerPeak_FilterOperator int = null,

@MaxPowerOffPeak_SortDirection int = null,
@MaxPowerOffPeak_FilterText nvarchar(1024) = null,
@MaxPowerOffPeak_FilterOperator int = null,

@MaxPowerGlobal_SortDirection int = null,
@MaxPowerGlobal_FilterText nvarchar(1024) = null,
@MaxPowerGlobal_FilterOperator int = null,

@ConsumptionPeak_SortDirection int = null,
@ConsumptionPeak_FilterText nvarchar(1024) = null,
@ConsumptionPeak_FilterOperator int = null,

@ConsumptionOffPeak_SortDirection int = null,
@ConsumptionOffPeak_FilterText nvarchar(1024) = null,
@ConsumptionOffPeak_FilterOperator int = null,

@ConsumptionGlobalElectric_SortDirection int = null,
@ConsumptionGlobalElectric_FilterText nvarchar(1024) = null,
@ConsumptionGlobalElectric_FilterOperator int = null,

@ConsumptionTotal_SortDirection int = null,
@ConsumptionTotal_FilterText nvarchar(1024) = null,
@ConsumptionTotal_FilterOperator int = null,

@CaloricValue_SortDirection int = null,
@CaloricValue_FilterText nvarchar(1024) = null,
@CaloricValue_FilterOperator int = null,

@CosPhi_SortDirection int = null,
@CosPhi_FilterText nvarchar(1024) = null,
@CosPhi_FilterOperator int = null,

@MeterIndexPeak_SortDirection int = null,
@MeterIndexPeak_FilterText nvarchar(1024) = null,
@MeterIndexPeak_FilterOperator int = null,

@MeterIndexOffPeak_SortDirection int = null,
@MeterIndexOffPeak_FilterText nvarchar(1024) = null,
@MeterIndexOffPeak_FilterOperator int = null,

@MeterIndexPeakPrevious_SortDirection int = null,
@MeterIndexPeakPrevious_FilterText nvarchar(1024) = null,
@MeterIndexPeakPrevious_FilterOperator int = null,

@MeterIndexOffPeakPrevious_SortDirection int = null,
@MeterIndexOffPeakPrevious_FilterText nvarchar(1024) = null,
@MeterIndexOffPeakPrevious_FilterOperator int = null,

@MeterIndexGlobal_SortDirection int = null,
@MeterIndexGlobal_FilterText nvarchar(1024) = null,
@MeterIndexGlobal_FilterOperator int = null,

@MeterIndexGlobalPrevious_SortDirection int = null,
@MeterIndexGlobalPrevious_FilterText nvarchar(1024) = null,
@MeterIndexGlobalPrevious_FilterOperator int = null,

@ConsumptionGlobalGas_SortDirection int = null,
@ConsumptionGlobalGas_FilterText nvarchar(1024) = null,
@ConsumptionGlobalGas_FilterOperator int = null,

@PowerCost_SortDirection int = null,
@PowerCost_FilterText nvarchar(1024) = null,
@PowerCost_FilterOperator int = null,

@ConsumptionPeakCostExcl_SortDirection int = null,
@ConsumptionPeakCostExcl_FilterText nvarchar(1024) = null,
@ConsumptionPeakCostExcl_FilterOperator int = null,

@ConsumptionOffPeakCostExcl_SortDirection int = null,
@ConsumptionOffPeakCostExcl_FilterText nvarchar(1024) = null,
@ConsumptionOffPeakCostExcl_FilterOperator int = null,

@ConsumptionGlobalCostExcl_SortDirection int = null,
@ConsumptionGlobalCostExcl_FilterText nvarchar(1024) = null,
@ConsumptionGlobalCostExcl_FilterOperator int = null,

@ConsumptionTotalCostExcl_SortDirection int = null,
@ConsumptionTotalCostExcl_FilterText nvarchar(1024) = null,
@ConsumptionTotalCostExcl_FilterOperator int = null,

@GuarantyOfOriginCostExcl_SortDirection int = null,
@GuarantyOfOriginCostExcl_FilterText nvarchar(1024) = null,
@GuarantyOfOriginCostExcl_FilterOperator int = null,

@RenewableEnergyContributionExcl_SortDirection int = null,
@RenewableEnergyContributionExcl_FilterText nvarchar(1024) = null,
@RenewableEnergyContributionExcl_FilterOperator int = null,

@CogenContributionExcl_SortDirection int = null,
@CogenContributionExcl_FilterText nvarchar(1024) = null,
@CogenContributionExcl_FilterOperator int = null,

@MeterCost_SortDirection int = null,
@MeterCost_FilterText nvarchar(1024) = null,
@MeterCost_FilterOperator int = null,

@OtherEnergyCostExcl_SortDirection int = null,
@OtherEnergyCostExcl_FilterText nvarchar(1024) = null,
@OtherEnergyCostExcl_FilterOperator int = null,

@TotalEnergyCostExcl_SortDirection int = null,
@TotalEnergyCostExcl_FilterText nvarchar(1024) = null,
@TotalEnergyCostExcl_FilterOperator int = null,

@DistributionCostExcl_SortDirection int = null,
@DistributionCostExcl_FilterText nvarchar(1024) = null,
@DistributionCostExcl_FilterOperator int = null,

@TransmitionCostExcl_SortDirection int = null,
@TransmitionCostExcl_FilterText nvarchar(1024) = null,
@TransmitionCostExcl_FilterOperator int = null,

@TotalNetCostExcl_SortDirection int = null,
@TotalNetCostExcl_FilterText nvarchar(1024) = null,
@TotalNetCostExcl_FilterOperator int = null,

@TotalTaxesExcl_SortDirection int = null,
@TotalTaxesExcl_FilterText nvarchar(1024) = null,
@TotalTaxesExcl_FilterOperator int = null,

@TotalCostExclVAT_SortDirection int = null,
@TotalCostExclVAT_FilterText nvarchar(1024) = null,
@TotalCostExclVAT_FilterOperator int = null,

@VATPercentage_SortDirection int = null,
@VATPercentage_FilterText nvarchar(1024) = null,
@VATPercentage_FilterOperator int = null,

@TotalVATInvoice_SortDirection int = null,
@TotalVATInvoice_FilterText nvarchar(1024) = null,
@TotalVATInvoice_FilterOperator int = null,

@TotalVAT_SortDirection int = null,
@TotalVAT_FilterText nvarchar(1024) = null,
@TotalVAT_FilterOperator int = null,

@TotalCostInclVAT_SortDirection int = null,
@TotalCostInclVAT_FilterText nvarchar(1024) = null,
@TotalCostInclVAT_FilterOperator int = null,

@VATFreeAmountInvoice_SortDirection int = null,
@VATFreeAmountInvoice_FilterText nvarchar(1024) = null,
@VATFreeAmountInvoice_FilterOperator int = null,

@AmountDownPaymentsIncluding_SortDirection int = null,
@AmountDownPaymentsIncluding_FilterText nvarchar(1024) = null,
@AmountDownPaymentsIncluding_FilterOperator int = null,

@PrepaidDownPaymentsIncl_SortDirection int = null,
@PrepaidDownPaymentsIncl_FilterText nvarchar(1024) = null,
@PrepaidDownPaymentsIncl_FilterOperator int = null,

@NewPeriodicalDownPaymentsIncl_SortDirection int = null,
@NewPeriodicalDownPaymentsIncl_FilterText nvarchar(1024) = null,
@NewPeriodicalDownPaymentsIncl_FilterOperator int = null,

@eClick_SortDirection int = null,
@eClick_FilterText nvarchar(1024) = null,
@eClick_FilterOperator int = null,

@FileName_SortDirection int = null,
@FileName_FilterText nvarchar(1024) = null,
@FileName_FilterOperator int = null
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @SortDirection_Descending int;
	DECLARE @SortDirection_Ascending int;
	DECLARE @FilterOperator_IsLessThan int;
	DECLARE @FilterOperator_IsLessThanOrEqualTo int;
	DECLARE @FilterOperator_IsEqualTo int;
	DECLARE @FilterOperator_IsNotEqualTo int;
	DECLARE @FilterOperator_IsGreaterThanOrEqualTo int;
	DECLARE @FilterOperator_IsGreaterThan int;
	DECLARE @FilterOperator_StartsWith int;
	DECLARE @FilterOperator_EndsWith int;
	DECLARE @FilterOperator_Contains int;
	DECLARE @FilterOperator_IsContainedIn int;
	DECLARE @FilterOperator_DoesNotContain int;
	DECLARE @FilterOperator_IsBetween int;

	SET @SortDirection_Descending = 1;
	SET @SortDirection_Ascending = 0;

	SET @FilterOperator_IsLessThan = 0;
	SET @FilterOperator_IsLessThanOrEqualTo = 1;
	SET @FilterOperator_IsEqualTo = 2;
	SET @FilterOperator_IsNotEqualTo = 3;
	SET @FilterOperator_IsGreaterThanOrEqualTo = 4;
	SET @FilterOperator_IsGreaterThan = 5;
	SET @FilterOperator_StartsWith = 6;
	SET @FilterOperator_EndsWith = 7;
	SET @FilterOperator_Contains = 8;
	SET @FilterOperator_IsContainedIn = 9;
	SET @FilterOperator_DoesNotContain = 10;
	SET @FilterOperator_IsBetween = 255;

	IF @skip < 0
	BEGIN
	select @skip = 0
	END
	IF @skip is not null AND @take is not null
	Begin
		 SELECT	[Id],
				--[FA],
				[EANToFAVectorId],
				[EAN],
				[DeliveryAddress_Street],
				[DeliveryAddress_PostalCode],
				[DeliveryAddress_City],
				[MeterNumber],
				[Provider],
				[InvoiceNumber],
				[InvoiceType],
				[InvoiceSubType],[InvoiceReference],
				[InvoiceCategory],
				[ClientNumber],
				[ClientName],
				--[BuildingAddressId],
				[PeriodFrom],
				[PeriodTo],
				[PeriodLength],
				[InvoiceDate],
				[MaxPowerPeak],
				[MaxPowerOffPeak],
				[MaxPowerGlobal],
				[ConsumptionPeak],
				[ConsumptionOffPeak],
				[ConsumptionGlobalElectric],
				[ConsumptionTotal],
				[CaloricValue],
				[CosPhi],
				[MeterIndexPeak],
				[MeterIndexOffPeak],
				[MeterIndexPeakPrevious],
				[MeterIndexOffPeakPrevious],
				[MeterIndexGlobal],
				[MeterIndexGlobalPrevious],
				[ConsumptionGlobalGas],
				[PowerCost],
				[ConsumptionPeakCostExcl],
				[ConsumptionOffPeakCostExcl],
				[ConsumptionGlobalCostExcl],
				[ConsumptionTotalCostExcl],
				[GuarantyOfOriginCostExcl],
				[RenewableEnergyContributionExcl],
				[CogenContributionExcl],
				[MeterCost],
				[OtherEnergyCostExcl],
				[TotalEnergyCostExcl],
				[DistributionCostExcl],
				[TransmitionCostExcl],
				[TotalNetCostExcl],
				[TotalTaxesExcl],
				[TotalCostExclVAT],
				[VATPercentage],
				[TotalVATInvoice],
				[TotalVAT],
				[TotalCostInclVAT],
				[VATFreeAmountInvoice],
				[AmountDownPaymentsIncluding],
				[PrepaidDownPaymentsIncl],
				[NewPeriodicalDownPaymentsIncl],
				[eClick],
				[FileName],
				[PdfFileContent],
				[HasPdf],
				[InsertedBy],
				[InsertedOn],
				[UpdatedBy],
				[UpdatedOn]
		FROM(   
			SELECT i.[Id],
				--i.[FA],
				i.[EANToFAVectorId],
				i.[EAN],
				i.[DeliveryAddress_Street],
				i.[DeliveryAddress_PostalCode],
				i.[DeliveryAddress_City],
				i.[MeterNumber],
				i.[Provider],
				i.[InvoiceNumber],
				i.[InvoiceType],
				i.[InvoiceSubType],
				i.[InvoiceReference],
				i.[InvoiceCategory],
				i.[ClientNumber],
				i.[ClientName],
				--i.[BuildingAddressId],
				i.[PeriodFrom],
				i.[PeriodTo],
				i.[PeriodLength],
				i.[InvoiceDate],
				i.[MaxPowerPeak],
				i.[MaxPowerOffPeak],
				i.[MaxPowerGlobal],
				i.[ConsumptionPeak],
				i.[ConsumptionOffPeak],
				i.[ConsumptionGlobalElectric],
				i.[ConsumptionTotal],
				i.[CaloricValue],
				i.[CosPhi],
				i.[MeterIndexPeak],
				i.[MeterIndexOffPeak],
				i.[MeterIndexPeakPrevious],
				i.[MeterIndexOffPeakPrevious],
				i.[MeterIndexGlobal],
				i.[MeterIndexGlobalPrevious],
				i.[ConsumptionGlobalGas],
				i.[PowerCost],
				i.[ConsumptionPeakCostExcl],
				i.[ConsumptionOffPeakCostExcl],
				i.[ConsumptionGlobalCostExcl],
				i.[ConsumptionTotalCostExcl],
				i.[GuarantyOfOriginCostExcl],
				i.[RenewableEnergyContributionExcl],
				i.[CogenContributionExcl],
				i.[MeterCost],
				i.[OtherEnergyCostExcl],
				i.[TotalEnergyCostExcl],
				i.[DistributionCostExcl],
				i.[TransmitionCostExcl],
				i.[TotalNetCostExcl],
				i.[TotalTaxesExcl],
				i.[TotalCostExclVAT],
				i.[VATPercentage],
				i.[TotalVATInvoice],
				i.[TotalVAT],
				i.[TotalCostInclVAT],
				i.[VATFreeAmountInvoice],
				i.[AmountDownPaymentsIncluding],
				i.[PrepaidDownPaymentsIncl],
				i.[NewPeriodicalDownPaymentsIncl],
				i.[eClick],
				i.[FileName],
				CASE WHEN @includePdf = 1 THEN i.[PdfFileContent]
				ELSE NULL
				END AS [PdfFileContent],
				(CASE WHEN [pdfFileContent] is null THEN CAST(0 as bit)
				  ELSE CAST(1 as bit)
				  END) AS [HasPdf],
				i.[InsertedBy],
				i.[InsertedOn],
				i.[UpdatedBy],
				i.[UpdatedOn]
				,ROW_NUMBER() OVER (ORDER BY
					 CASE WHEN @EANToFAVector_FA_SortDirection = @SortDirection_Descending THEN efv.[FA] END DESC
					,CASE WHEN @EANToFAVector_FA_SortDirection = @SortDirection_Ascending THEN efv.[FA] END -- ASC

					,CASE WHEN @EANToFAVector_Vector_SortDirection = @SortDirection_Descending THEN efv.[Vector] END DESC
					,CASE WHEN @EANToFAVector_Vector_SortDirection = @SortDirection_Ascending THEN efv.[Vector] END -- ASC
					
					,CASE WHEN @EANToFAVector_SubVector_SortDirection = @SortDirection_Descending THEN efv.[SubVector] END DESC
					,CASE WHEN @EANToFAVector_SubVector_SortDirection = @SortDirection_Ascending THEN efv.[SubVector] END -- ASC
					
					,CASE WHEN @EANToFAVector_State_SortDirection = @SortDirection_Descending THEN efv.[State] END DESC
					,CASE WHEN @EANToFAVector_State_SortDirection = @SortDirection_Ascending THEN efv.[State] END -- ASC
										
					,CASE WHEN @EANToFAVector_ClosedOn_SortDirection = @SortDirection_Descending THEN efv.[ClosedOn] END DESC
					,CASE WHEN @EANToFAVector_ClosedOn_SortDirection = @SortDirection_Ascending THEN efv.[ClosedOn] END -- ASC

					,CASE WHEN @EAN_SortDirection = @SortDirection_Descending THEN i.[EAN] END DESC
					,CASE WHEN @EAN_SortDirection = @SortDirection_Ascending THEN i.[EAN] END -- ASC

					,CASE WHEN @DeliveryAddress_Street_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_Street] END DESC
					,CASE WHEN @DeliveryAddress_Street_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_Street] END -- ASC

					,CASE WHEN @DeliveryAddress_PostalCode_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_PostalCode] END DESC
					,CASE WHEN @DeliveryAddress_PostalCode_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_PostalCode] END -- ASC

					,CASE WHEN @DeliveryAddress_City_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_City] END DESC
					,CASE WHEN @DeliveryAddress_City_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_City] END -- ASC

					,CASE WHEN @MeterNumber_SortDirection = @SortDirection_Descending THEN i.[MeterNumber] END DESC
					,CASE WHEN @MeterNumber_SortDirection = @SortDirection_Ascending THEN i.[MeterNumber] END -- ASC

					,CASE WHEN @Provider_SortDirection = @SortDirection_Descending THEN i.[Provider] END DESC
					,CASE WHEN @Provider_SortDirection = @SortDirection_Ascending THEN i.[Provider] END -- ASC

					,CASE WHEN @InvoiceNumber_SortDirection = @SortDirection_Descending THEN i.[InvoiceNumber] END DESC
					,CASE WHEN @InvoiceNumber_SortDirection = @SortDirection_Ascending THEN i.[InvoiceNumber] END -- ASC

					,CASE WHEN @InvoiceType_SortDirection = @SortDirection_Descending THEN i.[InvoiceType] END DESC
					,CASE WHEN @InvoiceType_SortDirection = @SortDirection_Ascending THEN i.[InvoiceType] END -- ASC

					,CASE WHEN @InvoiceSubType_SortDirection = @SortDirection_Descending THEN i.[InvoiceSubType] END DESC
					,CASE WHEN @InvoiceSubType_SortDirection = @SortDirection_Ascending THEN i.[InvoiceSubType] END -- ASC

					,CASE WHEN @InvoiceReference_SortDirection = @SortDirection_Descending THEN i.[InvoiceReference] END DESC
					,CASE WHEN @InvoiceReference_SortDirection = @SortDirection_Ascending THEN i.[InvoiceReference] END -- ASC
					
					,CASE WHEN @InvoiceCategory_SortDirection = @SortDirection_Descending THEN i.[InvoiceCategory] END DESC
					,CASE WHEN @InvoiceCategory_SortDirection = @SortDirection_Ascending THEN i.[InvoiceCategory] END -- ASC

					,CASE WHEN @ClientNumber_SortDirection = @SortDirection_Descending THEN i.[ClientNumber] END DESC
					,CASE WHEN @ClientNumber_SortDirection = @SortDirection_Ascending THEN i.[ClientNumber] END -- ASC

					,CASE WHEN @ClientName_SortDirection = @SortDirection_Descending THEN i.[ClientName] END DESC
					,CASE WHEN @ClientName_SortDirection = @SortDirection_Ascending THEN i.[ClientName] END -- ASC

					,CASE WHEN @BuildingAddress_Name_SortDirection = @SortDirection_Descending THEN ba.[Name] END DESC
					,CASE WHEN @BuildingAddress_Name_SortDirection = @SortDirection_Ascending THEN ba.[Name] END -- ASC
					
					,CASE WHEN @BuildingAddress_Street_SortDirection = @SortDirection_Descending THEN ba.[Street] END DESC
					,CASE WHEN @BuildingAddress_Street_SortDirection = @SortDirection_Ascending THEN ba.[Street] END -- ASC
					
					,CASE WHEN @BuildingAddress_PostalCode_SortDirection = @SortDirection_Descending THEN ba.[PostalCode] END DESC
					,CASE WHEN @BuildingAddress_PostalCode_SortDirection = @SortDirection_Ascending THEN ba.[PostalCode] END -- ASC
					
					,CASE WHEN @BuildingAddress_City_SortDirection = @SortDirection_Descending THEN ba.[City] END DESC
					,CASE WHEN @BuildingAddress_City_SortDirection = @SortDirection_Ascending THEN ba.[City] END -- ASC

					,CASE WHEN @PeriodFrom_SortDirection = @SortDirection_Descending THEN i.[PeriodFrom] END DESC
					,CASE WHEN @PeriodFrom_SortDirection = @SortDirection_Ascending THEN i.[PeriodFrom] END -- ASC

					,CASE WHEN @PeriodTo_SortDirection = @SortDirection_Descending THEN i.[PeriodTo] END DESC
					,CASE WHEN @PeriodTo_SortDirection = @SortDirection_Ascending THEN i.[PeriodTo] END -- ASC

					,CASE WHEN @PeriodLength_SortDirection = @SortDirection_Descending THEN i.[PeriodLength] END DESC
					,CASE WHEN @PeriodLength_SortDirection = @SortDirection_Ascending THEN i.[PeriodLength] END -- ASC

					,CASE WHEN @InvoiceDate_SortDirection = @SortDirection_Descending THEN i.[InvoiceDate] END DESC
					,CASE WHEN @InvoiceDate_SortDirection = @SortDirection_Ascending THEN i.[InvoiceDate] END -- ASC

					,CASE WHEN @MaxPowerPeak_SortDirection = @SortDirection_Descending THEN i.[MaxPowerPeak] END DESC
					,CASE WHEN @MaxPowerPeak_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerPeak] END -- ASC

					,CASE WHEN @MaxPowerOffPeak_SortDirection = @SortDirection_Descending THEN i.[MaxPowerOffPeak] END DESC
					,CASE WHEN @MaxPowerOffPeak_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerOffPeak] END -- ASC

					,CASE WHEN @MaxPowerGlobal_SortDirection = @SortDirection_Descending THEN i.[MaxPowerGlobal] END DESC
					,CASE WHEN @MaxPowerGlobal_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerGlobal] END -- ASC

					,CASE WHEN @ConsumptionPeak_SortDirection = @SortDirection_Descending THEN i.[ConsumptionPeak] END DESC
					,CASE WHEN @ConsumptionPeak_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionPeak] END -- ASC

					,CASE WHEN @ConsumptionOffPeak_SortDirection = @SortDirection_Descending THEN i.[ConsumptionOffPeak] END DESC
					,CASE WHEN @ConsumptionOffPeak_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionOffPeak] END -- ASC

					,CASE WHEN @ConsumptionGlobalElectric_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalElectric] END DESC
					,CASE WHEN @ConsumptionGlobalElectric_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalElectric] END -- ASC

					,CASE WHEN @ConsumptionTotal_SortDirection = @SortDirection_Descending THEN i.[ConsumptionTotal] END DESC
					,CASE WHEN @ConsumptionTotal_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionTotal] END -- ASC

					,CASE WHEN @CaloricValue_SortDirection = @SortDirection_Descending THEN i.[CaloricValue] END DESC
					,CASE WHEN @CaloricValue_SortDirection = @SortDirection_Ascending THEN i.[CaloricValue] END -- ASC

					,CASE WHEN @CosPhi_SortDirection = @SortDirection_Descending THEN i.[CosPhi] END DESC
					,CASE WHEN @CosPhi_SortDirection = @SortDirection_Ascending THEN i.[CosPhi] END -- ASC

					,CASE WHEN @MeterIndexPeak_SortDirection = @SortDirection_Descending THEN i.[MeterIndexPeak] END DESC
					,CASE WHEN @MeterIndexPeak_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexPeak] END -- ASC

					,CASE WHEN @MeterIndexOffPeak_SortDirection = @SortDirection_Descending THEN i.[MeterIndexOffPeak] END DESC
					,CASE WHEN @MeterIndexOffPeak_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexOffPeak] END -- ASC

					,CASE WHEN @MeterIndexPeakPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexPeakPrevious] END DESC
					,CASE WHEN @MeterIndexPeakPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexPeakPrevious] END -- ASC

					,CASE WHEN @MeterIndexOffPeakPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexOffPeakPrevious] END DESC
					,CASE WHEN @MeterIndexOffPeakPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexOffPeakPrevious] END -- ASC

					,CASE WHEN @MeterIndexGlobal_SortDirection = @SortDirection_Descending THEN i.[MeterIndexGlobal] END DESC
					,CASE WHEN @MeterIndexGlobal_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexGlobal] END -- ASC

					,CASE WHEN @MeterIndexGlobalPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexGlobalPrevious] END DESC
					,CASE WHEN @MeterIndexGlobalPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexGlobalPrevious] END -- ASC

					,CASE WHEN @ConsumptionGlobalGas_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalGas] END DESC
					,CASE WHEN @ConsumptionGlobalGas_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalGas] END -- ASC
					
					,CASE WHEN @PowerCost_SortDirection = @SortDirection_Descending THEN i.[PowerCost] END DESC
					,CASE WHEN @PowerCost_SortDirection = @SortDirection_Ascending THEN i.[PowerCost] END -- ASC

					,CASE WHEN @ConsumptionPeakCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionPeakCostExcl] END DESC
					,CASE WHEN @ConsumptionPeakCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionPeakCostExcl] END -- ASC

					,CASE WHEN @ConsumptionOffPeakCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionOffPeakCostExcl] END DESC
					,CASE WHEN @ConsumptionOffPeakCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionOffPeakCostExcl] END -- ASC

					,CASE WHEN @ConsumptionGlobalCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalCostExcl] END DESC
					,CASE WHEN @ConsumptionGlobalCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalCostExcl] END -- ASC

					,CASE WHEN @ConsumptionTotalCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionTotalCostExcl] END DESC
					,CASE WHEN @ConsumptionTotalCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionTotalCostExcl] END -- ASC

					,CASE WHEN @GuarantyOfOriginCostExcl_SortDirection = @SortDirection_Descending THEN i.[GuarantyOfOriginCostExcl] END DESC
					,CASE WHEN @GuarantyOfOriginCostExcl_SortDirection = @SortDirection_Ascending THEN i.[GuarantyOfOriginCostExcl] END -- ASC

					,CASE WHEN @RenewableEnergyContributionExcl_SortDirection = @SortDirection_Descending THEN i.[RenewableEnergyContributionExcl] END DESC
					,CASE WHEN @RenewableEnergyContributionExcl_SortDirection = @SortDirection_Ascending THEN i.[RenewableEnergyContributionExcl] END -- ASC

					,CASE WHEN @CogenContributionExcl_SortDirection = @SortDirection_Descending THEN i.[CogenContributionExcl] END DESC
					,CASE WHEN @CogenContributionExcl_SortDirection = @SortDirection_Ascending THEN i.[CogenContributionExcl] END -- ASC

					,CASE WHEN @OtherEnergyCostExcl_SortDirection = @SortDirection_Descending THEN i.[OtherEnergyCostExcl] END DESC
					,CASE WHEN @OtherEnergyCostExcl_SortDirection = @SortDirection_Ascending THEN i.[OtherEnergyCostExcl] END -- ASC

					,CASE WHEN @TotalEnergyCostExcl_SortDirection = @SortDirection_Descending THEN i.[TotalEnergyCostExcl] END DESC
					,CASE WHEN @TotalEnergyCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalEnergyCostExcl] END -- ASC

					,CASE WHEN @DistributionCostExcl_SortDirection = @SortDirection_Descending THEN i.[DistributionCostExcl] END DESC
					,CASE WHEN @DistributionCostExcl_SortDirection = @SortDirection_Ascending THEN i.[DistributionCostExcl] END -- ASC

					,CASE WHEN @TransmitionCostExcl_SortDirection = @SortDirection_Descending THEN i.[TransmitionCostExcl] END DESC
					,CASE WHEN @TransmitionCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TransmitionCostExcl] END -- ASC

					,CASE WHEN @TotalNetCostExcl_SortDirection = @SortDirection_Descending THEN i.[TotalNetCostExcl] END DESC
					,CASE WHEN @TotalNetCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalNetCostExcl] END -- ASC

					,CASE WHEN @TotalTaxesExcl_SortDirection = @SortDirection_Descending THEN i.[TotalTaxesExcl] END DESC
					,CASE WHEN @TotalTaxesExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalTaxesExcl] END -- ASC

					,CASE WHEN @TotalCostExclVAT_SortDirection = @SortDirection_Descending THEN i.[TotalCostExclVAT] END DESC
					,CASE WHEN @TotalCostExclVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalCostExclVAT] END -- ASC

					,CASE WHEN @VATPercentage_SortDirection = @SortDirection_Descending THEN i.[VATPercentage] END DESC
					,CASE WHEN @VATPercentage_SortDirection = @SortDirection_Ascending THEN i.[VATPercentage] END -- ASC

					,CASE WHEN @TotalVATInvoice_SortDirection = @SortDirection_Descending THEN i.[TotalVATInvoice] END DESC
					,CASE WHEN @TotalVATInvoice_SortDirection = @SortDirection_Ascending THEN i.[TotalVATInvoice] END -- ASC

    				,CASE WHEN @TotalVAT_SortDirection = @SortDirection_Descending THEN i.[TotalVAT] END DESC
    				,CASE WHEN @TotalVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalVAT] END -- ASC

					,CASE WHEN @TotalCostInclVAT_SortDirection = @SortDirection_Descending THEN i.[TotalCostInclVAT] END DESC
					,CASE WHEN @TotalCostInclVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalCostInclVAT] END -- ASC

					,CASE WHEN @VATFreeAmountInvoice_SortDirection = @SortDirection_Descending THEN i.[VATFreeAmountInvoice] END DESC
					,CASE WHEN @VATFreeAmountInvoice_SortDirection = @SortDirection_Ascending THEN i.[VATFreeAmountInvoice] END -- ASC

					,CASE WHEN @AmountDownPaymentsIncluding_SortDirection = @SortDirection_Descending THEN i.[AmountDownPaymentsIncluding] END DESC
					,CASE WHEN @AmountDownPaymentsIncluding_SortDirection = @SortDirection_Ascending THEN i.[AmountDownPaymentsIncluding] END -- ASC

					,CASE WHEN @PrepaidDownPaymentsIncl_SortDirection = @SortDirection_Descending THEN i.[PrepaidDownPaymentsIncl] END DESC
					,CASE WHEN @PrepaidDownPaymentsIncl_SortDirection = @SortDirection_Ascending THEN i.[PrepaidDownPaymentsIncl] END -- ASC

					,CASE WHEN @NewPeriodicalDownPaymentsIncl_SortDirection = @SortDirection_Descending THEN i.[NewPeriodicalDownPaymentsIncl] END DESC
					,CASE WHEN @NewPeriodicalDownPaymentsIncl_SortDirection = @SortDirection_Ascending THEN i.[NewPeriodicalDownPaymentsIncl] END -- ASC

					,CASE WHEN @eClick_SortDirection = @SortDirection_Descending THEN i.[eClick] END DESC
					,CASE WHEN @eClick_SortDirection = @SortDirection_Ascending THEN i.[eClick] END -- ASC

					,CASE WHEN @FileName_SortDirection = @SortDirection_Descending THEN i.[FileName] END DESC
					,CASE WHEN @FileName_SortDirection = @SortDirection_Ascending THEN i.[FileName] END -- ASC

				   ) AS RowNumber
			FROM [dbo].[Invoice] i		
			LEFT OUTER JOIN [dbo].[EANToFAVector] efv ON i.[EANToFAVectorId] = efv.[Id]
			LEFT OUTER JOIN [dbo].[BuildingAddress] ba ON efv.[FA] = ba.[FA]	
			WHERE	
			(@from is null
			OR	(	@from is not null
					AND	i.[InvoiceDate] >= @from))
			And (@to is null
			OR	(	@to is not null
					AND	i.[InvoiceDate] <= @to))
			AND (@id is null
			OR	(	@id is not null
					AND	i.[Id] = @id))
			--AND (	@FA is null
			--	OR	(	@FA is not null
			--			AND	i.[FA] = @FA))
			AND (	@EANToFAVectorId is null
				OR	(	@EANToFAVectorId is not null
						AND	i.[EANToFAVectorId] = @EANToFAVectorId))
			AND (	@EAN is null
				OR	(	@EAN is not null
						AND	i.[EAN] = @EAN))
			AND (	@DeliveryAddress_Street is null
				OR	(	@DeliveryAddress_Street is not null
						AND	i.[DeliveryAddress_Street] = @DeliveryAddress_Street))
			AND (	@DeliveryAddress_PostalCode is null
				OR	(	@DeliveryAddress_PostalCode is not null
						AND	i.[DeliveryAddress_PostalCode] = @DeliveryAddress_PostalCode))
			AND (	@DeliveryAddress_City is null
				OR	(	@DeliveryAddress_City is not null
						AND	i.[DeliveryAddress_City] = @DeliveryAddress_City))
			AND (	@MeterNumber is null
				OR	(	@MeterNumber is not null
						AND	i.[MeterNumber] = @MeterNumber))
			AND (	@Provider is null
				OR	(	@Provider is not null
						AND	i.[Provider] = @Provider))
			AND (	@InvoiceNumber is null
				OR	(	@InvoiceNumber is not null
						AND	i.[InvoiceNumber] = @InvoiceNumber))
			AND (	@InvoiceType is null
				OR	(	@InvoiceType is not null
						AND	i.[InvoiceType] = @InvoiceType))
			AND (	@InvoiceSubType is null
				OR	(	@InvoiceSubType is not null
						AND	i.[InvoiceSubType] = @InvoiceSubType))
			AND (	@InvoiceReference is null
				OR	(	@InvoiceReference is not null
						AND	i.[InvoiceReference] = @InvoiceReference))
			AND (	@InvoiceCategory is null
				OR	(	@InvoiceCategory is not null
						AND	i.[InvoiceCategory] = @InvoiceCategory))
			AND (	@ClientNumber is null
				OR	(	@ClientNumber is not null
						AND	i.[ClientNumber] = @ClientNumber))
			AND (	@ClientName is null
				OR	(	@ClientName is not null
						AND	i.[ClientName] = @ClientName))
			----AND (	@BuildingAddressId is null
			----	OR	(	@BuildingAddressId is not null
			----			AND	i.[BuildingAddressId] = @BuildingAddressId))
			AND (	@PeriodFrom is null
				OR	(	@PeriodFrom is not null
						AND	i.[PeriodFrom] = @PeriodFrom))
			AND (	@PeriodTo is null
				OR	(	@PeriodTo is not null
						AND	i.[PeriodTo] = @PeriodTo))
			AND (	@PeriodLength is null
				OR	(	@PeriodLength is not null
						AND	i.[PeriodLength] = @PeriodLength))
			AND (	@InvoiceDate is null
				OR	(	@InvoiceDate is not null
						AND	i.[InvoiceDate] = @InvoiceDate))
			AND (	@MaxPowerPeak is null
				OR	(	@MaxPowerPeak is not null
						AND	i.[MaxPowerPeak] = @MaxPowerPeak))
			AND (	@MaxPowerOffPeak is null
				OR	(	@MaxPowerOffPeak is not null
						AND	i.[MaxPowerOffPeak] = @MaxPowerOffPeak))
			AND (	@MaxPowerGlobal is null
				OR	(	@MaxPowerGlobal is not null
						AND	i.[MaxPowerGlobal] = @MaxPowerGlobal))
			AND (	@ConsumptionPeak is null
				OR	(	@ConsumptionPeak is not null
						AND	i.[ConsumptionPeak] = @ConsumptionPeak))
			AND (	@ConsumptionOffPeak is null
				OR	(	@ConsumptionOffPeak is not null
						AND	i.[ConsumptionOffPeak] = @ConsumptionOffPeak))
			AND (	@ConsumptionGlobalElectric is null
				OR	(	@ConsumptionGlobalElectric is not null
						AND	i.[ConsumptionGlobalElectric] = @ConsumptionGlobalElectric))
			AND (	@ConsumptionTotal is null
				OR	(	@ConsumptionTotal is not null
						AND	i.[ConsumptionTotal] = @ConsumptionTotal))
			AND (	@CaloricValue is null
				OR	(	@CaloricValue is not null
						AND	i.[CaloricValue] = @CaloricValue))
			AND (	@CosPhi is null
				OR	(	@CosPhi is not null
						AND	i.[CosPhi] = @CosPhi))
			AND (	@MeterIndexPeak is null
				OR	(	@MeterIndexPeak is not null
						AND	i.[MeterIndexPeak] = @MeterIndexPeak))
			AND (	@MeterIndexOffPeak is null
				OR	(	@MeterIndexOffPeak is not null
						AND	i.[MeterIndexOffPeak] = @MeterIndexOffPeak))
			AND (	@MeterIndexPeakPrevious is null
				OR	(	@MeterIndexPeakPrevious is not null
						AND	i.[MeterIndexPeakPrevious] = @MeterIndexPeakPrevious))
			AND (	@MeterIndexOffPeakPrevious is null
				OR	(	@MeterIndexOffPeakPrevious is not null
						AND	i.[MeterIndexOffPeakPrevious] = @MeterIndexOffPeakPrevious))
			AND (	@MeterIndexGlobal is null
				OR	(	@MeterIndexGlobal is not null
						AND	i.[MeterIndexGlobal] = @MeterIndexGlobal))
			AND (	@MeterIndexGlobalPrevious is null
				OR	(	@MeterIndexGlobalPrevious is not null
						AND	i.[MeterIndexGlobalPrevious] = @MeterIndexGlobalPrevious))
			AND (	@ConsumptionGlobalGas is null
				OR	(	@ConsumptionGlobalGas is not null
						AND	i.[ConsumptionGlobalGas] = @ConsumptionGlobalGas))
			AND (	@PowerCost is null
				OR	(	@PowerCost is not null
						AND	i.[PowerCost] = @PowerCost))
			AND (	@ConsumptionPeakCostExcl is null
				OR	(	@ConsumptionPeakCostExcl is not null
						AND	i.[ConsumptionPeakCostExcl] = @ConsumptionPeakCostExcl))
			AND (	@ConsumptionOffPeakCostExcl is null
				OR	(	@ConsumptionOffPeakCostExcl is not null
						AND	i.[ConsumptionOffPeakCostExcl] = @ConsumptionOffPeakCostExcl))
			AND (	@ConsumptionGlobalCostExcl is null
				OR	(	@ConsumptionGlobalCostExcl is not null
						AND	i.[ConsumptionGlobalCostExcl] = @ConsumptionGlobalCostExcl))
			AND (	@ConsumptionTotalCostExcl is null
				OR	(	@ConsumptionTotalCostExcl is not null
						AND	i.[ConsumptionTotalCostExcl] = @ConsumptionTotalCostExcl))
			AND (	@GuarantyOfOriginCostExcl is null
				OR	(	@GuarantyOfOriginCostExcl is not null
						AND	i.[GuarantyOfOriginCostExcl] = @GuarantyOfOriginCostExcl))
			AND (	@RenewableEnergyContributionExcl is null
				OR	(	@RenewableEnergyContributionExcl is not null
						AND	i.[RenewableEnergyContributionExcl] = @RenewableEnergyContributionExcl))
			AND (	@CogenContributionExcl is null
				OR	(	@CogenContributionExcl is not null
						AND	i.[CogenContributionExcl] = @CogenContributionExcl))
			AND (	@MeterCost is null
				OR	(	@MeterCost is not null
						AND	i.[MeterCost] = @MeterCost))
			AND (	@OtherEnergyCostExcl is null
				OR	(	@OtherEnergyCostExcl is not null
						AND	i.[OtherEnergyCostExcl] = @OtherEnergyCostExcl))
			AND (	@TotalEnergyCostExcl is null
				OR	(	@TotalEnergyCostExcl is not null
						AND	i.[TotalEnergyCostExcl] = @TotalEnergyCostExcl))
			AND (	@DistributionCostExcl is null
				OR	(	@DistributionCostExcl is not null
						AND	i.[DistributionCostExcl] = @DistributionCostExcl))
			AND (	@TransmitionCostExcl is null
				OR	(	@TransmitionCostExcl is not null
						AND	i.[TransmitionCostExcl] = @TransmitionCostExcl))
			AND (	@TotalNetCostExcl is null
				OR	(	@TotalNetCostExcl is not null
						AND	i.[TotalNetCostExcl] = @TotalNetCostExcl))
			AND (	@TotalTaxesExcl is null
				OR	(	@TotalTaxesExcl is not null
						AND	i.[TotalTaxesExcl] = @TotalTaxesExcl))
			AND (	@TotalCostExclVAT is null
				OR	(	@TotalCostExclVAT is not null
						AND	i.[TotalCostExclVAT] = @TotalCostExclVAT))
			AND (	@VATPercentage is null
				OR	(	@VATPercentage is not null
						AND	i.[VATPercentage] = @VATPercentage))
			AND (	@TotalVATInvoice is null
				OR	(	@TotalVATInvoice is not null
						AND	i.[TotalVATInvoice] = @TotalVATInvoice))
			AND (	@TotalVAT is null
				OR	(	@TotalVAT is not null
						AND	i.[TotalVAT] = @TotalVAT))
			AND (	@TotalCostInclVAT is null
				OR	(	@TotalCostInclVAT is not null
						AND	i.[TotalCostInclVAT] = @TotalCostInclVAT))
			AND (	@VATFreeAmountInvoice is null
				OR	(	@VATFreeAmountInvoice is not null
						AND	i.[VATFreeAmountInvoice] = @VATFreeAmountInvoice))
			AND (	@AmountDownPaymentsIncluding is null
				OR	(	@AmountDownPaymentsIncluding is not null
						AND	i.[AmountDownPaymentsIncluding] = @AmountDownPaymentsIncluding))
			AND (	@PrepaidDownPaymentsIncl is null
				OR	(	@PrepaidDownPaymentsIncl is not null
						AND	i.[PrepaidDownPaymentsIncl] = @PrepaidDownPaymentsIncl))
			AND (	@NewPeriodicalDownPaymentsIncl is null
				OR	(	@NewPeriodicalDownPaymentsIncl is not null
						AND	i.[NewPeriodicalDownPaymentsIncl] = @NewPeriodicalDownPaymentsIncl))
			AND (	@eClick is null
				OR	(	@eClick is not null
						AND	i.[eClick] = @eClick))
			AND (	@FileName is null
				OR	(	@FileName is not null
						AND	i.[FileName] = @FileName))

			AND	(	@EANToFAVector_FA_FilterText is null OR @EANToFAVector_FA_FilterOperator is null
				OR	(	@EANToFAVector_FA_FilterText is not null AND @EANToFAVector_FA_FilterOperator is not null
					AND	( (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_FA_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[FA] is null) OR (efv.[FA] = @EANToFAVector_FA_FilterText)))
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[FA] <> @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsLessThan AND efv.[FA] < @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND efv.[FA] <= @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsGreaterThan AND efv.[FA] > @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND efv.[FA] >= @EANToFAVector_FA_FilterText)
					)
				)
			)
			AND	(	@EANToFAVector_Vector_FilterText is null OR @EANToFAVector_Vector_FilterOperator is null
				OR	(	@EANToFAVector_Vector_FilterText is not null AND @EANToFAVector_Vector_FilterOperator is not null
					AND	( (@EANToFAVector_Vector_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_Vector_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[Vector] is null) OR (efv.[Vector] = @EANToFAVector_Vector_FilterText)))
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[Vector] <> @EANToFAVector_Vector_FilterText)
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_StartsWith AND efv.[Vector] like @EANToFAVector_Vector_FilterText + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_EndsWith AND efv.[Vector] like '%' + @EANToFAVector_Vector_FilterText)
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_Contains AND efv.[Vector] like '%' + @EANToFAVector_Vector_FilterText + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_ISContainedIn AND @EANToFAVector_Vector_FilterText like '%' + efv.[Vector] + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_DoesNotContain AND efv.[Vector] not like '%' + @EANToFAVector_Vector_FilterText + '%')
					)
				)
			)
			AND	(	@EANToFAVector_SubVector_FilterText is null OR @EANToFAVector_SubVector_FilterOperator is null
				OR	(	@EANToFAVector_SubVector_FilterText is not null AND @EANToFAVector_SubVector_FilterOperator is not null
					AND	( (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_SubVector_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[SubVector] is null) OR (efv.[SubVector] = @EANToFAVector_SubVector_FilterText)))
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[SubVector] <> @EANToFAVector_SubVector_FilterText)
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_StartsWith AND efv.[SubVector] like @EANToFAVector_SubVector_FilterText + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_EndsWith AND efv.[SubVector] like '%' + @EANToFAVector_SubVector_FilterText)
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_Contains AND efv.[SubVector] like '%' + @EANToFAVector_SubVector_FilterText + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_ISContainedIn AND @EANToFAVector_SubVector_FilterText like '%' + efv.[SubVector] + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_DoesNotContain AND efv.[SubVector] not like '%' + @EANToFAVector_SubVector_FilterText + '%')
					)
				)
			)
			AND	(	@EANToFAVector_State_FilterText is null OR @EANToFAVector_State_FilterOperator is null
				OR	(	@EANToFAVector_State_FilterText is not null AND @EANToFAVector_State_FilterOperator is not null
					AND	( (@EANToFAVector_State_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_State_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[State] is null) OR (efv.[State] = @EANToFAVector_State_FilterText)))
						OR (@EANToFAVector_State_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[State] <> @EANToFAVector_State_FilterText)
					)
				)
			)
			
			AND	(	@EANToFAVector_ClosedOn_FilterText is null OR @EANToFAVector_ClosedOn_FilterOperator is null
				OR	(	@EANToFAVector_ClosedOn_FilterText is not null AND @EANToFAVector_ClosedOn_FilterOperator is not null
					AND	( (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_ClosedOn_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[ClosedOn] is null) OR (efv.[ClosedOn] = @EANToFAVector_ClosedOn_FilterText)))
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[ClosedOn] <> @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsLessThan AND efv.[ClosedOn] < @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND efv.[ClosedOn] <= @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsGreaterThan AND efv.[ClosedOn] > @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND efv.[ClosedOn] >= @EANToFAVector_ClosedOn_FilterText)
						OR(@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsBetween AND efv.[ClosedOn] BETWEEN @EANToFAVector_ClosedOn_FilterText AND @EANToFAVector_ClosedOn_FilterText2))
					)
				)			
			AND	(	@EAN_FilterText is null OR @EAN_FilterOperator is null
				OR	(	@EAN_FilterText is not null AND @EAN_FilterOperator is not null
					AND	( (@EAN_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EAN_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[EAN] is null) OR (i.[EAN] = @EAN_FilterText)))
						OR (@EAN_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[EAN] <> @EAN_FilterText)
						OR (@EAN_FilterOperator = @FilterOperator_StartsWith AND i.[EAN] like @EAN_FilterText + '%')
						OR (@EAN_FilterOperator = @FilterOperator_EndsWith AND i.[EAN] like '%' + @EAN_FilterText)
						OR (@EAN_FilterOperator = @FilterOperator_Contains AND i.[EAN] like '%' + @EAN_FilterText + '%')
						OR (@EAN_FilterOperator = @FilterOperator_ISContainedIn AND @EAN_FilterText like '%' + i.[EAN] + '%')
						OR (@EAN_FilterOperator = @FilterOperator_DoesNotContain AND i.[EAN] not like '%' + @EAN_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_Street_FilterText is null OR @DeliveryAddress_Street_FilterOperator is null
				OR	(	@DeliveryAddress_Street_FilterText is not null AND @DeliveryAddress_Street_FilterOperator is not null
					AND	( (@DeliveryAddress_Street_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_Street_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_Street] is null) OR (i.[DeliveryAddress_Street] = @DeliveryAddress_Street_FilterText)))
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_Street] <> @DeliveryAddress_Street_FilterText)
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_Street] like @DeliveryAddress_Street_FilterText + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_Street] like '%' + @DeliveryAddress_Street_FilterText)
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_Street] like '%' + @DeliveryAddress_Street_FilterText + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_Street_FilterText like '%' + i.[DeliveryAddress_Street] + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_Street] not like '%' + @DeliveryAddress_Street_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_PostalCode_FilterText is null OR @DeliveryAddress_PostalCode_FilterOperator is null
				OR	(	@DeliveryAddress_PostalCode_FilterText is not null AND @DeliveryAddress_PostalCode_FilterOperator is not null
					AND	( (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_PostalCode_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_PostalCode] is null) OR (i.[DeliveryAddress_PostalCode] = @DeliveryAddress_PostalCode_FilterText)))
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_PostalCode] <> @DeliveryAddress_PostalCode_FilterText)
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_PostalCode] like @DeliveryAddress_PostalCode_FilterText + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_PostalCode] like '%' + @DeliveryAddress_PostalCode_FilterText)
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_PostalCode] like '%' + @DeliveryAddress_PostalCode_FilterText + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_PostalCode_FilterText like '%' + i.[DeliveryAddress_PostalCode] + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_PostalCode] not like '%' + @DeliveryAddress_PostalCode_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_City_FilterText is null OR @DeliveryAddress_City_FilterOperator is null
				OR	(	@DeliveryAddress_City_FilterText is not null AND @DeliveryAddress_City_FilterOperator is not null
					AND	( (@DeliveryAddress_City_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_City_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_City] is null) OR (i.[DeliveryAddress_City] = @DeliveryAddress_City_FilterText)))
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_City] <> @DeliveryAddress_City_FilterText)
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_City] like @DeliveryAddress_City_FilterText + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_City] like '%' + @DeliveryAddress_City_FilterText)
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_City] like '%' + @DeliveryAddress_City_FilterText + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_City_FilterText like '%' + i.[DeliveryAddress_City] + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_City] not like '%' + @DeliveryAddress_City_FilterText + '%')
					)
				)
			)
			AND	(	@MeterNumber_FilterText is null OR @MeterNumber_FilterOperator is null
				OR	(	@MeterNumber_FilterText is not null AND @MeterNumber_FilterOperator is not null
					AND	( (@MeterNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterNumber] is null) OR (i.[MeterNumber] = @MeterNumber_FilterText)))
						OR (@MeterNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterNumber] <> @MeterNumber_FilterText)
						OR (@MeterNumber_FilterOperator = @FilterOperator_StartsWith AND i.[MeterNumber] like @MeterNumber_FilterText + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_EndsWith AND i.[MeterNumber] like '%' + @MeterNumber_FilterText)
						OR (@MeterNumber_FilterOperator = @FilterOperator_Contains AND i.[MeterNumber] like '%' + @MeterNumber_FilterText + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_ISContainedIn AND @MeterNumber_FilterText like '%' + i.[MeterNumber] + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[MeterNumber] not like '%' + @MeterNumber_FilterText + '%')
					)
				)
			)
			AND	(	@Provider_FilterText is null OR @Provider_FilterOperator is null
				OR	(	@Provider_FilterText is not null AND @Provider_FilterOperator is not null
					AND	( (@Provider_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@Provider_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[Provider] is null) OR (i.[Provider] = @Provider_FilterText)))
						OR (@Provider_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[Provider] <> @Provider_FilterText)
						OR (@Provider_FilterOperator = @FilterOperator_StartsWith AND i.[Provider] like @Provider_FilterText + '%')
						OR (@Provider_FilterOperator = @FilterOperator_EndsWith AND i.[Provider] like '%' + @Provider_FilterText)
						OR (@Provider_FilterOperator = @FilterOperator_Contains AND i.[Provider] like '%' + @Provider_FilterText + '%')
						OR (@Provider_FilterOperator = @FilterOperator_ISContainedIn AND @Provider_FilterText like '%' + i.[Provider] + '%')
						OR (@Provider_FilterOperator = @FilterOperator_DoesNotContain AND i.[Provider] not like '%' + @Provider_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceNumber_FilterText is null OR @InvoiceNumber_FilterOperator is null
				OR	(	@InvoiceNumber_FilterText is not null AND @InvoiceNumber_FilterOperator is not null
					AND	( (@InvoiceNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceNumber] is null) OR (i.[InvoiceNumber] = @InvoiceNumber_FilterText)))
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceNumber] <> @InvoiceNumber_FilterText)
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceNumber] like @InvoiceNumber_FilterText + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceNumber] like '%' + @InvoiceNumber_FilterText)
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_Contains AND i.[InvoiceNumber] like '%' + @InvoiceNumber_FilterText + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceNumber_FilterText like '%' + i.[InvoiceNumber] + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceNumber] not like '%' + @InvoiceNumber_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceType_FilterText is null OR @InvoiceType_FilterOperator is null
				OR	(	@InvoiceType_FilterText is not null AND @InvoiceType_FilterOperator is not null
					AND	( (@InvoiceType_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceType_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceType] is null) OR (i.[InvoiceType] = @InvoiceType_FilterText)))
						OR (@InvoiceType_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceType] <> @InvoiceType_FilterText)
						OR (@InvoiceType_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceType] like @InvoiceType_FilterText + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceType] like '%' + @InvoiceType_FilterText)
						OR (@InvoiceType_FilterOperator = @FilterOperator_Contains AND i.[InvoiceType] like '%' + @InvoiceType_FilterText + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceType_FilterText like '%' + i.[InvoiceType] + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceType] not like '%' + @InvoiceType_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceSubType_FilterText is null OR @InvoiceSubType_FilterOperator is null
				OR	(	@InvoiceSubType_FilterText is not null AND @InvoiceSubType_FilterOperator is not null
					AND	( (@InvoiceSubType_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceSubType_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceSubType] is null) OR (i.[InvoiceSubType] = @InvoiceSubType_FilterText)))
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceSubType] <> @InvoiceSubType_FilterText)
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceSubType] like @InvoiceSubType_FilterText + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceSubType] like '%' + @InvoiceSubType_FilterText)
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_Contains AND i.[InvoiceSubType] like '%' + @InvoiceSubType_FilterText + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceSubType_FilterText like '%' + i.[InvoiceSubType] + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceSubType] not like '%' + @InvoiceSubType_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceReference_FilterText is null OR @InvoiceReference_FilterOperator is null
				OR	(	@InvoiceReference_FilterText is not null AND @InvoiceReference_FilterOperator is not null
					AND	( (@InvoiceReference_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceReference_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceReference] is null) OR (i.[InvoiceReference] = @InvoiceReference_FilterText)))
						OR (@InvoiceReference_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceReference] <> @InvoiceReference_FilterText)
						OR (@InvoiceReference_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceReference] like @InvoiceReference_FilterText + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceReference] like '%' + @InvoiceReference_FilterText)
						OR (@InvoiceReference_FilterOperator = @FilterOperator_Contains AND i.[InvoiceReference] like '%' + @InvoiceReference_FilterText + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceReference_FilterText like '%' + i.[InvoiceReference] + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceReference] not like '%' + @InvoiceReference_FilterText + '%')
					)
				)
			)
			
			AND	(	@InvoiceCategory_FilterText is null OR @InvoiceCategory_FilterOperator is null
				OR	(	@InvoiceCategory_FilterText is not null AND @InvoiceCategory_FilterOperator is not null
					AND	( (@InvoiceCategory_FilterOperator = @FilterOperator_IsEqualTo AND (i.[InvoiceCategory] = @InvoiceCategory_FilterText))
						OR (@InvoiceCategory_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceCategory] <> @InvoiceCategory_FilterText)
					)
				)
			)
			AND	(	@ClientNumber_FilterText is null OR @ClientNumber_FilterOperator is null
				OR	(	@ClientNumber_FilterText is not null AND @ClientNumber_FilterOperator is not null
					AND	( (@ClientNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ClientNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ClientNumber] is null) OR (i.[ClientNumber] = @ClientNumber_FilterText)))
						OR (@ClientNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ClientNumber] <> @ClientNumber_FilterText)
						OR (@ClientNumber_FilterOperator = @FilterOperator_StartsWith AND i.[ClientNumber] like @ClientNumber_FilterText + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_EndsWith AND i.[ClientNumber] like '%' + @ClientNumber_FilterText)
						OR (@ClientNumber_FilterOperator = @FilterOperator_Contains AND i.[ClientNumber] like '%' + @ClientNumber_FilterText + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_ISContainedIn AND @ClientNumber_FilterText like '%' + i.[ClientNumber] + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[ClientNumber] not like '%' + @ClientNumber_FilterText + '%')
					)
				)
			)
			AND	(	@ClientName_FilterText is null OR @ClientName_FilterOperator is null
				OR	(	@ClientName_FilterText is not null AND @ClientName_FilterOperator is not null
					AND	( (@ClientName_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ClientName_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ClientName] is null) OR (i.[ClientName] = @ClientName_FilterText)))
						OR (@ClientName_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ClientName] <> @ClientName_FilterText)
						OR (@ClientName_FilterOperator = @FilterOperator_StartsWith AND i.[ClientName] like @ClientName_FilterText + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_EndsWith AND i.[ClientName] like '%' + @ClientName_FilterText)
						OR (@ClientName_FilterOperator = @FilterOperator_Contains AND i.[ClientName] like '%' + @ClientName_FilterText + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_ISContainedIn AND @ClientName_FilterText like '%' + i.[ClientName] + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_DoesNotContain AND i.[ClientName] not like '%' + @ClientName_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_Name_FilterText is null OR @BuildingAddress_Name_FilterOperator is null
				OR	(	@BuildingAddress_Name_FilterText is not null AND @BuildingAddress_Name_FilterOperator is not null
					AND	( (@BuildingAddress_Name_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_Name_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[Name] is null) OR (ba.[Name] = @BuildingAddress_Name_FilterText)))
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[Name] <> @BuildingAddress_Name_FilterText)
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_StartsWith AND ba.[Name] like @BuildingAddress_Name_FilterText + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_EndsWith AND ba.[Name] like '%' + @BuildingAddress_Name_FilterText)
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_Contains AND ba.[Name] like '%' + @BuildingAddress_Name_FilterText + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_Name_FilterText like '%' + ba.[Name] + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_DoesNotContain AND ba.[Name] not like '%' + @BuildingAddress_Name_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_Street_FilterText is null OR @BuildingAddress_Street_FilterOperator is null
				OR	(	@BuildingAddress_Street_FilterText is not null AND @BuildingAddress_Street_FilterOperator is not null
					AND	( (@BuildingAddress_Street_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_Street_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[Street] is null) OR (ba.[Street] = @BuildingAddress_Street_FilterText)))
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[Street] <> @BuildingAddress_Street_FilterText)
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_StartsWith AND ba.[Street] like @BuildingAddress_Street_FilterText + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_EndsWith AND ba.[Street] like '%' + @BuildingAddress_Street_FilterText)
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_Contains AND ba.[Street] like '%' + @BuildingAddress_Street_FilterText + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_Street_FilterText like '%' + ba.[Street] + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_DoesNotContain AND ba.[Street] not like '%' + @BuildingAddress_Street_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_PostalCode_FilterText is null OR @BuildingAddress_PostalCode_FilterOperator is null
				OR	(	@BuildingAddress_PostalCode_FilterText is not null AND @BuildingAddress_PostalCode_FilterOperator is not null
					AND	( (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_PostalCode_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[PostalCode] is null) OR (ba.[PostalCode] = @BuildingAddress_PostalCode_FilterText)))
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[PostalCode] <> @BuildingAddress_PostalCode_FilterText)
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_StartsWith AND ba.[PostalCode] like @BuildingAddress_PostalCode_FilterText + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_EndsWith AND ba.[PostalCode] like '%' + @BuildingAddress_PostalCode_FilterText)
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_Contains AND ba.[PostalCode] like '%' + @BuildingAddress_PostalCode_FilterText + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_PostalCode_FilterText like '%' + ba.[PostalCode] + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_DoesNotContain AND ba.[PostalCode] not like '%' + @BuildingAddress_PostalCode_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_City_FilterText is null OR @BuildingAddress_City_FilterOperator is null
				OR	(	@BuildingAddress_City_FilterText is not null AND @BuildingAddress_City_FilterOperator is not null
					AND	( (@BuildingAddress_City_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_City_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[City] is null) OR (ba.[City] = @BuildingAddress_City_FilterText)))
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[City] <> @BuildingAddress_City_FilterText)
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_StartsWith AND ba.[City] like @BuildingAddress_City_FilterText + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_EndsWith AND ba.[City] like '%' + @BuildingAddress_City_FilterText)
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_Contains AND ba.[City] like '%' + @BuildingAddress_City_FilterText + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_City_FilterText like '%' + ba.[City] + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_DoesNotContain AND ba.[City] not like '%' + @BuildingAddress_City_FilterText + '%')
					)
				)
			)
			AND	(	@PeriodFrom_FilterText is null OR @PeriodFrom_FilterOperator is null
				OR	(	@PeriodFrom_FilterText is not null AND @PeriodFrom_FilterOperator is not null
					AND	( (@PeriodFrom_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodFrom_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodFrom] is null) OR (i.[PeriodFrom] = @PeriodFrom_FilterText)))
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodFrom] <> @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodFrom] < @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodFrom] <= @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodFrom] > @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodFrom] >= @PeriodFrom_FilterText)
						OR(@PeriodFrom_FilterOperator = @FilterOperator_IsBetween AND i.[PeriodFrom] BETWEEN @PeriodFrom_FilterText AND @PeriodFrom_FilterText2)
					)
				)
			)
			AND	(	@PeriodTo_FilterText is null OR @PeriodTo_FilterOperator is null
				OR	(	@PeriodTo_FilterText is not null AND @PeriodTo_FilterOperator is not null
					AND	( (@PeriodTo_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodTo_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodTo] is null) OR (i.[PeriodTo] = @PeriodTo_FilterText)))
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodTo] <> @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodTo] < @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodTo] <= @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodTo] > @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodTo] >= @PeriodTo_FilterText)
						OR(@PeriodTo_FilterOperator = @FilterOperator_IsBetween AND i.[PeriodTo] BETWEEN @PeriodTo_FilterText AND @PeriodTo_FilterText2)
					)
				)
			)
			AND	(	@PeriodLength_FilterText is null OR @PeriodLength_FilterOperator is null
				OR	(	@PeriodLength_FilterText is not null AND @PeriodLength_FilterOperator is not null
					AND	( (@PeriodLength_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodLength_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodLength] is null) OR (i.[PeriodLength] = @PeriodLength_FilterText)))
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodLength] <> @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodLength] < @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodLength] <= @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodLength] > @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodLength] >= @PeriodLength_FilterText)
					)
				)
			)
			AND	(	@InvoiceDate_FilterText is null OR @InvoiceDate_FilterOperator is null
				OR	(	@InvoiceDate_FilterText is not null AND @InvoiceDate_FilterOperator is not null
					AND	( (@InvoiceDate_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceDate_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceDate] is null) OR (i.[InvoiceDate] = @InvoiceDate_FilterText)))
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceDate] <> @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsLessThan AND i.[InvoiceDate] < @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[InvoiceDate] <= @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsGreaterThan AND i.[InvoiceDate] > @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[InvoiceDate] >= @InvoiceDate_FilterText)
						OR(@InvoiceDate_FilterOperator = @FilterOperator_IsBetween AND i.[InvoiceDate] BETWEEN @InvoiceDate_FilterText AND @InvoiceDate_FilterText2)
					)
				)
			)
			AND	(	@MaxPowerPeak_FilterText is null OR @MaxPowerPeak_FilterOperator is null
				OR	(	@MaxPowerPeak_FilterText is not null AND @MaxPowerPeak_FilterOperator is not null
					AND	( (@MaxPowerPeak_FilterOperator = @FilterOperator_IsEqualTo  AND ((UPPER(@MaxPowerPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerPeak] is null) OR (i.[MaxPowerPeak] = @MaxPowerPeak_FilterText)))
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerPeak] <> @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerPeak] < @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerPeak] <= @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerPeak] > @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerPeak] >= @MaxPowerPeak_FilterText)
					)
				)
			)
			AND	(	@MaxPowerOffPeak_FilterText is null OR @MaxPowerOffPeak_FilterOperator is null
				OR	(	@MaxPowerOffPeak_FilterText is not null AND @MaxPowerOffPeak_FilterOperator is not null
					AND	( (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MaxPowerOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerOffPeak] is null) OR (i.[MaxPowerOffPeak] = @MaxPowerOffPeak_FilterText)))
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerOffPeak] <> @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerOffPeak] < @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerOffPeak] <= @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerOffPeak] > @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerOffPeak] >= @MaxPowerOffPeak_FilterText)
					)
				)
			)
			AND	(	@MaxPowerGlobal_FilterText is null OR @MaxPowerGlobal_FilterOperator is null
				OR	(	@MaxPowerGlobal_FilterText is not null AND @MaxPowerGlobal_FilterOperator is not null
					AND	( (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MaxPowerGlobal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerGlobal] is null) OR (i.[MaxPowerGlobal] = @MaxPowerGlobal_FilterText)))
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerGlobal] <> @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerGlobal] < @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerGlobal] <= @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerGlobal] > @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerGlobal] >= @MaxPowerGlobal_FilterText)
					)
				)
			)
			AND	(	@ConsumptionPeak_FilterText is null OR @ConsumptionPeak_FilterOperator is null
				OR	(	@ConsumptionPeak_FilterText is not null AND @ConsumptionPeak_FilterOperator is not null
					AND	( (@ConsumptionPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionPeak] is null) OR (i.[ConsumptionPeak] = @ConsumptionPeak_FilterText)))
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionPeak] <> @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionPeak] < @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionPeak] <= @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionPeak] > @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionPeak] >= @ConsumptionPeak_FilterText)
					)
				)
			)
			AND	(	@ConsumptionOffPeak_FilterText is null OR @ConsumptionOffPeak_FilterOperator is null
				OR	(	@ConsumptionOffPeak_FilterText is not null AND @ConsumptionOffPeak_FilterOperator is not null
					AND	( (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionOffPeak] is null) OR (i.[ConsumptionOffPeak] = @ConsumptionOffPeak_FilterText)))
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionOffPeak] <> @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionOffPeak] < @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionOffPeak] <= @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionOffPeak] > @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionOffPeak] >= @ConsumptionOffPeak_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalElectric_FilterText is null OR @ConsumptionGlobalElectric_FilterOperator is null
				OR	(	@ConsumptionGlobalElectric_FilterText is not null AND @ConsumptionGlobalElectric_FilterOperator is not null
					AND	( (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalElectric_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalElectric] is null) OR (i.[ConsumptionGlobalElectric] = @ConsumptionGlobalElectric_FilterText)))
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalElectric] <> @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalElectric] < @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalElectric] <= @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalElectric] > @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalElectric] >= @ConsumptionGlobalElectric_FilterText)
					)
				)
			)
			AND	(	@ConsumptionTotal_FilterText is null OR @ConsumptionTotal_FilterOperator is null
				OR	(	@ConsumptionTotal_FilterText is not null AND @ConsumptionTotal_FilterOperator is not null
					AND	( (@ConsumptionTotal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionTotal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionTotal] is null) OR (i.[ConsumptionTotal] = @ConsumptionTotal_FilterText)))
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionTotal] <> @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionTotal] < @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionTotal] <= @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionTotal] > @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionTotal] >= @ConsumptionTotal_FilterText)
					)
				)
			)
			AND	(	@CaloricValue_FilterText is null OR @CaloricValue_FilterOperator is null
				OR	(	@CaloricValue_FilterText is not null AND @CaloricValue_FilterOperator is not null
					AND	( (@CaloricValue_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CaloricValue_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CaloricValue] is null) OR (i.[CaloricValue] = @CaloricValue_FilterText)))
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CaloricValue] <> @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsLessThan AND i.[CaloricValue] < @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CaloricValue] <= @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CaloricValue] > @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CaloricValue] >= @CaloricValue_FilterText)
					)
				)
			)
			AND	(	@CosPhi_FilterText is null OR @CosPhi_FilterOperator is null
				OR	(	@CosPhi_FilterText is not null AND @CosPhi_FilterOperator is not null
					AND	( (@CosPhi_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CosPhi_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CosPhi] is null) OR (i.[CosPhi] = @CosPhi_FilterText)))
						OR (@CosPhi_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CosPhi] <> @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsLessThan AND i.[CosPhi] < @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CosPhi] <= @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CosPhi] > @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CosPhi] >= @CosPhi_FilterText)
					)
				)
			)
			AND	(	@MeterIndexPeak_FilterText is null OR @MeterIndexPeak_FilterOperator is null
				OR	(	@MeterIndexPeak_FilterText is not null AND @MeterIndexPeak_FilterOperator is not null
					AND	( (@MeterIndexPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexPeak] is null) OR (i.[MeterIndexPeak] = @MeterIndexPeak_FilterText)))
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexPeak] <> @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexPeak] < @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexPeak] <= @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexPeak] > @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexPeak] >= @MeterIndexPeak_FilterText)
					)
				)
			)
			AND	(	@MeterIndexOffPeak_FilterText is null OR @MeterIndexOffPeak_FilterOperator is null
				OR	(	@MeterIndexOffPeak_FilterText is not null AND @MeterIndexOffPeak_FilterOperator is not null
					AND	( (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexOffPeak] is null) OR (i.[MeterIndexOffPeak] = @MeterIndexOffPeak_FilterText)))
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexOffPeak] <> @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexOffPeak] < @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexOffPeak] <= @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexOffPeak] > @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexOffPeak] >= @MeterIndexOffPeak_FilterText)
					)
				)
			)
			AND	(	@MeterIndexPeakPrevious_FilterText is null OR @MeterIndexPeakPrevious_FilterOperator is null
				OR	(	@MeterIndexPeakPrevious_FilterText is not null AND @MeterIndexPeakPrevious_FilterOperator is not null
					AND	( (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexPeakPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexPeakPrevious] is null) OR (i.[MeterIndexPeakPrevious] = @MeterIndexPeakPrevious_FilterText)))
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexPeakPrevious] <> @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexPeakPrevious] < @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexPeakPrevious] <= @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexPeakPrevious] > @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexPeakPrevious] >= @MeterIndexPeakPrevious_FilterText)
					)
				)
			)
			AND	(	@MeterIndexOffPeakPrevious_FilterText is null OR @MeterIndexOffPeakPrevious_FilterOperator is null
				OR	(	@MeterIndexOffPeakPrevious_FilterText is not null AND @MeterIndexOffPeakPrevious_FilterOperator is not null
					AND	( (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexOffPeakPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexOffPeakPrevious] is null) OR (i.[MeterIndexOffPeakPrevious] = @MeterIndexOffPeakPrevious_FilterText)))
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexOffPeakPrevious] <> @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexOffPeakPrevious] < @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexOffPeakPrevious] <= @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexOffPeakPrevious] > @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexOffPeakPrevious] >= @MeterIndexOffPeakPrevious_FilterText)
					)
				)
			)
			AND	(	@MeterIndexGlobal_FilterText is null OR @MeterIndexGlobal_FilterOperator is null
				OR	(	@MeterIndexGlobal_FilterText is not null AND @MeterIndexGlobal_FilterOperator is not null
					AND	( (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexGlobal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexGlobal] is null) OR (i.[MeterIndexGlobal] = @MeterIndexGlobal_FilterText)))
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexGlobal] <> @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexGlobal] < @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexGlobal] <= @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexGlobal] > @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexGlobal] >= @MeterIndexGlobal_FilterText)
					)
				)
			)
			AND	(	@MeterIndexGlobalPrevious_FilterText is null OR @MeterIndexGlobalPrevious_FilterOperator is null
				OR	(	@MeterIndexGlobalPrevious_FilterText is not null AND @MeterIndexGlobalPrevious_FilterOperator is not null
					AND	( (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexGlobalPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexGlobalPrevious] is null) OR (i.[MeterIndexGlobalPrevious] = @MeterIndexGlobalPrevious_FilterText)))
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexGlobalPrevious] <> @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexGlobalPrevious] < @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexGlobalPrevious] <= @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexGlobalPrevious] > @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexGlobalPrevious] >= @MeterIndexGlobalPrevious_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalGas_FilterText is null OR @ConsumptionGlobalGas_FilterOperator is null
				OR	(	@ConsumptionGlobalGas_FilterText is not null AND @ConsumptionGlobalGas_FilterOperator is not null
					AND	( (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalGas_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalGas] is null) OR (i.[ConsumptionGlobalGas] = @PowerCost_FilterText)))
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalGas] <> @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalGas] < @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalGas] <= @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalGas] > @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalGas] >= @ConsumptionGlobalGas_FilterText)
					)
				)
			)
			AND	(	@PowerCost_FilterText is null OR @PowerCost_FilterOperator is null
				OR	(	@PowerCost_FilterText is not null AND @PowerCost_FilterOperator is not null
					AND	( (@PowerCost_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PowerCost_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PowerCost] is null) OR (i.[PowerCost] = @PowerCost_FilterText)))
						OR (@PowerCost_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PowerCost] <> @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsLessThan AND i.[PowerCost] < @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PowerCost] <= @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PowerCost] > @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PowerCost] >= @PowerCost_FilterText)
					)
				)
			)
			AND	(	@ConsumptionPeakCostExcl_FilterText is null OR @ConsumptionPeakCostExcl_FilterOperator is null
				OR	(	@ConsumptionPeakCostExcl_FilterText is not null AND @ConsumptionPeakCostExcl_FilterOperator is not null
					AND	( (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionPeakCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionPeakCostExcl] is null) OR (i.[ConsumptionPeakCostExcl] = @ConsumptionPeakCostExcl_FilterText)))
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionPeakCostExcl] <> @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionPeakCostExcl] < @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionPeakCostExcl] <= @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionPeakCostExcl] > @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionPeakCostExcl] >= @ConsumptionPeakCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionOffPeakCostExcl_FilterText is null OR @ConsumptionOffPeakCostExcl_FilterOperator is null
				OR	(	@ConsumptionOffPeakCostExcl_FilterText is not null AND @ConsumptionOffPeakCostExcl_FilterOperator is not null
					AND	( (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionOffPeakCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionOffPeakCostExcl] is null) OR (i.[ConsumptionOffPeakCostExcl] = @ConsumptionOffPeakCostExcl_FilterText)))
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionOffPeakCostExcl] <> @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionOffPeakCostExcl] < @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionOffPeakCostExcl] <= @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionOffPeakCostExcl] > @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionOffPeakCostExcl] >= @ConsumptionOffPeakCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalCostExcl_FilterText is null OR @ConsumptionGlobalCostExcl_FilterOperator is null
				OR	(	@ConsumptionGlobalCostExcl_FilterText is not null AND @ConsumptionGlobalCostExcl_FilterOperator is not null
					AND	( (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalCostExcl] is null) OR (i.[ConsumptionGlobalCostExcl] = @ConsumptionGlobalCostExcl_FilterText)))
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalCostExcl] <> @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalCostExcl] < @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalCostExcl] <= @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalCostExcl] > @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalCostExcl] >= @ConsumptionGlobalCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionTotalCostExcl_FilterText is null OR @ConsumptionTotalCostExcl_FilterOperator is null
				OR	(	@ConsumptionTotalCostExcl_FilterText is not null AND @ConsumptionTotalCostExcl_FilterOperator is not null
					AND	( (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionTotalCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionTotalCostExcl] is null) OR (i.[ConsumptionTotalCostExcl] = @ConsumptionTotalCostExcl_FilterText)))
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionTotalCostExcl] <> @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionTotalCostExcl] < @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionTotalCostExcl] <= @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionTotalCostExcl] > @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionTotalCostExcl] >= @ConsumptionTotalCostExcl_FilterText)
					)
				)
			)
			AND	(	@GuarantyOfOriginCostExcl_FilterText is null OR @GuarantyOfOriginCostExcl_FilterOperator is null
				OR	(	@GuarantyOfOriginCostExcl_FilterText is not null AND @GuarantyOfOriginCostExcl_FilterOperator is not null
					AND	( (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@GuarantyOfOriginCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[GuarantyOfOriginCostExcl] is null) OR (i.[GuarantyOfOriginCostExcl] = @GuarantyOfOriginCostExcl_FilterText)))
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[GuarantyOfOriginCostExcl] <> @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[GuarantyOfOriginCostExcl] < @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[GuarantyOfOriginCostExcl] <= @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[GuarantyOfOriginCostExcl] > @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[GuarantyOfOriginCostExcl] >= @GuarantyOfOriginCostExcl_FilterText)
					)
				)
			)
			AND	(	@RenewableEnergyContributionExcl_FilterText is null OR @RenewableEnergyContributionExcl_FilterOperator is null
				OR	(	@RenewableEnergyContributionExcl_FilterText is not null AND @RenewableEnergyContributionExcl_FilterOperator is not null
					AND	( (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@RenewableEnergyContributionExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[RenewableEnergyContributionExcl] is null) OR (i.[RenewableEnergyContributionExcl] = @RenewableEnergyContributionExcl_FilterText)))
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[RenewableEnergyContributionExcl] <> @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[RenewableEnergyContributionExcl] < @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[RenewableEnergyContributionExcl] <= @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[RenewableEnergyContributionExcl] > @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[RenewableEnergyContributionExcl] >= @RenewableEnergyContributionExcl_FilterText)
					)
				)
			)
			AND	(	@CogenContributionExcl_FilterText is null OR @CogenContributionExcl_FilterOperator is null
				OR	(	@CogenContributionExcl_FilterText is not null AND @CogenContributionExcl_FilterOperator is not null
					AND	( (@CogenContributionExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CogenContributionExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CogenContributionExcl] is null) OR (i.[CogenContributionExcl] = @CogenContributionExcl_FilterText)))
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CogenContributionExcl] <> @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[CogenContributionExcl] < @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CogenContributionExcl] <= @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CogenContributionExcl] > @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CogenContributionExcl] >= @CogenContributionExcl_FilterText)
					)
				)
			)
			AND	(	@MeterCost_FilterText is null OR @MeterCost_FilterOperator is null
				OR	(	@MeterCost_FilterText is not null AND @MeterCost_FilterOperator is not null
					AND	( (@MeterCost_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterCost_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterCost] is null) OR (i.[MeterCost] = @MeterCost_FilterText)))
						OR (@MeterCost_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterCost] <> @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterCost] < @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterCost] <= @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterCost] > @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterCost] >= @MeterCost_FilterText)
					)
				)
			)
			AND	(	@OtherEnergyCostExcl_FilterText is null OR @OtherEnergyCostExcl_FilterOperator is null
				OR	(	@OtherEnergyCostExcl_FilterText is not null AND @OtherEnergyCostExcl_FilterOperator is not null
					AND	( (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@OtherEnergyCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[OtherEnergyCostExcl] is null) OR (i.[OtherEnergyCostExcl] = @OtherEnergyCostExcl_FilterText)))
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[OtherEnergyCostExcl] <> @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[OtherEnergyCostExcl] < @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[OtherEnergyCostExcl] <= @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[OtherEnergyCostExcl] > @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[OtherEnergyCostExcl] >= @OtherEnergyCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalEnergyCostExcl_FilterText is null OR @TotalEnergyCostExcl_FilterOperator is null
				OR	(	@TotalEnergyCostExcl_FilterText is not null AND @TotalEnergyCostExcl_FilterOperator is not null
					AND	( (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalEnergyCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalEnergyCostExcl] is null) OR (i.[TotalEnergyCostExcl] = @TotalEnergyCostExcl_FilterText)))
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalEnergyCostExcl] <> @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalEnergyCostExcl] < @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalEnergyCostExcl] <= @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalEnergyCostExcl] > @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalEnergyCostExcl] >= @TotalEnergyCostExcl_FilterText)
					)
				)
			)
			AND	(	@DistributionCostExcl_FilterText is null OR @DistributionCostExcl_FilterOperator is null
				OR	(	@DistributionCostExcl_FilterText is not null AND @DistributionCostExcl_FilterOperator is not null
					AND	( (@DistributionCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DistributionCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DistributionCostExcl] is null) OR (i.[DistributionCostExcl] = @DistributionCostExcl_FilterText)))
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DistributionCostExcl] <> @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[DistributionCostExcl] < @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[DistributionCostExcl] <= @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[DistributionCostExcl] > @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[DistributionCostExcl] >= @DistributionCostExcl_FilterText)
					)
				)
			)
			AND	(	@TransmitionCostExcl_FilterText is null OR @TransmitionCostExcl_FilterOperator is null
				OR	(	@TransmitionCostExcl_FilterText is not null AND @TransmitionCostExcl_FilterOperator is not null
					AND	( (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TransmitionCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TransmitionCostExcl] is null) OR (i.[TransmitionCostExcl] = @TransmitionCostExcl_FilterText)))
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TransmitionCostExcl] <> @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TransmitionCostExcl] < @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TransmitionCostExcl] <= @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TransmitionCostExcl] > @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TransmitionCostExcl] >= @TransmitionCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalNetCostExcl_FilterText is null OR @TotalNetCostExcl_FilterOperator is null
				OR	(	@TotalNetCostExcl_FilterText is not null AND @TotalNetCostExcl_FilterOperator is not null
					AND	( (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalNetCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalNetCostExcl] is null) OR (i.[TotalNetCostExcl] = @TotalNetCostExcl_FilterText)))
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalNetCostExcl] <> @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalNetCostExcl] < @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalNetCostExcl] <= @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalNetCostExcl] > @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalNetCostExcl] >= @TotalNetCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalTaxesExcl_FilterText is null OR @TotalTaxesExcl_FilterOperator is null
				OR	(	@TotalTaxesExcl_FilterText is not null AND @TotalTaxesExcl_FilterOperator is not null
					AND	( (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalTaxesExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalTaxesExcl] is null) OR (i.[TotalTaxesExcl] = @TotalTaxesExcl_FilterText)))
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalTaxesExcl] <> @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalTaxesExcl] < @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalTaxesExcl] <= @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalTaxesExcl] > @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalTaxesExcl] >= @TotalTaxesExcl_FilterText)
					)
				)
			)
			AND	(	@TotalCostExclVAT_FilterText is null OR @TotalCostExclVAT_FilterOperator is null
				OR	(	@TotalCostExclVAT_FilterText is not null AND @TotalCostExclVAT_FilterOperator is not null
					AND	( (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalCostExclVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalCostExclVAT] is null) OR (i.[TotalCostExclVAT] = @TotalCostExclVAT_FilterText)))
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalCostExclVAT] <> @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalCostExclVAT] < @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalCostExclVAT] <= @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalCostExclVAT] > @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalCostExclVAT] >= @TotalCostExclVAT_FilterText)
					)
				)
			)
			AND	(	@VATPercentage_FilterText is null OR @VATPercentage_FilterOperator is null
				OR	(	@VATPercentage_FilterText is not null AND @VATPercentage_FilterOperator is not null
					AND	( (@VATPercentage_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@VATPercentage_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[VATPercentage] is null) OR (i.[VATPercentage] = @VATPercentage_FilterText)))
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[VATPercentage] <> @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsLessThan AND i.[VATPercentage] < @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[VATPercentage] <= @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsGreaterThan AND i.[VATPercentage] > @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[VATPercentage] >= @VATPercentage_FilterText)
					)
				)
			)
			AND	(	@TotalVATInvoice_FilterText is null OR @TotalVATInvoice_FilterOperator is null
				OR	(	@TotalVATInvoice_FilterText is not null AND @TotalVATInvoice_FilterOperator is not null
					AND	( (@TotalVATInvoice_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalVATInvoice_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalVATInvoice] is null) OR (i.[TotalVATInvoice] = @TotalVATInvoice_FilterText)))
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalVATInvoice] <> @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalVATInvoice] < @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalVATInvoice] <= @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalVATInvoice] > @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalVATInvoice] >= @TotalVATInvoice_FilterText)
					)
				)
			)
			AND	(	@TotalVAT_FilterText is null OR @TotalVAT_FilterOperator is null
				OR	(	@TotalVAT_FilterText is not null AND @TotalVAT_FilterOperator is not null
					AND	( (@TotalVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalVAT] is null) OR (i.[TotalVAT] = @TotalVAT_FilterText)))
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalVAT] <> @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalVAT] < @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalVAT] <= @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalVAT] > @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalVAT] >= @TotalVAT_FilterText)
					)
				)
			)
			AND	(	@TotalCostInclVAT_FilterText is null OR @TotalCostInclVAT_FilterOperator is null
				OR	(	@TotalCostInclVAT_FilterText is not null AND @TotalCostInclVAT_FilterOperator is not null
					AND	( (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalCostInclVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalCostInclVAT] is null) OR (i.[TotalCostInclVAT] = @TotalCostInclVAT_FilterText)))
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalCostInclVAT] <> @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalCostInclVAT] < @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalCostInclVAT] <= @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalCostInclVAT] > @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalCostInclVAT] >= @TotalCostInclVAT_FilterText)
					)
				)
			)
			AND	(	@VATFreeAmountInvoice_FilterText is null OR @VATFreeAmountInvoice_FilterOperator is null
				OR	(	@VATFreeAmountInvoice_FilterText is not null AND @VATFreeAmountInvoice_FilterOperator is not null
					AND	( (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@VATFreeAmountInvoice_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[VATFreeAmountInvoice] is null) OR (i.[VATFreeAmountInvoice] = @VATFreeAmountInvoice_FilterText)))
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[VATFreeAmountInvoice] <> @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsLessThan AND i.[VATFreeAmountInvoice] < @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[VATFreeAmountInvoice] <= @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsGreaterThan AND i.[VATFreeAmountInvoice] > @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[VATFreeAmountInvoice] >= @VATFreeAmountInvoice_FilterText)
					)
				)
			)
			AND	(	@AmountDownPaymentsIncluding_FilterText is null OR @AmountDownPaymentsIncluding_FilterOperator is null
				OR	(	@AmountDownPaymentsIncluding_FilterText is not null AND @AmountDownPaymentsIncluding_FilterOperator is not null
					AND	( (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@AmountDownPaymentsIncluding_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[AmountDownPaymentsIncluding] is null) OR (i.[AmountDownPaymentsIncluding] = @AmountDownPaymentsIncluding_FilterText)))
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[AmountDownPaymentsIncluding] <> @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsLessThan AND i.[AmountDownPaymentsIncluding] < @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[AmountDownPaymentsIncluding] <= @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsGreaterThan AND i.[AmountDownPaymentsIncluding] > @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[AmountDownPaymentsIncluding] >= @AmountDownPaymentsIncluding_FilterText)
					)
				)
			)
			AND	(	@PrepaidDownPaymentsIncl_FilterText is null OR @PrepaidDownPaymentsIncl_FilterOperator is null
				OR	(	@PrepaidDownPaymentsIncl_FilterText is not null AND @PrepaidDownPaymentsIncl_FilterOperator is not null
					AND	( (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PrepaidDownPaymentsIncl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PrepaidDownPaymentsIncl] is null) OR (i.[PrepaidDownPaymentsIncl] = @PrepaidDownPaymentsIncl_FilterText)))
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PrepaidDownPaymentsIncl] <> @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThan AND i.[PrepaidDownPaymentsIncl] < @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PrepaidDownPaymentsIncl] <= @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PrepaidDownPaymentsIncl] > @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PrepaidDownPaymentsIncl] >= @PrepaidDownPaymentsIncl_FilterText)
					)
				)
			)
			AND	(	@NewPeriodicalDownPaymentsIncl_FilterText is null OR @NewPeriodicalDownPaymentsIncl_FilterOperator is null
				OR	(	@NewPeriodicalDownPaymentsIncl_FilterText is not null AND @NewPeriodicalDownPaymentsIncl_FilterOperator is not null
					AND	( (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@NewPeriodicalDownPaymentsIncl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[NewPeriodicalDownPaymentsIncl] is null) OR (i.[NewPeriodicalDownPaymentsIncl] = @NewPeriodicalDownPaymentsIncl_FilterText)))
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[NewPeriodicalDownPaymentsIncl] <> @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThan AND i.[NewPeriodicalDownPaymentsIncl] < @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[NewPeriodicalDownPaymentsIncl] <= @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[NewPeriodicalDownPaymentsIncl] > @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[NewPeriodicalDownPaymentsIncl] >= @NewPeriodicalDownPaymentsIncl_FilterText)
					)
				)
			)
			AND	(	@eClick_FilterText is null OR @eClick_FilterOperator is null
				OR	(	@eClick_FilterText is not null AND @eClick_FilterOperator is not null
					AND	( (@eClick_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@eClick_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[eClick] is null) OR (i.[eClick] = @eClick_FilterText)))
						OR (@eClick_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[eClick] <> @eClick_FilterText)
						OR (@eClick_FilterOperator = @FilterOperator_StartsWith AND i.[eClick] like @eClick_FilterText + '%')
						OR (@eClick_FilterOperator = @FilterOperator_EndsWith AND i.[eClick] like '%' + @eClick_FilterText)
						OR (@eClick_FilterOperator = @FilterOperator_Contains AND i.[eClick] like '%' + @eClick_FilterText + '%')
						OR (@eClick_FilterOperator = @FilterOperator_ISContainedIn AND @eClick_FilterText like '%' + i.[eClick] + '%')
						OR (@eClick_FilterOperator = @FilterOperator_DoesNotContain AND i.[eClick] not like '%' + @eClick_FilterText + '%')
					)
				)
			)
			
			AND	(	@FileName_FilterText is null OR @FileName_FilterOperator is null
				OR	(	@FileName_FilterText is not null AND @FileName_FilterOperator is not null
					AND	( (@FileName_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@FileName_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[FileName] is null) OR (i.[FileName] = @FileName_FilterText)))
						OR (@FileName_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[FileName] <> @FileName_FilterText)
						OR (@FileName_FilterOperator = @FilterOperator_StartsWith AND i.[FileName] like @FileName_FilterText + '%')
						OR (@FileName_FilterOperator = @FilterOperator_EndsWith AND i.[FileName] like '%' + @FileName_FilterText)
						OR (@FileName_FilterOperator = @FilterOperator_Contains AND i.[FileName] like '%' + @FileName_FilterText + '%')
						OR (@FileName_FilterOperator = @FilterOperator_ISContainedIn AND @FileName_FilterText like '%' + i.[FileName] + '%')
						OR (@FileName_FilterOperator = @FilterOperator_DoesNotContain AND i.[FileName] not like '%' + @FileName_FilterText + '%')
					)
				)
			)
		) AS Sub
		WHERE Sub.RowNumber BETWEEN @skip +1 AND @skip + @Take
	END
	ELSE 
	BEGIN
		 SELECT  i.[Id],
				--i.[FA],
				i.[EANToFAVectorId],
				i.[EAN],
				i.[DeliveryAddress_Street],
				i.[DeliveryAddress_PostalCode],
				i.[DeliveryAddress_City],
				i.[MeterNumber],
				i.[Provider],
				i.[InvoiceNumber],
				i.[InvoiceType],
				i.[InvoiceSubType],
				i.[InvoiceReference],
				i.[InvoiceCategory],
				i.[ClientNumber],
				i.[ClientName],
			--	i.[BuildingAddressId],
				i.[PeriodFrom],
				i.[PeriodTo],
				i.[PeriodLength],
				i.[InvoiceDate],
				i.[MaxPowerPeak],
				i.[MaxPowerOffPeak],
				i.[MaxPowerGlobal],
				i.[ConsumptionPeak],
				i.[ConsumptionOffPeak],
				i.[ConsumptionGlobalElectric],
				i.[ConsumptionTotal],
				i.[CaloricValue],
				i.[CosPhi],
				i.[MeterIndexPeak],
				i.[MeterIndexOffPeak],
				i.[MeterIndexPeakPrevious],
				i.[MeterIndexOffPeakPrevious],
				i.[MeterIndexGlobal],
				i.[MeterIndexGlobalPrevious],
				i.[ConsumptionGlobalGas],
				i.[PowerCost],
				i.[ConsumptionPeakCostExcl],
				i.[ConsumptionOffPeakCostExcl],
				i.[ConsumptionGlobalCostExcl],
				i.[ConsumptionTotalCostExcl],
				i.[GuarantyOfOriginCostExcl],
				i.[RenewableEnergyContributionExcl],
				i.[CogenContributionExcl],
				i.[MeterCost],
				i.[OtherEnergyCostExcl],
				i.[TotalEnergyCostExcl],
				i.[DistributionCostExcl],
				i.[TransmitionCostExcl],
				i.[TotalNetCostExcl],
				i.[TotalTaxesExcl],
				i.[TotalCostExclVAT],
				i.[VATPercentage],
				i.[TotalVATInvoice],
				i.[TotalVAT],
				i.[TotalCostInclVAT],
				i.[VATFreeAmountInvoice],
				i.[AmountDownPaymentsIncluding],
				i.[PrepaidDownPaymentsIncl],
				i.[NewPeriodicalDownPaymentsIncl],
				i.[eClick],
				i.[FileName],
				CASE WHEN @includePdf = 1 THEN i.[PdfFileContent]
				ELSE NULL
				END AS [PdfFileContent],
				(CASE WHEN [pdfFileContent] is null THEN CAST(0 as bit)
				  ELSE CAST(1 as bit)
				  END) AS [HasPdf],
				i.[InsertedBy],
				i.[InsertedOn],
				i.[UpdatedBy],
				i.[UpdatedOn]
		FROM [dbo].[Invoice] i		
			LEFT OUTER JOIN [dbo].[EANToFAVector] efv ON i.[EANToFAVectorId] = efv.Id
			LEFT OUTER JOIN [dbo].[BuildingAddress] ba ON efv.[FA] = ba.[FA]
		WHERE	
		(	@id is null
			OR	(	@id is not null
					AND	i.[Id] = @id))
			--AND (	@FA is null
			--	OR	(	@FA is not null
			--			AND	i.[FA] = @FA))
			AND (	@EANToFAVectorId is null
				OR	(	@EANToFAVectorId is not null
						AND	i.[EANToFAVectorId] = @EANToFAVectorId))
			AND (	@EAN is null
				OR	(	@EAN is not null
						AND	i.[EAN] = @EAN))
			AND (	@DeliveryAddress_Street is null
				OR	(	@DeliveryAddress_Street is not null
						AND	i.[DeliveryAddress_Street] = @DeliveryAddress_Street))
			AND (	@DeliveryAddress_PostalCode is null
				OR	(	@DeliveryAddress_PostalCode is not null
						AND	i.[DeliveryAddress_PostalCode] = @DeliveryAddress_PostalCode))
			AND (	@DeliveryAddress_City is null
				OR	(	@DeliveryAddress_City is not null
						AND	i.[DeliveryAddress_City] = @DeliveryAddress_City))
			AND (	@MeterNumber is null
				OR	(	@MeterNumber is not null
						AND	i.[MeterNumber] = @MeterNumber))
			AND (	@Provider is null
				OR	(	@Provider is not null
						AND	i.[Provider] = @Provider))
			AND (	@InvoiceNumber is null
				OR	(	@InvoiceNumber is not null
						AND	i.[InvoiceNumber] = @InvoiceNumber))
			AND (	@InvoiceType is null
				OR	(	@InvoiceType is not null
						AND	i.[InvoiceType] = @InvoiceType))
			AND (	@InvoiceSubType is null
				OR	(	@InvoiceSubType is not null
						AND	i.[InvoiceSubType] = @InvoiceSubType))
			AND (	@InvoiceReference is null
				OR	(	@InvoiceReference is not null
						AND	i.[InvoiceReference] = @InvoiceReference))
			AND (	@InvoiceCategory is null
				OR	(	InvoiceCategory is not null
						AND	i.[InvoiceCategory] = @InvoiceCategory))
			AND (	@ClientNumber is null
				OR	(	@ClientNumber is not null
						AND	i.[ClientNumber] = @ClientNumber))
			AND (	@ClientName is null
				OR	(	@ClientName is not null
						AND	i.[ClientName] = @ClientName))
			--AND (	@BuildingAddressId is null
			--	OR	(	@BuildingAddressId is not null
			--			AND	i.[BuildingAddressId] = @BuildingAddressId))
			AND (	@PeriodFrom is null
				OR	(	@PeriodFrom is not null
						AND	i.[PeriodFrom] = @PeriodFrom))
			AND (	@PeriodTo is null
				OR	(	@PeriodTo is not null
						AND	i.[PeriodTo] = @PeriodTo))
			AND (	@PeriodLength is null
				OR	(	@PeriodLength is not null
						AND	i.[PeriodLength] = @PeriodLength))
			AND (	@InvoiceDate is null
				OR	(	@InvoiceDate is not null
						AND	i.[InvoiceDate] = @InvoiceDate))
			AND (	@MaxPowerPeak is null
				OR	(	@MaxPowerPeak is not null
						AND	i.[MaxPowerPeak] = @MaxPowerPeak))
			AND (	@MaxPowerOffPeak is null
				OR	(	@MaxPowerOffPeak is not null
						AND	i.[MaxPowerOffPeak] = @MaxPowerOffPeak))
			AND (	@MaxPowerGlobal is null
				OR	(	@MaxPowerGlobal is not null
						AND	i.[MaxPowerGlobal] = @MaxPowerGlobal))
			AND (	@ConsumptionPeak is null
				OR	(	@ConsumptionPeak is not null
						AND	i.[ConsumptionPeak] = @ConsumptionPeak))
			AND (	@ConsumptionOffPeak is null
				OR	(	@ConsumptionOffPeak is not null
						AND	i.[ConsumptionOffPeak] = @ConsumptionOffPeak))
			AND (	@ConsumptionGlobalElectric is null
				OR	(	@ConsumptionGlobalElectric is not null
						AND	i.[ConsumptionGlobalElectric] = @ConsumptionGlobalElectric))
			AND (	@ConsumptionTotal is null
				OR	(	@ConsumptionTotal is not null
						AND	i.[ConsumptionTotal] = @ConsumptionTotal))
			AND (	@CaloricValue is null
				OR	(	@CaloricValue is not null
						AND	i.[CaloricValue] = @CaloricValue))
			AND (	@CosPhi is null
				OR	(	@CosPhi is not null
						AND	i.[CosPhi] = @CosPhi))
			AND (	@MeterIndexPeak is null
				OR	(	@MeterIndexPeak is not null
						AND	i.[MeterIndexPeak] = @MeterIndexPeak))
			AND (	@MeterIndexOffPeak is null
				OR	(	@MeterIndexOffPeak is not null
						AND	i.[MeterIndexOffPeak] = @MeterIndexOffPeak))
			AND (	@MeterIndexPeakPrevious is null
				OR	(	@MeterIndexPeakPrevious is not null
						AND	i.[MeterIndexPeakPrevious] = @MeterIndexPeakPrevious))
			AND (	@MeterIndexOffPeakPrevious is null
				OR	(	@MeterIndexOffPeakPrevious is not null
						AND	i.[MeterIndexOffPeakPrevious] = @MeterIndexOffPeakPrevious))
			AND (	@MeterIndexGlobal is null
				OR	(	@MeterIndexGlobal is not null
						AND	i.[MeterIndexGlobal] = @MeterIndexGlobal))
			AND (	@MeterIndexGlobalPrevious is null
				OR	(	@MeterIndexGlobalPrevious is not null
						AND	i.[MeterIndexGlobalPrevious] = @MeterIndexGlobalPrevious))
			AND (	@ConsumptionGlobalGas is null
				OR	(	@ConsumptionGlobalGas is not null
						AND	i.[ConsumptionGlobalGas] = @ConsumptionGlobalGas))
			AND (	@PowerCost is null
				OR	(	@PowerCost is not null
						AND	i.[PowerCost] = @PowerCost))
			AND (	@ConsumptionPeakCostExcl is null
				OR	(	@ConsumptionPeakCostExcl is not null
						AND	i.[ConsumptionPeakCostExcl] = @ConsumptionPeakCostExcl))
			AND (	@ConsumptionOffPeakCostExcl is null
				OR	(	@ConsumptionOffPeakCostExcl is not null
						AND	i.[ConsumptionOffPeakCostExcl] = @ConsumptionOffPeakCostExcl))
			AND (	@ConsumptionGlobalCostExcl is null
				OR	(	@ConsumptionGlobalCostExcl is not null
						AND	i.[ConsumptionGlobalCostExcl] = @ConsumptionGlobalCostExcl))
			AND (	@ConsumptionTotalCostExcl is null
				OR	(	@ConsumptionTotalCostExcl is not null
						AND	i.[ConsumptionTotalCostExcl] = @ConsumptionTotalCostExcl))
			AND (	@GuarantyOfOriginCostExcl is null
				OR	(	@GuarantyOfOriginCostExcl is not null
						AND	i.[GuarantyOfOriginCostExcl] = @GuarantyOfOriginCostExcl))
			AND (	@RenewableEnergyContributionExcl is null
				OR	(	@RenewableEnergyContributionExcl is not null
						AND	i.[RenewableEnergyContributionExcl] = @RenewableEnergyContributionExcl))
			AND (	@CogenContributionExcl is null
				OR	(	@CogenContributionExcl is not null
						AND	i.[CogenContributionExcl] = @CogenContributionExcl))
			AND (	@MeterCost is null
				OR	(	@MeterCost is not null
						AND	i.[MeterCost] = @MeterCost))
			AND (	@OtherEnergyCostExcl is null
				OR	(	@OtherEnergyCostExcl is not null
						AND	i.[OtherEnergyCostExcl] = @OtherEnergyCostExcl))
			AND (	@TotalEnergyCostExcl is null
				OR	(	@TotalEnergyCostExcl is not null
						AND	i.[TotalEnergyCostExcl] = @TotalEnergyCostExcl))
			AND (	@DistributionCostExcl is null
				OR	(	@DistributionCostExcl is not null
						AND	i.[DistributionCostExcl] = @DistributionCostExcl))
			AND (	@TransmitionCostExcl is null
				OR	(	@TransmitionCostExcl is not null
						AND	i.[TransmitionCostExcl] = @TransmitionCostExcl))
			AND (	@TotalNetCostExcl is null
				OR	(	@TotalNetCostExcl is not null
						AND	i.[TotalNetCostExcl] = @TotalNetCostExcl))
			AND (	@TotalTaxesExcl is null
				OR	(	@TotalTaxesExcl is not null
						AND	i.[TotalTaxesExcl] = @TotalTaxesExcl))
			AND (	@TotalCostExclVAT is null
				OR	(	@TotalCostExclVAT is not null
						AND	i.[TotalCostExclVAT] = @TotalCostExclVAT))
			AND (	@VATPercentage is null
				OR	(	@VATPercentage is not null
						AND	i.[VATPercentage] = @VATPercentage))
			AND (	@TotalVATInvoice is null
				OR	(	@TotalVATInvoice is not null
						AND	i.[TotalVATInvoice] = @TotalVATInvoice))
			AND (	@TotalVAT is null
				OR	(	@TotalVAT is not null
						AND	i.[TotalVAT] = @TotalVAT))
			AND (	@TotalCostInclVAT is null
				OR	(	@TotalCostInclVAT is not null
						AND	i.[TotalCostInclVAT] = @TotalCostInclVAT))
			AND (	@VATFreeAmountInvoice is null
				OR	(	@VATFreeAmountInvoice is not null
						AND	i.[VATFreeAmountInvoice] = @VATFreeAmountInvoice))
			AND (	@AmountDownPaymentsIncluding is null
				OR	(	@AmountDownPaymentsIncluding is not null
						AND	i.[AmountDownPaymentsIncluding] = @AmountDownPaymentsIncluding))
			AND (	@PrepaidDownPaymentsIncl is null
				OR	(	@PrepaidDownPaymentsIncl is not null
						AND	i.[PrepaidDownPaymentsIncl] = @PrepaidDownPaymentsIncl))
			AND (	@NewPeriodicalDownPaymentsIncl is null
				OR	(	@NewPeriodicalDownPaymentsIncl is not null
						AND	i.[NewPeriodicalDownPaymentsIncl] = @NewPeriodicalDownPaymentsIncl))
			AND (	@eClick is null
				OR	(	@eClick is not null
						AND	i.[eClick] = @eClick))
			AND (	@FileName is null
				OR	(	@FileName is not null
						AND	i.[FileName] = @FileName))
			AND	(	@EANToFAVector_FA_FilterText is null OR @EANToFAVector_FA_FilterOperator is null
				OR	(	@EANToFAVector_FA_FilterText is not null AND @EANToFAVector_FA_FilterOperator is not null
					AND	( (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_FA_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[FA] is null) OR (efv.[FA] = @EANToFAVector_FA_FilterText)))
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[FA] <> @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsLessThan AND efv.[FA] < @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND efv.[FA] <= @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsGreaterThan AND efv.[FA] > @EANToFAVector_FA_FilterText)
						OR (@EANToFAVector_FA_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND efv.[FA] >= @EANToFAVector_FA_FilterText)
					)
				)
			)
			AND	(	@EANToFAVector_Vector_FilterText is null OR @EANToFAVector_Vector_FilterOperator is null
				OR	(	@EANToFAVector_Vector_FilterText is not null AND @EANToFAVector_Vector_FilterOperator is not null
					AND	( (@EANToFAVector_Vector_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_Vector_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[Vector] is null) OR (efv.[Vector] = @EANToFAVector_Vector_FilterText)))
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[Vector] <> @EANToFAVector_Vector_FilterText)
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_StartsWith AND efv.[Vector] like @EANToFAVector_Vector_FilterText + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_EndsWith AND efv.[Vector] like '%' + @EANToFAVector_Vector_FilterText)
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_Contains AND efv.[Vector] like '%' + @EANToFAVector_Vector_FilterText + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_ISContainedIn AND @EANToFAVector_Vector_FilterText like '%' + efv.[Vector] + '%')
						OR (@EANToFAVector_Vector_FilterOperator = @FilterOperator_DoesNotContain AND efv.[Vector] not like '%' + @EANToFAVector_Vector_FilterText + '%')
					)
				)
			)
			AND	(	@EANToFAVector_SubVector_FilterText is null OR @EANToFAVector_SubVector_FilterOperator is null
				OR	(	@EANToFAVector_SubVector_FilterText is not null AND @EANToFAVector_SubVector_FilterOperator is not null
					AND	( (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_SubVector_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[SubVector] is null) OR (efv.[SubVector] = @EANToFAVector_SubVector_FilterText)))
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[SubVector] <> @EANToFAVector_SubVector_FilterText)
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_StartsWith AND efv.[SubVector] like @EANToFAVector_SubVector_FilterText + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_EndsWith AND efv.[SubVector] like '%' + @EANToFAVector_SubVector_FilterText)
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_Contains AND efv.[SubVector] like '%' + @EANToFAVector_SubVector_FilterText + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_ISContainedIn AND @EANToFAVector_SubVector_FilterText like '%' + efv.[SubVector] + '%')
						OR (@EANToFAVector_SubVector_FilterOperator = @FilterOperator_DoesNotContain AND efv.[SubVector] not like '%' + @EANToFAVector_SubVector_FilterText + '%')
					)
				)
			)
			AND	(	@EANToFAVector_State_FilterText is null OR @EANToFAVector_State_FilterOperator is null
				OR	(	@EANToFAVector_State_FilterText is not null AND @EANToFAVector_State_FilterOperator is not null
					AND	( (@EANToFAVector_State_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_State_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[State] is null) OR (efv.[State] = @EANToFAVector_State_FilterText)))
						OR (@EANToFAVector_State_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[State] <> @EANToFAVector_State_FilterText)
					)
				)
			)
			
			AND	(	@EANToFAVector_ClosedOn_FilterText is null OR @EANToFAVector_ClosedOn_FilterOperator is null
				OR	(	@EANToFAVector_ClosedOn_FilterText is not null AND @EANToFAVector_ClosedOn_FilterOperator is not null
					AND	( (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EANToFAVector_ClosedOn_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND efv.[ClosedOn] is null) OR (efv.[ClosedOn] = @EANToFAVector_ClosedOn_FilterText)))
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsNotEqualTo AND efv.[ClosedOn] <> @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsLessThan AND efv.[ClosedOn] < @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND efv.[ClosedOn] <= @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsGreaterThan AND efv.[ClosedOn] > @EANToFAVector_ClosedOn_FilterText)
						OR (@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND efv.[ClosedOn] >= @EANToFAVector_ClosedOn_FilterText)
						OR(@EANToFAVector_ClosedOn_FilterOperator = @FilterOperator_IsBetween AND efv.[ClosedOn] BETWEEN @EANToFAVector_ClosedOn_FilterText AND @EANToFAVector_ClosedOn_FilterText2))
					)
				)
			
			AND	(	@EAN_FilterText is null OR @EAN_FilterOperator is null
				OR	(	@EAN_FilterText is not null AND @EAN_FilterOperator is not null
					AND	( (@EAN_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@EAN_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[EAN] is null) OR (i.[EAN] = @EAN_FilterText)))
						OR (@EAN_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[EAN] <> @EAN_FilterText)
						OR (@EAN_FilterOperator = @FilterOperator_StartsWith AND i.[EAN] like @EAN_FilterText + '%')
						OR (@EAN_FilterOperator = @FilterOperator_EndsWith AND i.[EAN] like '%' + @EAN_FilterText)
						OR (@EAN_FilterOperator = @FilterOperator_Contains AND i.[EAN] like '%' + @EAN_FilterText + '%')
						OR (@EAN_FilterOperator = @FilterOperator_ISContainedIn AND @EAN_FilterText like '%' + i.[EAN] + '%')
						OR (@EAN_FilterOperator = @FilterOperator_DoesNotContain AND i.[EAN] not like '%' + @EAN_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_Street_FilterText is null OR @DeliveryAddress_Street_FilterOperator is null
				OR	(	@DeliveryAddress_Street_FilterText is not null AND @DeliveryAddress_Street_FilterOperator is not null
					AND	( (@DeliveryAddress_Street_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_Street_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_Street] is null) OR (i.[DeliveryAddress_Street] = @DeliveryAddress_Street_FilterText)))
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_Street] <> @DeliveryAddress_Street_FilterText)
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_Street] like @DeliveryAddress_Street_FilterText + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_Street] like '%' + @DeliveryAddress_Street_FilterText)
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_Street] like '%' + @DeliveryAddress_Street_FilterText + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_Street_FilterText like '%' + i.[DeliveryAddress_Street] + '%')
						OR (@DeliveryAddress_Street_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_Street] not like '%' + @DeliveryAddress_Street_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_PostalCode_FilterText is null OR @DeliveryAddress_PostalCode_FilterOperator is null
				OR	(	@DeliveryAddress_PostalCode_FilterText is not null AND @DeliveryAddress_PostalCode_FilterOperator is not null
					AND	( (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_PostalCode_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_PostalCode] is null) OR (i.[DeliveryAddress_PostalCode] = @DeliveryAddress_PostalCode_FilterText)))
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_PostalCode] <> @DeliveryAddress_PostalCode_FilterText)
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_PostalCode] like @DeliveryAddress_PostalCode_FilterText + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_PostalCode] like '%' + @DeliveryAddress_PostalCode_FilterText)
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_PostalCode] like '%' + @DeliveryAddress_PostalCode_FilterText + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_PostalCode_FilterText like '%' + i.[DeliveryAddress_PostalCode] + '%')
						OR (@DeliveryAddress_PostalCode_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_PostalCode] not like '%' + @DeliveryAddress_PostalCode_FilterText + '%')
					)
				)
			)
			AND	(	@DeliveryAddress_City_FilterText is null OR @DeliveryAddress_City_FilterOperator is null
				OR	(	@DeliveryAddress_City_FilterText is not null AND @DeliveryAddress_City_FilterOperator is not null
					AND	( (@DeliveryAddress_City_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DeliveryAddress_City_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DeliveryAddress_City] is null) OR (i.[DeliveryAddress_City] = @DeliveryAddress_City_FilterText)))
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DeliveryAddress_City] <> @DeliveryAddress_City_FilterText)
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_StartsWith AND i.[DeliveryAddress_City] like @DeliveryAddress_City_FilterText + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_EndsWith AND i.[DeliveryAddress_City] like '%' + @DeliveryAddress_City_FilterText)
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_Contains AND i.[DeliveryAddress_City] like '%' + @DeliveryAddress_City_FilterText + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_ISContainedIn AND @DeliveryAddress_City_FilterText like '%' + i.[DeliveryAddress_City] + '%')
						OR (@DeliveryAddress_City_FilterOperator = @FilterOperator_DoesNotContain AND i.[DeliveryAddress_City] not like '%' + @DeliveryAddress_City_FilterText + '%')
					)
				)
			)
			AND	(	@MeterNumber_FilterText is null OR @MeterNumber_FilterOperator is null
				OR	(	@MeterNumber_FilterText is not null AND @MeterNumber_FilterOperator is not null
					AND	( (@MeterNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterNumber] is null) OR (i.[MeterNumber] = @MeterNumber_FilterText)))
						OR (@MeterNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterNumber] <> @MeterNumber_FilterText)
						OR (@MeterNumber_FilterOperator = @FilterOperator_StartsWith AND i.[MeterNumber] like @MeterNumber_FilterText + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_EndsWith AND i.[MeterNumber] like '%' + @MeterNumber_FilterText)
						OR (@MeterNumber_FilterOperator = @FilterOperator_Contains AND i.[MeterNumber] like '%' + @MeterNumber_FilterText + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_ISContainedIn AND @MeterNumber_FilterText like '%' + i.[MeterNumber] + '%')
						OR (@MeterNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[MeterNumber] not like '%' + @MeterNumber_FilterText + '%')
					)
				)
			)
			AND	(	@Provider_FilterText is null OR @Provider_FilterOperator is null
				OR	(	@Provider_FilterText is not null AND @Provider_FilterOperator is not null
					AND	( (@Provider_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@Provider_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[Provider] is null) OR (i.[Provider] = @Provider_FilterText)))
						OR (@Provider_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[Provider] <> @Provider_FilterText)
						OR (@Provider_FilterOperator = @FilterOperator_StartsWith AND i.[Provider] like @Provider_FilterText + '%')
						OR (@Provider_FilterOperator = @FilterOperator_EndsWith AND i.[Provider] like '%' + @Provider_FilterText)
						OR (@Provider_FilterOperator = @FilterOperator_Contains AND i.[Provider] like '%' + @Provider_FilterText + '%')
						OR (@Provider_FilterOperator = @FilterOperator_ISContainedIn AND @Provider_FilterText like '%' + i.[Provider] + '%')
						OR (@Provider_FilterOperator = @FilterOperator_DoesNotContain AND i.[Provider] not like '%' + @Provider_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceNumber_FilterText is null OR @InvoiceNumber_FilterOperator is null
				OR	(	@InvoiceNumber_FilterText is not null AND @InvoiceNumber_FilterOperator is not null
					AND	( (@InvoiceNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceNumber] is null) OR (i.[InvoiceNumber] = @InvoiceNumber_FilterText)))
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceNumber] <> @InvoiceNumber_FilterText)
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceNumber] like @InvoiceNumber_FilterText + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceNumber] like '%' + @InvoiceNumber_FilterText)
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_Contains AND i.[InvoiceNumber] like '%' + @InvoiceNumber_FilterText + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceNumber_FilterText like '%' + i.[InvoiceNumber] + '%')
						OR (@InvoiceNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceNumber] not like '%' + @InvoiceNumber_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceType_FilterText is null OR @InvoiceType_FilterOperator is null
				OR	(	@InvoiceType_FilterText is not null AND @InvoiceType_FilterOperator is not null
					AND	( (@InvoiceType_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceType_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceType] is null) OR (i.[InvoiceType] = @InvoiceType_FilterText)))
						OR (@InvoiceType_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceType] <> @InvoiceType_FilterText)
						OR (@InvoiceType_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceType] like @InvoiceType_FilterText + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceType] like '%' + @InvoiceType_FilterText)
						OR (@InvoiceType_FilterOperator = @FilterOperator_Contains AND i.[InvoiceType] like '%' + @InvoiceType_FilterText + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceType_FilterText like '%' + i.[InvoiceType] + '%')
						OR (@InvoiceType_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceType] not like '%' + @InvoiceType_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceSubType_FilterText is null OR @InvoiceSubType_FilterOperator is null
				OR	(	@InvoiceSubType_FilterText is not null AND @InvoiceSubType_FilterOperator is not null
					AND	( (@InvoiceSubType_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceSubType_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceSubType] is null) OR (i.[InvoiceSubType] = @InvoiceSubType_FilterText)))
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceSubType] <> @InvoiceSubType_FilterText)
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceSubType] like @InvoiceSubType_FilterText + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceSubType] like '%' + @InvoiceSubType_FilterText)
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_Contains AND i.[InvoiceSubType] like '%' + @InvoiceSubType_FilterText + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceSubType_FilterText like '%' + i.[InvoiceSubType] + '%')
						OR (@InvoiceSubType_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceSubType] not like '%' + @InvoiceSubType_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceReference_FilterText is null OR @InvoiceReference_FilterOperator is null
				OR	(	@InvoiceReference_FilterText is not null AND @InvoiceReference_FilterOperator is not null
					AND	( (@InvoiceReference_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceReference_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceReference] is null) OR (i.[InvoiceReference] = @InvoiceReference_FilterText)))
						OR (@InvoiceReference_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceReference] <> @InvoiceReference_FilterText)
						OR (@InvoiceReference_FilterOperator = @FilterOperator_StartsWith AND i.[InvoiceReference] like @InvoiceReference_FilterText + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_EndsWith AND i.[InvoiceReference] like '%' + @InvoiceReference_FilterText)
						OR (@InvoiceReference_FilterOperator = @FilterOperator_Contains AND i.[InvoiceReference] like '%' + @InvoiceReference_FilterText + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_ISContainedIn AND @InvoiceReference_FilterText like '%' + i.[InvoiceReference] + '%')
						OR (@InvoiceReference_FilterOperator = @FilterOperator_DoesNotContain AND i.[InvoiceReference] not like '%' + @InvoiceReference_FilterText + '%')
					)
				)
			)
			AND	(	@InvoiceCategory_FilterText is null OR @InvoiceCategory_FilterOperator is null
				OR	(	@InvoiceCategory_FilterText is not null AND @InvoiceCategory_FilterOperator is not null
					AND	( (@InvoiceCategory_FilterOperator = @FilterOperator_IsEqualTo AND (i.[InvoiceCategory] = @InvoiceCategory_FilterText))
						OR (@InvoiceCategory_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceCategory] <> @InvoiceCategory_FilterText)
					)
				)
			)
			AND	(	@ClientNumber_FilterText is null OR @ClientNumber_FilterOperator is null
				OR	(	@ClientNumber_FilterText is not null AND @ClientNumber_FilterOperator is not null
					AND	( (@ClientNumber_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ClientNumber_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ClientNumber] is null) OR (i.[ClientNumber] = @ClientNumber_FilterText)))
						OR (@ClientNumber_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ClientNumber] <> @ClientNumber_FilterText)
						OR (@ClientNumber_FilterOperator = @FilterOperator_StartsWith AND i.[ClientNumber] like @ClientNumber_FilterText + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_EndsWith AND i.[ClientNumber] like '%' + @ClientNumber_FilterText)
						OR (@ClientNumber_FilterOperator = @FilterOperator_Contains AND i.[ClientNumber] like '%' + @ClientNumber_FilterText + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_ISContainedIn AND @ClientNumber_FilterText like '%' + i.[ClientNumber] + '%')
						OR (@ClientNumber_FilterOperator = @FilterOperator_DoesNotContain AND i.[ClientNumber] not like '%' + @ClientNumber_FilterText + '%')
					)
				)
			)
			AND	(	@ClientName_FilterText is null OR @ClientName_FilterOperator is null
				OR	(	@ClientName_FilterText is not null AND @ClientName_FilterOperator is not null
					AND	( (@ClientName_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ClientName_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ClientName] is null) OR (i.[ClientName] = @ClientName_FilterText)))
						OR (@ClientName_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ClientName] <> @ClientName_FilterText)
						OR (@ClientName_FilterOperator = @FilterOperator_StartsWith AND i.[ClientName] like @ClientName_FilterText + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_EndsWith AND i.[ClientName] like '%' + @ClientName_FilterText)
						OR (@ClientName_FilterOperator = @FilterOperator_Contains AND i.[ClientName] like '%' + @ClientName_FilterText + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_ISContainedIn AND @ClientName_FilterText like '%' + i.[ClientName] + '%')
						OR (@ClientName_FilterOperator = @FilterOperator_DoesNotContain AND i.[ClientName] not like '%' + @ClientName_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_Name_FilterText is null OR @BuildingAddress_Name_FilterOperator is null
				OR	(	@BuildingAddress_Name_FilterText is not null AND @BuildingAddress_Name_FilterOperator is not null
					AND	( (@BuildingAddress_Name_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_Name_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[Name] is null) OR (ba.[Name] = @BuildingAddress_Name_FilterText)))
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[Name] <> @BuildingAddress_Name_FilterText)
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_StartsWith AND ba.[Name] like @BuildingAddress_Name_FilterText + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_EndsWith AND ba.[Name] like '%' + @BuildingAddress_Name_FilterText)
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_Contains AND ba.[Name] like '%' + @BuildingAddress_Name_FilterText + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_Name_FilterText like '%' + ba.[Name] + '%')
						OR (@BuildingAddress_Name_FilterOperator = @FilterOperator_DoesNotContain AND ba.[Name] not like '%' + @BuildingAddress_Name_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_Street_FilterText is null OR @BuildingAddress_Street_FilterOperator is null
				OR	(	@BuildingAddress_Street_FilterText is not null AND @BuildingAddress_Street_FilterOperator is not null
					AND	( (@BuildingAddress_Street_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_Street_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[Street] is null) OR (ba.[Street] = @BuildingAddress_Street_FilterText)))
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[Street] <> @BuildingAddress_Street_FilterText)
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_StartsWith AND ba.[Street] like @BuildingAddress_Street_FilterText + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_EndsWith AND ba.[Street] like '%' + @BuildingAddress_Street_FilterText)
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_Contains AND ba.[Street] like '%' + @BuildingAddress_Street_FilterText + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_Street_FilterText like '%' + ba.[Street] + '%')
						OR (@BuildingAddress_Street_FilterOperator = @FilterOperator_DoesNotContain AND ba.[Street] not like '%' + @BuildingAddress_Street_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_PostalCode_FilterText is null OR @BuildingAddress_PostalCode_FilterOperator is null
				OR	(	@BuildingAddress_PostalCode_FilterText is not null AND @BuildingAddress_PostalCode_FilterOperator is not null
					AND	( (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_PostalCode_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[PostalCode] is null) OR (ba.[PostalCode] = @BuildingAddress_PostalCode_FilterText)))
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[PostalCode] <> @BuildingAddress_PostalCode_FilterText)
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_StartsWith AND ba.[PostalCode] like @BuildingAddress_PostalCode_FilterText + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_EndsWith AND ba.[PostalCode] like '%' + @BuildingAddress_PostalCode_FilterText)
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_Contains AND ba.[PostalCode] like '%' + @BuildingAddress_PostalCode_FilterText + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_PostalCode_FilterText like '%' + ba.[PostalCode] + '%')
						OR (@BuildingAddress_PostalCode_FilterOperator = @FilterOperator_DoesNotContain AND ba.[PostalCode] not like '%' + @BuildingAddress_PostalCode_FilterText + '%')
					)
				)
			)
			AND	(	@BuildingAddress_City_FilterText is null OR @BuildingAddress_City_FilterOperator is null
				OR	(	@BuildingAddress_City_FilterText is not null AND @BuildingAddress_City_FilterOperator is not null
					AND	( (@BuildingAddress_City_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@BuildingAddress_City_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND ba.[City] is null) OR (ba.[City] = @BuildingAddress_City_FilterText)))
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_IsNotEqualTo AND ba.[City] <> @BuildingAddress_City_FilterText)
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_StartsWith AND ba.[City] like @BuildingAddress_City_FilterText + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_EndsWith AND ba.[City] like '%' + @BuildingAddress_City_FilterText)
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_Contains AND ba.[City] like '%' + @BuildingAddress_City_FilterText + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_ISContainedIn AND @BuildingAddress_City_FilterText like '%' + ba.[City] + '%')
						OR (@BuildingAddress_City_FilterOperator = @FilterOperator_DoesNotContain AND ba.[City] not like '%' + @BuildingAddress_City_FilterText + '%')
					)
				)
			)
			AND	(	@PeriodFrom_FilterText is null OR @PeriodFrom_FilterOperator is null
				OR	(	@PeriodFrom_FilterText is not null AND @PeriodFrom_FilterOperator is not null
					AND	( (@PeriodFrom_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodFrom_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodFrom] is null) OR (i.[PeriodFrom] = @PeriodFrom_FilterText)))
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodFrom] <> @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodFrom] < @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodFrom] <= @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodFrom] > @PeriodFrom_FilterText)
						OR (@PeriodFrom_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodFrom] >= @PeriodFrom_FilterText)
						OR(@PeriodFrom_FilterOperator = @FilterOperator_IsBetween AND i.[PeriodFrom] BETWEEN @PeriodFrom_FilterText AND @PeriodFrom_FilterText2)
					)
				)
			)
			AND	(	@PeriodTo_FilterText is null OR @PeriodTo_FilterOperator is null
				OR	(	@PeriodTo_FilterText is not null AND @PeriodTo_FilterOperator is not null
					AND	( (@PeriodTo_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodTo_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodTo] is null) OR (i.[PeriodTo] = @PeriodTo_FilterText)))
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodTo] <> @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodTo] < @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodTo] <= @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodTo] > @PeriodTo_FilterText)
						OR (@PeriodTo_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodTo] >= @PeriodTo_FilterText)
						OR(@PeriodTo_FilterOperator = @FilterOperator_IsBetween AND i.[PeriodTo] BETWEEN @PeriodTo_FilterText AND @PeriodTo_FilterText2)
					)
				)
			)
			AND	(	@PeriodLength_FilterText is null OR @PeriodLength_FilterOperator is null
				OR	(	@PeriodLength_FilterText is not null AND @PeriodLength_FilterOperator is not null
					AND	( (@PeriodLength_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PeriodLength_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PeriodLength] is null) OR (i.[PeriodLength] = @PeriodLength_FilterText)))
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PeriodLength] <> @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsLessThan AND i.[PeriodLength] < @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PeriodLength] <= @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PeriodLength] > @PeriodLength_FilterText)
						OR (@PeriodLength_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PeriodLength] >= @PeriodLength_FilterText)
					)
				)
			)
			AND	(	@InvoiceDate_FilterText is null OR @InvoiceDate_FilterOperator is null
				OR	(	@InvoiceDate_FilterText is not null AND @InvoiceDate_FilterOperator is not null
					AND	( (@InvoiceDate_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@InvoiceDate_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[InvoiceDate] is null) OR (i.[InvoiceDate] = @InvoiceDate_FilterText)))
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[InvoiceDate] <> @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsLessThan AND i.[InvoiceDate] < @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[InvoiceDate] <= @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsGreaterThan AND i.[InvoiceDate] > @InvoiceDate_FilterText)
						OR (@InvoiceDate_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[InvoiceDate] >= @InvoiceDate_FilterText)
						OR(@InvoiceDate_FilterOperator = @FilterOperator_IsBetween AND i.[InvoiceDate] BETWEEN @InvoiceDate_FilterText AND @InvoiceDate_FilterText2)
					)
				)
			)
			AND	(	@MaxPowerPeak_FilterText is null OR @MaxPowerPeak_FilterOperator is null
				OR	(	@MaxPowerPeak_FilterText is not null AND @MaxPowerPeak_FilterOperator is not null
					AND	( (@MaxPowerPeak_FilterOperator = @FilterOperator_IsEqualTo  AND ((UPPER(@MaxPowerPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerPeak] is null) OR (i.[MaxPowerPeak] = @MaxPowerPeak_FilterText)))
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerPeak] <> @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerPeak] < @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerPeak] <= @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerPeak] > @MaxPowerPeak_FilterText)
						OR (@MaxPowerPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerPeak] >= @MaxPowerPeak_FilterText)
					)
				)
			)
			AND	(	@MaxPowerOffPeak_FilterText is null OR @MaxPowerOffPeak_FilterOperator is null
				OR	(	@MaxPowerOffPeak_FilterText is not null AND @MaxPowerOffPeak_FilterOperator is not null
					AND	( (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MaxPowerOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerOffPeak] is null) OR (i.[MaxPowerOffPeak] = @MaxPowerOffPeak_FilterText)))
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerOffPeak] <> @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerOffPeak] < @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerOffPeak] <= @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerOffPeak] > @MaxPowerOffPeak_FilterText)
						OR (@MaxPowerOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerOffPeak] >= @MaxPowerOffPeak_FilterText)
					)
				)
			)
			AND	(	@MaxPowerGlobal_FilterText is null OR @MaxPowerGlobal_FilterOperator is null
				OR	(	@MaxPowerGlobal_FilterText is not null AND @MaxPowerGlobal_FilterOperator is not null
					AND	( (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MaxPowerGlobal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MaxPowerGlobal] is null) OR (i.[MaxPowerGlobal] = @MaxPowerGlobal_FilterText)))
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MaxPowerGlobal] <> @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsLessThan AND i.[MaxPowerGlobal] < @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MaxPowerGlobal] <= @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MaxPowerGlobal] > @MaxPowerGlobal_FilterText)
						OR (@MaxPowerGlobal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MaxPowerGlobal] >= @MaxPowerGlobal_FilterText)
					)
				)
			)
			AND	(	@ConsumptionPeak_FilterText is null OR @ConsumptionPeak_FilterOperator is null
				OR	(	@ConsumptionPeak_FilterText is not null AND @ConsumptionPeak_FilterOperator is not null
					AND	( (@ConsumptionPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionPeak] is null) OR (i.[ConsumptionPeak] = @ConsumptionPeak_FilterText)))
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionPeak] <> @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionPeak] < @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionPeak] <= @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionPeak] > @ConsumptionPeak_FilterText)
						OR (@ConsumptionPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionPeak] >= @ConsumptionPeak_FilterText)
					)
				)
			)
			AND	(	@ConsumptionOffPeak_FilterText is null OR @ConsumptionOffPeak_FilterOperator is null
				OR	(	@ConsumptionOffPeak_FilterText is not null AND @ConsumptionOffPeak_FilterOperator is not null
					AND	( (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionOffPeak] is null) OR (i.[ConsumptionOffPeak] = @ConsumptionOffPeak_FilterText)))
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionOffPeak] <> @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionOffPeak] < @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionOffPeak] <= @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionOffPeak] > @ConsumptionOffPeak_FilterText)
						OR (@ConsumptionOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionOffPeak] >= @ConsumptionOffPeak_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalElectric_FilterText is null OR @ConsumptionGlobalElectric_FilterOperator is null
				OR	(	@ConsumptionGlobalElectric_FilterText is not null AND @ConsumptionGlobalElectric_FilterOperator is not null
					AND	( (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalElectric_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalElectric] is null) OR (i.[ConsumptionGlobalElectric] = @ConsumptionGlobalElectric_FilterText)))
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalElectric] <> @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalElectric] < @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalElectric] <= @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalElectric] > @ConsumptionGlobalElectric_FilterText)
						OR (@ConsumptionGlobalElectric_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalElectric] >= @ConsumptionGlobalElectric_FilterText)
					)
				)
			)
			AND	(	@ConsumptionTotal_FilterText is null OR @ConsumptionTotal_FilterOperator is null
				OR	(	@ConsumptionTotal_FilterText is not null AND @ConsumptionTotal_FilterOperator is not null
					AND	( (@ConsumptionTotal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionTotal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionTotal] is null) OR (i.[ConsumptionTotal] = @ConsumptionTotal_FilterText)))
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionTotal] <> @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionTotal] < @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionTotal] <= @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionTotal] > @ConsumptionTotal_FilterText)
						OR (@ConsumptionTotal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionTotal] >= @ConsumptionTotal_FilterText)
					)
				)
			)
			AND	(	@CaloricValue_FilterText is null OR @CaloricValue_FilterOperator is null
				OR	(	@CaloricValue_FilterText is not null AND @CaloricValue_FilterOperator is not null
					AND	( (@CaloricValue_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CaloricValue_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CaloricValue] is null) OR (i.[CaloricValue] = @CaloricValue_FilterText)))
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CaloricValue] <> @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsLessThan AND i.[CaloricValue] < @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CaloricValue] <= @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CaloricValue] > @CaloricValue_FilterText)
						OR (@CaloricValue_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CaloricValue] >= @CaloricValue_FilterText)
					)
				)
			)
			AND	(	@CosPhi_FilterText is null OR @CosPhi_FilterOperator is null
				OR	(	@CosPhi_FilterText is not null AND @CosPhi_FilterOperator is not null
					AND	( (@CosPhi_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CosPhi_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CosPhi] is null) OR (i.[CosPhi] = @CosPhi_FilterText)))
						OR (@CosPhi_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CosPhi] <> @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsLessThan AND i.[CosPhi] < @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CosPhi] <= @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CosPhi] > @CosPhi_FilterText)
						OR (@CosPhi_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CosPhi] >= @CosPhi_FilterText)
					)
				)
			)
			AND	(	@MeterIndexPeak_FilterText is null OR @MeterIndexPeak_FilterOperator is null
				OR	(	@MeterIndexPeak_FilterText is not null AND @MeterIndexPeak_FilterOperator is not null
					AND	( (@MeterIndexPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexPeak] is null) OR (i.[MeterIndexPeak] = @MeterIndexPeak_FilterText)))
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexPeak] <> @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexPeak] < @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexPeak] <= @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexPeak] > @MeterIndexPeak_FilterText)
						OR (@MeterIndexPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexPeak] >= @MeterIndexPeak_FilterText)
					)
				)
			)
			AND	(	@MeterIndexOffPeak_FilterText is null OR @MeterIndexOffPeak_FilterOperator is null
				OR	(	@MeterIndexOffPeak_FilterText is not null AND @MeterIndexOffPeak_FilterOperator is not null
					AND	( (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexOffPeak_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexOffPeak] is null) OR (i.[MeterIndexOffPeak] = @MeterIndexOffPeak_FilterText)))
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexOffPeak] <> @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexOffPeak] < @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexOffPeak] <= @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexOffPeak] > @MeterIndexOffPeak_FilterText)
						OR (@MeterIndexOffPeak_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexOffPeak] >= @MeterIndexOffPeak_FilterText)
					)
				)
			)
			AND	(	@MeterIndexPeakPrevious_FilterText is null OR @MeterIndexPeakPrevious_FilterOperator is null
				OR	(	@MeterIndexPeakPrevious_FilterText is not null AND @MeterIndexPeakPrevious_FilterOperator is not null
					AND	( (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexPeakPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexPeakPrevious] is null) OR (i.[MeterIndexPeakPrevious] = @MeterIndexPeakPrevious_FilterText)))
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexPeakPrevious] <> @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexPeakPrevious] < @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexPeakPrevious] <= @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexPeakPrevious] > @MeterIndexPeakPrevious_FilterText)
						OR (@MeterIndexPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexPeakPrevious] >= @MeterIndexPeakPrevious_FilterText)
					)
				)
			)
			AND	(	@MeterIndexOffPeakPrevious_FilterText is null OR @MeterIndexOffPeakPrevious_FilterOperator is null
				OR	(	@MeterIndexOffPeakPrevious_FilterText is not null AND @MeterIndexOffPeakPrevious_FilterOperator is not null
					AND	( (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexOffPeakPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexOffPeakPrevious] is null) OR (i.[MeterIndexOffPeakPrevious] = @MeterIndexOffPeakPrevious_FilterText)))
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexOffPeakPrevious] <> @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexOffPeakPrevious] < @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexOffPeakPrevious] <= @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexOffPeakPrevious] > @MeterIndexOffPeakPrevious_FilterText)
						OR (@MeterIndexOffPeakPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexOffPeakPrevious] >= @MeterIndexOffPeakPrevious_FilterText)
					)
				)
			)
			AND	(	@MeterIndexGlobal_FilterText is null OR @MeterIndexGlobal_FilterOperator is null
				OR	(	@MeterIndexGlobal_FilterText is not null AND @MeterIndexGlobal_FilterOperator is not null
					AND	( (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexGlobal_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexGlobal] is null) OR (i.[MeterIndexGlobal] = @MeterIndexGlobal_FilterText)))
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexGlobal] <> @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexGlobal] < @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexGlobal] <= @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexGlobal] > @MeterIndexGlobal_FilterText)
						OR (@MeterIndexGlobal_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexGlobal] >= @MeterIndexGlobal_FilterText)
					)
				)
			)
			AND	(	@MeterIndexGlobalPrevious_FilterText is null OR @MeterIndexGlobalPrevious_FilterOperator is null
				OR	(	@MeterIndexGlobalPrevious_FilterText is not null AND @MeterIndexGlobalPrevious_FilterOperator is not null
					AND	( (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterIndexGlobalPrevious_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterIndexGlobalPrevious] is null) OR (i.[MeterIndexGlobalPrevious] = @MeterIndexGlobalPrevious_FilterText)))
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterIndexGlobalPrevious] <> @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterIndexGlobalPrevious] < @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterIndexGlobalPrevious] <= @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterIndexGlobalPrevious] > @MeterIndexGlobalPrevious_FilterText)
						OR (@MeterIndexGlobalPrevious_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterIndexGlobalPrevious] >= @MeterIndexGlobalPrevious_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalGas_FilterText is null OR @ConsumptionGlobalGas_FilterOperator is null
				OR	(	@ConsumptionGlobalGas_FilterText is not null AND @ConsumptionGlobalGas_FilterOperator is not null
					AND	( (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalGas_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalGas] is null) OR (i.[ConsumptionGlobalGas] = @PowerCost_FilterText)))
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalGas] <> @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalGas] < @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalGas] <= @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalGas] > @ConsumptionGlobalGas_FilterText)
						OR (@ConsumptionGlobalGas_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalGas] >= @ConsumptionGlobalGas_FilterText)
					)
				)
			)
			AND	(	@PowerCost_FilterText is null OR @PowerCost_FilterOperator is null
				OR	(	@PowerCost_FilterText is not null AND @PowerCost_FilterOperator is not null
					AND	( (@PowerCost_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PowerCost_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PowerCost] is null) OR (i.[PowerCost] = @PowerCost_FilterText)))
						OR (@PowerCost_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PowerCost] <> @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsLessThan AND i.[PowerCost] < @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PowerCost] <= @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PowerCost] > @PowerCost_FilterText)
						OR (@PowerCost_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PowerCost] >= @PowerCost_FilterText)
					)
				)
			)
			AND	(	@ConsumptionPeakCostExcl_FilterText is null OR @ConsumptionPeakCostExcl_FilterOperator is null
				OR	(	@ConsumptionPeakCostExcl_FilterText is not null AND @ConsumptionPeakCostExcl_FilterOperator is not null
					AND	( (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionPeakCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionPeakCostExcl] is null) OR (i.[ConsumptionPeakCostExcl] = @ConsumptionPeakCostExcl_FilterText)))
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionPeakCostExcl] <> @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionPeakCostExcl] < @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionPeakCostExcl] <= @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionPeakCostExcl] > @ConsumptionPeakCostExcl_FilterText)
						OR (@ConsumptionPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionPeakCostExcl] >= @ConsumptionPeakCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionOffPeakCostExcl_FilterText is null OR @ConsumptionOffPeakCostExcl_FilterOperator is null
				OR	(	@ConsumptionOffPeakCostExcl_FilterText is not null AND @ConsumptionOffPeakCostExcl_FilterOperator is not null
					AND	( (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionOffPeakCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionOffPeakCostExcl] is null) OR (i.[ConsumptionOffPeakCostExcl] = @ConsumptionOffPeakCostExcl_FilterText)))
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionOffPeakCostExcl] <> @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionOffPeakCostExcl] < @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionOffPeakCostExcl] <= @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionOffPeakCostExcl] > @ConsumptionOffPeakCostExcl_FilterText)
						OR (@ConsumptionOffPeakCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionOffPeakCostExcl] >= @ConsumptionOffPeakCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionGlobalCostExcl_FilterText is null OR @ConsumptionGlobalCostExcl_FilterOperator is null
				OR	(	@ConsumptionGlobalCostExcl_FilterText is not null AND @ConsumptionGlobalCostExcl_FilterOperator is not null
					AND	( (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionGlobalCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionGlobalCostExcl] is null) OR (i.[ConsumptionGlobalCostExcl] = @ConsumptionGlobalCostExcl_FilterText)))
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionGlobalCostExcl] <> @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionGlobalCostExcl] < @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionGlobalCostExcl] <= @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionGlobalCostExcl] > @ConsumptionGlobalCostExcl_FilterText)
						OR (@ConsumptionGlobalCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionGlobalCostExcl] >= @ConsumptionGlobalCostExcl_FilterText)
					)
				)
			)
			AND	(	@ConsumptionTotalCostExcl_FilterText is null OR @ConsumptionTotalCostExcl_FilterOperator is null
				OR	(	@ConsumptionTotalCostExcl_FilterText is not null AND @ConsumptionTotalCostExcl_FilterOperator is not null
					AND	( (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@ConsumptionTotalCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[ConsumptionTotalCostExcl] is null) OR (i.[ConsumptionTotalCostExcl] = @ConsumptionTotalCostExcl_FilterText)))
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[ConsumptionTotalCostExcl] <> @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[ConsumptionTotalCostExcl] < @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[ConsumptionTotalCostExcl] <= @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[ConsumptionTotalCostExcl] > @ConsumptionTotalCostExcl_FilterText)
						OR (@ConsumptionTotalCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[ConsumptionTotalCostExcl] >= @ConsumptionTotalCostExcl_FilterText)
					)
				)
			)
			AND	(	@GuarantyOfOriginCostExcl_FilterText is null OR @GuarantyOfOriginCostExcl_FilterOperator is null
				OR	(	@GuarantyOfOriginCostExcl_FilterText is not null AND @GuarantyOfOriginCostExcl_FilterOperator is not null
					AND	( (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@GuarantyOfOriginCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[GuarantyOfOriginCostExcl] is null) OR (i.[GuarantyOfOriginCostExcl] = @GuarantyOfOriginCostExcl_FilterText)))
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[GuarantyOfOriginCostExcl] <> @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[GuarantyOfOriginCostExcl] < @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[GuarantyOfOriginCostExcl] <= @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[GuarantyOfOriginCostExcl] > @GuarantyOfOriginCostExcl_FilterText)
						OR (@GuarantyOfOriginCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[GuarantyOfOriginCostExcl] >= @GuarantyOfOriginCostExcl_FilterText)
					)
				)
			)
			AND	(	@RenewableEnergyContributionExcl_FilterText is null OR @RenewableEnergyContributionExcl_FilterOperator is null
				OR	(	@RenewableEnergyContributionExcl_FilterText is not null AND @RenewableEnergyContributionExcl_FilterOperator is not null
					AND	( (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@RenewableEnergyContributionExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[RenewableEnergyContributionExcl] is null) OR (i.[RenewableEnergyContributionExcl] = @RenewableEnergyContributionExcl_FilterText)))
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[RenewableEnergyContributionExcl] <> @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[RenewableEnergyContributionExcl] < @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[RenewableEnergyContributionExcl] <= @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[RenewableEnergyContributionExcl] > @RenewableEnergyContributionExcl_FilterText)
						OR (@RenewableEnergyContributionExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[RenewableEnergyContributionExcl] >= @RenewableEnergyContributionExcl_FilterText)
					)
				)
			)
			AND	(	@CogenContributionExcl_FilterText is null OR @CogenContributionExcl_FilterOperator is null
				OR	(	@CogenContributionExcl_FilterText is not null AND @CogenContributionExcl_FilterOperator is not null
					AND	( (@CogenContributionExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@CogenContributionExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[CogenContributionExcl] is null) OR (i.[CogenContributionExcl] = @CogenContributionExcl_FilterText)))
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[CogenContributionExcl] <> @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[CogenContributionExcl] < @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[CogenContributionExcl] <= @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[CogenContributionExcl] > @CogenContributionExcl_FilterText)
						OR (@CogenContributionExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[CogenContributionExcl] >= @CogenContributionExcl_FilterText)
					)
				)
			)
			AND	(	@MeterCost_FilterText is null OR @MeterCost_FilterOperator is null
				OR	(	@MeterCost_FilterText is not null AND @MeterCost_FilterOperator is not null
					AND	( (@MeterCost_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@MeterCost_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[MeterCost] is null) OR (i.[MeterCost] = @MeterCost_FilterText)))
						OR (@MeterCost_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[MeterCost] <> @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsLessThan AND i.[MeterCost] < @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[MeterCost] <= @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsGreaterThan AND i.[MeterCost] > @MeterCost_FilterText)
						OR (@MeterCost_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[MeterCost] >= @MeterCost_FilterText)
					)
				)
			)
			AND	(	@OtherEnergyCostExcl_FilterText is null OR @OtherEnergyCostExcl_FilterOperator is null
				OR	(	@OtherEnergyCostExcl_FilterText is not null AND @OtherEnergyCostExcl_FilterOperator is not null
					AND	( (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@OtherEnergyCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[OtherEnergyCostExcl] is null) OR (i.[OtherEnergyCostExcl] = @OtherEnergyCostExcl_FilterText)))
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[OtherEnergyCostExcl] <> @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[OtherEnergyCostExcl] < @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[OtherEnergyCostExcl] <= @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[OtherEnergyCostExcl] > @OtherEnergyCostExcl_FilterText)
						OR (@OtherEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[OtherEnergyCostExcl] >= @OtherEnergyCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalEnergyCostExcl_FilterText is null OR @TotalEnergyCostExcl_FilterOperator is null
				OR	(	@TotalEnergyCostExcl_FilterText is not null AND @TotalEnergyCostExcl_FilterOperator is not null
					AND	( (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalEnergyCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalEnergyCostExcl] is null) OR (i.[TotalEnergyCostExcl] = @TotalEnergyCostExcl_FilterText)))
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalEnergyCostExcl] <> @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalEnergyCostExcl] < @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalEnergyCostExcl] <= @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalEnergyCostExcl] > @TotalEnergyCostExcl_FilterText)
						OR (@TotalEnergyCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalEnergyCostExcl] >= @TotalEnergyCostExcl_FilterText)
					)
				)
			)
			AND	(	@DistributionCostExcl_FilterText is null OR @DistributionCostExcl_FilterOperator is null
				OR	(	@DistributionCostExcl_FilterText is not null AND @DistributionCostExcl_FilterOperator is not null
					AND	( (@DistributionCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@DistributionCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[DistributionCostExcl] is null) OR (i.[DistributionCostExcl] = @DistributionCostExcl_FilterText)))
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[DistributionCostExcl] <> @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[DistributionCostExcl] < @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[DistributionCostExcl] <= @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[DistributionCostExcl] > @DistributionCostExcl_FilterText)
						OR (@DistributionCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[DistributionCostExcl] >= @DistributionCostExcl_FilterText)
					)
				)
			)
			AND	(	@TransmitionCostExcl_FilterText is null OR @TransmitionCostExcl_FilterOperator is null
				OR	(	@TransmitionCostExcl_FilterText is not null AND @TransmitionCostExcl_FilterOperator is not null
					AND	( (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TransmitionCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TransmitionCostExcl] is null) OR (i.[TransmitionCostExcl] = @TransmitionCostExcl_FilterText)))
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TransmitionCostExcl] <> @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TransmitionCostExcl] < @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TransmitionCostExcl] <= @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TransmitionCostExcl] > @TransmitionCostExcl_FilterText)
						OR (@TransmitionCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TransmitionCostExcl] >= @TransmitionCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalNetCostExcl_FilterText is null OR @TotalNetCostExcl_FilterOperator is null
				OR	(	@TotalNetCostExcl_FilterText is not null AND @TotalNetCostExcl_FilterOperator is not null
					AND	( (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalNetCostExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalNetCostExcl] is null) OR (i.[TotalNetCostExcl] = @TotalNetCostExcl_FilterText)))
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalNetCostExcl] <> @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalNetCostExcl] < @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalNetCostExcl] <= @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalNetCostExcl] > @TotalNetCostExcl_FilterText)
						OR (@TotalNetCostExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalNetCostExcl] >= @TotalNetCostExcl_FilterText)
					)
				)
			)
			AND	(	@TotalTaxesExcl_FilterText is null OR @TotalTaxesExcl_FilterOperator is null
				OR	(	@TotalTaxesExcl_FilterText is not null AND @TotalTaxesExcl_FilterOperator is not null
					AND	( (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalTaxesExcl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalTaxesExcl] is null) OR (i.[TotalTaxesExcl] = @TotalTaxesExcl_FilterText)))
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalTaxesExcl] <> @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalTaxesExcl] < @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalTaxesExcl] <= @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalTaxesExcl] > @TotalTaxesExcl_FilterText)
						OR (@TotalTaxesExcl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalTaxesExcl] >= @TotalTaxesExcl_FilterText)
					)
				)
			)
			AND	(	@TotalCostExclVAT_FilterText is null OR @TotalCostExclVAT_FilterOperator is null
				OR	(	@TotalCostExclVAT_FilterText is not null AND @TotalCostExclVAT_FilterOperator is not null
					AND	( (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalCostExclVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalCostExclVAT] is null) OR (i.[TotalCostExclVAT] = @TotalCostExclVAT_FilterText)))
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalCostExclVAT] <> @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalCostExclVAT] < @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalCostExclVAT] <= @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalCostExclVAT] > @TotalCostExclVAT_FilterText)
						OR (@TotalCostExclVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalCostExclVAT] >= @TotalCostExclVAT_FilterText)
					)
				)
			)
			AND	(	@VATPercentage_FilterText is null OR @VATPercentage_FilterOperator is null
				OR	(	@VATPercentage_FilterText is not null AND @VATPercentage_FilterOperator is not null
					AND	( (@VATPercentage_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@VATPercentage_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[VATPercentage] is null) OR (i.[VATPercentage] = @VATPercentage_FilterText)))
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[VATPercentage] <> @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsLessThan AND i.[VATPercentage] < @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[VATPercentage] <= @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsGreaterThan AND i.[VATPercentage] > @VATPercentage_FilterText)
						OR (@VATPercentage_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[VATPercentage] >= @VATPercentage_FilterText)
					)
				)
			)
			AND	(	@TotalVATInvoice_FilterText is null OR @TotalVATInvoice_FilterOperator is null
				OR	(	@TotalVATInvoice_FilterText is not null AND @TotalVATInvoice_FilterOperator is not null
					AND	( (@TotalVATInvoice_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalVATInvoice_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalVATInvoice] is null) OR (i.[TotalVATInvoice] = @TotalVATInvoice_FilterText)))
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalVATInvoice] <> @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalVATInvoice] < @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalVATInvoice] <= @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalVATInvoice] > @TotalVATInvoice_FilterText)
						OR (@TotalVATInvoice_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalVATInvoice] >= @TotalVATInvoice_FilterText)
					)
				)
			)
			AND	(	@TotalVAT_FilterText is null OR @TotalVAT_FilterOperator is null
				OR	(	@TotalVAT_FilterText is not null AND @TotalVAT_FilterOperator is not null
					AND	( (@TotalVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalVAT] is null) OR (i.[TotalVAT] = @TotalVAT_FilterText)))
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalVAT] <> @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalVAT] < @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalVAT] <= @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalVAT] > @TotalVAT_FilterText)
						OR (@TotalVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalVAT] >= @TotalVAT_FilterText)
					)
				)
			)
			AND	(	@TotalCostInclVAT_FilterText is null OR @TotalCostInclVAT_FilterOperator is null
				OR	(	@TotalCostInclVAT_FilterText is not null AND @TotalCostInclVAT_FilterOperator is not null
					AND	( (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@TotalCostInclVAT_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[TotalCostInclVAT] is null) OR (i.[TotalCostInclVAT] = @TotalCostInclVAT_FilterText)))
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[TotalCostInclVAT] <> @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsLessThan AND i.[TotalCostInclVAT] < @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[TotalCostInclVAT] <= @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsGreaterThan AND i.[TotalCostInclVAT] > @TotalCostInclVAT_FilterText)
						OR (@TotalCostInclVAT_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[TotalCostInclVAT] >= @TotalCostInclVAT_FilterText)
					)
				)
			)
			AND	(	@VATFreeAmountInvoice_FilterText is null OR @VATFreeAmountInvoice_FilterOperator is null
				OR	(	@VATFreeAmountInvoice_FilterText is not null AND @VATFreeAmountInvoice_FilterOperator is not null
					AND	( (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@VATFreeAmountInvoice_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[VATFreeAmountInvoice] is null) OR (i.[VATFreeAmountInvoice] = @VATFreeAmountInvoice_FilterText)))
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[VATFreeAmountInvoice] <> @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsLessThan AND i.[VATFreeAmountInvoice] < @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[VATFreeAmountInvoice] <= @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsGreaterThan AND i.[VATFreeAmountInvoice] > @VATFreeAmountInvoice_FilterText)
						OR (@VATFreeAmountInvoice_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[VATFreeAmountInvoice] >= @VATFreeAmountInvoice_FilterText)
					)
				)
			)
			AND	(	@AmountDownPaymentsIncluding_FilterText is null OR @AmountDownPaymentsIncluding_FilterOperator is null
				OR	(	@AmountDownPaymentsIncluding_FilterText is not null AND @AmountDownPaymentsIncluding_FilterOperator is not null
					AND	( (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@AmountDownPaymentsIncluding_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[AmountDownPaymentsIncluding] is null) OR (i.[AmountDownPaymentsIncluding] = @AmountDownPaymentsIncluding_FilterText)))
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[AmountDownPaymentsIncluding] <> @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsLessThan AND i.[AmountDownPaymentsIncluding] < @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[AmountDownPaymentsIncluding] <= @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsGreaterThan AND i.[AmountDownPaymentsIncluding] > @AmountDownPaymentsIncluding_FilterText)
						OR (@AmountDownPaymentsIncluding_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[AmountDownPaymentsIncluding] >= @AmountDownPaymentsIncluding_FilterText)
					)
				)
			)
			AND	(	@PrepaidDownPaymentsIncl_FilterText is null OR @PrepaidDownPaymentsIncl_FilterOperator is null
				OR	(	@PrepaidDownPaymentsIncl_FilterText is not null AND @PrepaidDownPaymentsIncl_FilterOperator is not null
					AND	( (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@PrepaidDownPaymentsIncl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[PrepaidDownPaymentsIncl] is null) OR (i.[PrepaidDownPaymentsIncl] = @PrepaidDownPaymentsIncl_FilterText)))
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[PrepaidDownPaymentsIncl] <> @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThan AND i.[PrepaidDownPaymentsIncl] < @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[PrepaidDownPaymentsIncl] <= @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[PrepaidDownPaymentsIncl] > @PrepaidDownPaymentsIncl_FilterText)
						OR (@PrepaidDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[PrepaidDownPaymentsIncl] >= @PrepaidDownPaymentsIncl_FilterText)
					)
				)
			)
			AND	(	@NewPeriodicalDownPaymentsIncl_FilterText is null OR @NewPeriodicalDownPaymentsIncl_FilterOperator is null
				OR	(	@NewPeriodicalDownPaymentsIncl_FilterText is not null AND @NewPeriodicalDownPaymentsIncl_FilterOperator is not null
					AND	( (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@NewPeriodicalDownPaymentsIncl_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[NewPeriodicalDownPaymentsIncl] is null) OR (i.[NewPeriodicalDownPaymentsIncl] = @NewPeriodicalDownPaymentsIncl_FilterText)))
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[NewPeriodicalDownPaymentsIncl] <> @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThan AND i.[NewPeriodicalDownPaymentsIncl] < @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsLessThanOrEqualTo AND i.[NewPeriodicalDownPaymentsIncl] <= @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThan AND i.[NewPeriodicalDownPaymentsIncl] > @NewPeriodicalDownPaymentsIncl_FilterText)
						OR (@NewPeriodicalDownPaymentsIncl_FilterOperator = @FilterOperator_IsGreaterThanOrEqualTo AND i.[NewPeriodicalDownPaymentsIncl] >= @NewPeriodicalDownPaymentsIncl_FilterText)
					)
				)
			)
			AND	(	@eClick_FilterText is null OR @eClick_FilterOperator is null
				OR	(	@eClick_FilterText is not null AND @eClick_FilterOperator is not null
					AND	( (@eClick_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@eClick_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[eClick] is null) OR (i.[eClick] = @eClick_FilterText)))
						OR (@eClick_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[eClick] <> @eClick_FilterText)
						OR (@eClick_FilterOperator = @FilterOperator_StartsWith AND i.[eClick] like @eClick_FilterText + '%')
						OR (@eClick_FilterOperator = @FilterOperator_EndsWith AND i.[eClick] like '%' + @eClick_FilterText)
						OR (@eClick_FilterOperator = @FilterOperator_Contains AND i.[eClick] like '%' + @eClick_FilterText + '%')
						OR (@eClick_FilterOperator = @FilterOperator_ISContainedIn AND @eClick_FilterText like '%' + i.[eClick] + '%')
						OR (@eClick_FilterOperator = @FilterOperator_DoesNotContain AND i.[eClick] not like '%' + @eClick_FilterText + '%')
					)
				)
			)
			AND	(	@FileName_FilterText is null OR @FileName_FilterOperator is null
				OR	(	@FileName_FilterText is not null AND @FileName_FilterOperator is not null
					AND	( (@FileName_FilterOperator = @FilterOperator_IsEqualTo AND ((UPPER(@FileName_FilterText) in ('[NIL]','[NOTHING]','[NA]') AND i.[FileName] is null) OR (i.[FileName] = @FileName_FilterText)))
						OR (@FileName_FilterOperator = @FilterOperator_IsNotEqualTo AND i.[FileName] <> @FileName_FilterText)
						OR (@FileName_FilterOperator = @FilterOperator_StartsWith AND i.[FileName] like @FileName_FilterText + '%')
						OR (@FileName_FilterOperator = @FilterOperator_EndsWith AND i.[FileName] like '%' + @FileName_FilterText)
						OR (@FileName_FilterOperator = @FilterOperator_Contains AND i.[FileName] like '%' + @FileName_FilterText + '%')
						OR (@FileName_FilterOperator = @FilterOperator_ISContainedIn AND @FileName_FilterText like '%' + i.[FileName] + '%')
						OR (@FileName_FilterOperator = @FilterOperator_DoesNotContain AND i.[FileName] not like '%' + @FileName_FilterText + '%')
					)
				)
			)

	ORDER BY 
		 CASE WHEN @EANToFAVector_FA_SortDirection = @SortDirection_Descending THEN efv.[FA] END DESC
		,CASE WHEN @EANToFAVector_FA_SortDirection = @SortDirection_Ascending THEN efv.[FA] END -- ASC

		,CASE WHEN @EANToFAVector_Vector_SortDirection = @SortDirection_Descending THEN efv.[Vector] END DESC
		,CASE WHEN @EANToFAVector_Vector_SortDirection = @SortDirection_Ascending THEN efv.[Vector] END -- ASC
			
		,CASE WHEN @EANToFAVector_SubVector_SortDirection = @SortDirection_Descending THEN efv.[SubVector] END DESC
		,CASE WHEN @EANToFAVector_SubVector_SortDirection = @SortDirection_Ascending THEN efv.[SubVector] END -- ASC
				
		,CASE WHEN @EANToFAVector_State_SortDirection = @SortDirection_Descending THEN efv.[State] END DESC
		,CASE WHEN @EANToFAVector_State_SortDirection = @SortDirection_Ascending THEN efv.[State] END -- ASC
					
		,CASE WHEN @EANToFAVector_ClosedOn_SortDirection = @SortDirection_Descending THEN efv.[ClosedOn] END DESC
		,CASE WHEN @EANToFAVector_ClosedOn_SortDirection = @SortDirection_Ascending THEN efv.[ClosedOn] END -- ASC
					
		,CASE WHEN @EAN_SortDirection = @SortDirection_Descending THEN i.[EAN] END DESC
		,CASE WHEN @EAN_SortDirection = @SortDirection_Ascending THEN i.[EAN] END -- ASC

		,CASE WHEN @DeliveryAddress_Street_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_Street] END DESC
		,CASE WHEN @DeliveryAddress_Street_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_Street] END -- ASC

		,CASE WHEN @DeliveryAddress_PostalCode_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_PostalCode] END DESC
		,CASE WHEN @DeliveryAddress_PostalCode_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_PostalCode] END -- ASC

		,CASE WHEN @DeliveryAddress_City_SortDirection = @SortDirection_Descending THEN i.[DeliveryAddress_City] END DESC
		,CASE WHEN @DeliveryAddress_City_SortDirection = @SortDirection_Ascending THEN i.[DeliveryAddress_City] END -- ASC

		,CASE WHEN @MeterNumber_SortDirection = @SortDirection_Descending THEN i.[MeterNumber] END DESC
		,CASE WHEN @MeterNumber_SortDirection = @SortDirection_Ascending THEN i.[MeterNumber] END -- ASC

		,CASE WHEN @Provider_SortDirection = @SortDirection_Descending THEN i.[Provider] END DESC
		,CASE WHEN @Provider_SortDirection = @SortDirection_Ascending THEN i.[Provider] END -- ASC

		,CASE WHEN @InvoiceNumber_SortDirection = @SortDirection_Descending THEN i.[InvoiceNumber] END DESC
		,CASE WHEN @InvoiceNumber_SortDirection = @SortDirection_Ascending THEN i.[InvoiceNumber] END -- ASC

		,CASE WHEN @InvoiceType_SortDirection = @SortDirection_Descending THEN i.[InvoiceType] END DESC
		,CASE WHEN @InvoiceType_SortDirection = @SortDirection_Ascending THEN i.[InvoiceType] END -- ASC

		,CASE WHEN @InvoiceSubType_SortDirection = @SortDirection_Descending THEN i.[InvoiceSubType] END DESC
		,CASE WHEN @InvoiceSubType_SortDirection = @SortDirection_Ascending THEN i.[InvoiceSubType] END -- ASC

		,CASE WHEN @InvoiceReference_SortDirection = @SortDirection_Descending THEN i.[InvoiceReference] END DESC
		,CASE WHEN @InvoiceReference_SortDirection = @SortDirection_Ascending THEN i.[InvoiceReference] END -- ASC

		,CASE WHEN @InvoiceCategory_SortDirection = @SortDirection_Descending THEN i.[InvoiceCategory] END DESC
		,CASE WHEN @InvoiceCategory_SortDirection = @SortDirection_Ascending THEN i.[InvoiceCategory] END -- ASC

		,CASE WHEN @ClientNumber_SortDirection = @SortDirection_Descending THEN i.[ClientNumber] END DESC
		,CASE WHEN @ClientNumber_SortDirection = @SortDirection_Ascending THEN i.[ClientNumber] END -- ASC

		,CASE WHEN @ClientName_SortDirection = @SortDirection_Descending THEN i.[ClientName] END DESC
		,CASE WHEN @ClientName_SortDirection = @SortDirection_Ascending THEN i.[ClientName] END -- ASC

		,CASE WHEN @BuildingAddress_Name_SortDirection = @SortDirection_Descending THEN ba.[Name] END DESC
		,CASE WHEN @BuildingAddress_Name_SortDirection = @SortDirection_Ascending THEN ba.[Name] END -- ASC
		
		,CASE WHEN @BuildingAddress_Street_SortDirection = @SortDirection_Descending THEN ba.[Street] END DESC
		,CASE WHEN @BuildingAddress_Street_SortDirection = @SortDirection_Ascending THEN ba.[Street] END -- ASC
		
		,CASE WHEN @BuildingAddress_PostalCode_SortDirection = @SortDirection_Descending THEN ba.[PostalCode] END DESC
		,CASE WHEN @BuildingAddress_PostalCode_SortDirection = @SortDirection_Ascending THEN ba.[PostalCode] END -- ASC
		
		,CASE WHEN @BuildingAddress_City_SortDirection = @SortDirection_Descending THEN ba.[City] END DESC
		,CASE WHEN @BuildingAddress_City_SortDirection = @SortDirection_Ascending THEN ba.[City] END -- ASC

		,CASE WHEN @PeriodFrom_SortDirection = @SortDirection_Descending THEN i.[PeriodFrom] END DESC
		,CASE WHEN @PeriodFrom_SortDirection = @SortDirection_Ascending THEN i.[PeriodFrom] END -- ASC

		,CASE WHEN @PeriodTo_SortDirection = @SortDirection_Descending THEN i.[PeriodTo] END DESC
		,CASE WHEN @PeriodTo_SortDirection = @SortDirection_Ascending THEN i.[PeriodTo] END -- ASC

		,CASE WHEN @PeriodLength_SortDirection = @SortDirection_Descending THEN i.[PeriodLength] END DESC
		,CASE WHEN @PeriodLength_SortDirection = @SortDirection_Ascending THEN i.[PeriodLength] END -- ASC

		,CASE WHEN @InvoiceDate_SortDirection = @SortDirection_Descending THEN i.[InvoiceDate] END DESC
		,CASE WHEN @InvoiceDate_SortDirection = @SortDirection_Ascending THEN i.[InvoiceDate] END -- ASC

		,CASE WHEN @MaxPowerPeak_SortDirection = @SortDirection_Descending THEN i.[MaxPowerPeak] END DESC
		,CASE WHEN @MaxPowerPeak_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerPeak] END -- ASC

		,CASE WHEN @MaxPowerOffPeak_SortDirection = @SortDirection_Descending THEN i.[MaxPowerOffPeak] END DESC
		,CASE WHEN @MaxPowerOffPeak_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerOffPeak] END -- ASC

		,CASE WHEN @MaxPowerGlobal_SortDirection = @SortDirection_Descending THEN i.[MaxPowerGlobal] END DESC
		,CASE WHEN @MaxPowerGlobal_SortDirection = @SortDirection_Ascending THEN i.[MaxPowerGlobal] END -- ASC

		,CASE WHEN @ConsumptionPeak_SortDirection = @SortDirection_Descending THEN i.[ConsumptionPeak] END DESC
		,CASE WHEN @ConsumptionPeak_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionPeak] END -- ASC

		,CASE WHEN @ConsumptionOffPeak_SortDirection = @SortDirection_Descending THEN i.[ConsumptionOffPeak] END DESC
		,CASE WHEN @ConsumptionOffPeak_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionOffPeak] END -- ASC

		,CASE WHEN @ConsumptionGlobalElectric_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalElectric] END DESC
		,CASE WHEN @ConsumptionGlobalElectric_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalElectric] END -- ASC

		,CASE WHEN @ConsumptionTotal_SortDirection = @SortDirection_Descending THEN i.[ConsumptionTotal] END DESC
		,CASE WHEN @ConsumptionTotal_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionTotal] END -- ASC

		,CASE WHEN @CaloricValue_SortDirection = @SortDirection_Descending THEN i.[CaloricValue] END DESC
		,CASE WHEN @CaloricValue_SortDirection = @SortDirection_Ascending THEN i.[CaloricValue] END -- ASC

		,CASE WHEN @CosPhi_SortDirection = @SortDirection_Descending THEN i.[CosPhi] END DESC
		,CASE WHEN @CosPhi_SortDirection = @SortDirection_Ascending THEN i.[CosPhi] END -- ASC

		,CASE WHEN @MeterIndexPeak_SortDirection = @SortDirection_Descending THEN i.[MeterIndexPeak] END DESC
		,CASE WHEN @MeterIndexPeak_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexPeak] END -- ASC

		,CASE WHEN @MeterIndexOffPeak_SortDirection = @SortDirection_Descending THEN i.[MeterIndexOffPeak] END DESC
		,CASE WHEN @MeterIndexOffPeak_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexOffPeak] END -- ASC

		,CASE WHEN @MeterIndexPeakPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexPeakPrevious] END DESC
		,CASE WHEN @MeterIndexPeakPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexPeakPrevious] END -- ASC

		,CASE WHEN @MeterIndexOffPeakPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexOffPeakPrevious] END DESC
		,CASE WHEN @MeterIndexOffPeakPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexOffPeakPrevious] END -- ASC

		,CASE WHEN @MeterIndexGlobal_SortDirection = @SortDirection_Descending THEN i.[MeterIndexGlobal] END DESC
		,CASE WHEN @MeterIndexGlobal_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexGlobal] END -- ASC

		,CASE WHEN @MeterIndexGlobalPrevious_SortDirection = @SortDirection_Descending THEN i.[MeterIndexGlobalPrevious] END DESC
		,CASE WHEN @MeterIndexGlobalPrevious_SortDirection = @SortDirection_Ascending THEN i.[MeterIndexGlobalPrevious] END -- ASC

		,CASE WHEN @ConsumptionGlobalGas_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalGas] END DESC
		,CASE WHEN @ConsumptionGlobalGas_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalGas] END -- ASC
		
		,CASE WHEN @PowerCost_SortDirection = @SortDirection_Descending THEN i.[PowerCost] END DESC
		,CASE WHEN @PowerCost_SortDirection = @SortDirection_Ascending THEN i.[PowerCost] END -- ASC

		,CASE WHEN @ConsumptionPeakCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionPeakCostExcl] END DESC
		,CASE WHEN @ConsumptionPeakCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionPeakCostExcl] END -- ASC

		,CASE WHEN @ConsumptionOffPeakCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionOffPeakCostExcl] END DESC
		,CASE WHEN @ConsumptionOffPeakCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionOffPeakCostExcl] END -- ASC

		,CASE WHEN @ConsumptionGlobalCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionGlobalCostExcl] END DESC
		,CASE WHEN @ConsumptionGlobalCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionGlobalCostExcl] END -- ASC

		,CASE WHEN @ConsumptionTotalCostExcl_SortDirection = @SortDirection_Descending THEN i.[ConsumptionTotalCostExcl] END DESC
		,CASE WHEN @ConsumptionTotalCostExcl_SortDirection = @SortDirection_Ascending THEN i.[ConsumptionTotalCostExcl] END -- ASC

		,CASE WHEN @GuarantyOfOriginCostExcl_SortDirection = @SortDirection_Descending THEN i.[GuarantyOfOriginCostExcl] END DESC
		,CASE WHEN @GuarantyOfOriginCostExcl_SortDirection = @SortDirection_Ascending THEN i.[GuarantyOfOriginCostExcl] END -- ASC

		,CASE WHEN @RenewableEnergyContributionExcl_SortDirection = @SortDirection_Descending THEN i.[RenewableEnergyContributionExcl] END DESC
		,CASE WHEN @RenewableEnergyContributionExcl_SortDirection = @SortDirection_Ascending THEN i.[RenewableEnergyContributionExcl] END -- ASC

		,CASE WHEN @CogenContributionExcl_SortDirection = @SortDirection_Descending THEN i.[CogenContributionExcl] END DESC
		,CASE WHEN @CogenContributionExcl_SortDirection = @SortDirection_Ascending THEN i.[CogenContributionExcl] END -- ASC

		,CASE WHEN @OtherEnergyCostExcl_SortDirection = @SortDirection_Descending THEN i.[OtherEnergyCostExcl] END DESC
		,CASE WHEN @OtherEnergyCostExcl_SortDirection = @SortDirection_Ascending THEN i.[OtherEnergyCostExcl] END -- ASC

		,CASE WHEN @TotalEnergyCostExcl_SortDirection = @SortDirection_Descending THEN i.[TotalEnergyCostExcl] END DESC
		,CASE WHEN @TotalEnergyCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalEnergyCostExcl] END -- ASC

		,CASE WHEN @DistributionCostExcl_SortDirection = @SortDirection_Descending THEN i.[DistributionCostExcl] END DESC
		,CASE WHEN @DistributionCostExcl_SortDirection = @SortDirection_Ascending THEN i.[DistributionCostExcl] END -- ASC

		,CASE WHEN @TransmitionCostExcl_SortDirection = @SortDirection_Descending THEN i.[TransmitionCostExcl] END DESC
		,CASE WHEN @TransmitionCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TransmitionCostExcl] END -- ASC

		,CASE WHEN @TotalNetCostExcl_SortDirection = @SortDirection_Descending THEN i.[TotalNetCostExcl] END DESC
		,CASE WHEN @TotalNetCostExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalNetCostExcl] END -- ASC

		,CASE WHEN @TotalTaxesExcl_SortDirection = @SortDirection_Descending THEN i.[TotalTaxesExcl] END DESC
		,CASE WHEN @TotalTaxesExcl_SortDirection = @SortDirection_Ascending THEN i.[TotalTaxesExcl] END -- ASC

		,CASE WHEN @TotalCostExclVAT_SortDirection = @SortDirection_Descending THEN i.[TotalCostExclVAT] END DESC
		,CASE WHEN @TotalCostExclVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalCostExclVAT] END -- ASC

		,CASE WHEN @VATPercentage_SortDirection = @SortDirection_Descending THEN i.[VATPercentage] END DESC
		,CASE WHEN @VATPercentage_SortDirection = @SortDirection_Ascending THEN i.[VATPercentage] END -- ASC

		,CASE WHEN @TotalVATInvoice_SortDirection = @SortDirection_Descending THEN i.[TotalVATInvoice] END DESC
		,CASE WHEN @TotalVATInvoice_SortDirection = @SortDirection_Ascending THEN i.[TotalVATInvoice] END -- ASC

   		,CASE WHEN @TotalVAT_SortDirection = @SortDirection_Descending THEN i.[TotalVAT] END DESC
		,CASE WHEN @TotalVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalVAT] END -- ASC

		,CASE WHEN @TotalCostInclVAT_SortDirection = @SortDirection_Descending THEN i.[TotalCostInclVAT] END DESC
		,CASE WHEN @TotalCostInclVAT_SortDirection = @SortDirection_Ascending THEN i.[TotalCostInclVAT] END -- ASC

		,CASE WHEN @VATFreeAmountInvoice_SortDirection = @SortDirection_Descending THEN i.[VATFreeAmountInvoice] END DESC
		,CASE WHEN @VATFreeAmountInvoice_SortDirection = @SortDirection_Ascending THEN i.[VATFreeAmountInvoice] END -- ASC

		,CASE WHEN @AmountDownPaymentsIncluding_SortDirection = @SortDirection_Descending THEN i.[AmountDownPaymentsIncluding] END DESC
		,CASE WHEN @AmountDownPaymentsIncluding_SortDirection = @SortDirection_Ascending THEN i.[AmountDownPaymentsIncluding] END -- ASC

		,CASE WHEN @PrepaidDownPaymentsIncl_SortDirection = @SortDirection_Descending THEN i.[PrepaidDownPaymentsIncl] END DESC
		,CASE WHEN @PrepaidDownPaymentsIncl_SortDirection = @SortDirection_Ascending THEN i.[PrepaidDownPaymentsIncl] END -- ASC

		,CASE WHEN @NewPeriodicalDownPaymentsIncl_SortDirection = @SortDirection_Descending THEN i.[NewPeriodicalDownPaymentsIncl] END DESC
		,CASE WHEN @NewPeriodicalDownPaymentsIncl_SortDirection = @SortDirection_Ascending THEN i.[NewPeriodicalDownPaymentsIncl] END -- ASC

		,CASE WHEN @eClick_SortDirection = @SortDirection_Descending THEN i.[eClick] END DESC
		,CASE WHEN @eClick_SortDirection = @SortDirection_Ascending THEN i.[eClick] END -- ASC

		,CASE WHEN @FileName_SortDirection = @SortDirection_Descending THEN i.[FileName] END DESC
		,CASE WHEN @FileName_SortDirection = @SortDirection_Ascending THEN i.[FileName] END -- ASC
	END
END
GO