﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[prc2]
								AS
								BEGIN 
									--  Test On IS_ROLEMEMBER
									IF IS_ROLEMEMBER ('db_datareader') = 1
									print 'Current user is a member of the db_datareader role'
									ELSE IF IS_ROLEMEMBER ('db_datareader') = 0
									print 'Current user is NOT a member of the db_datareader role'
									--  Test On NEXT VALUE FOR 
									DECLARE @myvar3 int ;
									SET @myvar3 = NEXT VALUE FOR Test.CountBy1 ;
									SELECT @myvar3 = NEXT VALUE FOR Test.CountBy1 ;

									--  Test On THROW
									BEGIN TRY
										INSERT table_for_program(DepartmentID) VALUES(1);
									END TRY
									BEGIN CATCH
										THROW;
									END CATCH;

									--  Test On SCHEMA_ID
									SELECT SCHEMA_ID();

									--  Test On DATABASE_PRINCIPAL_ID
									SELECT DATABASE_PRINCIPAL_ID();

									--  Test On OFFSET и FETCH
									DECLARE @StartingRowNumber tinyint = 1;
									SELECT DepartmentID, Name, GroupName
									FROM table_for_program
									ORDER BY DepartmentID ASC 
										OFFSET @StartingRowNumber ROWS 
										FETCH NEXT (SELECT PageSize FROM dbo.AppSettings WHERE AppSettingID = 1) ROWS ONLY;

									-- Новый Order By
									SELECT DepartmentID, Name, GroupName
									FROM table_for_program
									ORDER BY DepartmentID
										OFFSET 3 ROWS
										FETCH NEXT 1 ROWS ONLY;
							END
GO