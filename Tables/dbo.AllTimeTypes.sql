﻿CREATE TABLE [dbo].[AllTimeTypes] (
  [pk_int] [int] NOT NULL,
  [datetime] [datetime] NULL,
  [smalldatetime] [smalldatetime] NULL,
  [datetime2] [datetime2] NULL,
  [datetimeoffset] [datetimeoffset] NULL,
  [date] [date] NULL,
  [time] [time] NULL,
  PRIMARY KEY CLUSTERED ([pk_int])
)
ON [PRIMARY]
GO