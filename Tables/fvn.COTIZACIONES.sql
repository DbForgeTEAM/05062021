﻿CREATE TABLE [fvn].[COTIZACIONES] (
  [FechaDatos] [date] NOT NULL DEFAULT ('1901-01-01'),
  [CodigoISIN] [char](12) NOT NULL DEFAULT (''),
  [Cotizacion] [numeric](17, 11) NOT NULL DEFAULT (0),
  [Usuario] [char](10) NOT NULL DEFAULT (''),
  CONSTRAINT [PK_COTIZACIONES] PRIMARY KEY NONCLUSTERED ([FechaDatos], [CodigoISIN])
)
ON [PRIMARY]
GO