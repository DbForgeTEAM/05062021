﻿CREATE TABLE [dbo].[Employee] (
  [BusinessEntityID] [int] IDENTITY,
  [BirthDate] [date] NOT NULL,
  [ModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_Employee_ModifiedDate] DEFAULT (getdate()),
  CONSTRAINT [PK_Employee_BusinessEntityID] PRIMARY KEY NONCLUSTERED ([BusinessEntityID]) WITH (FILLFACTOR = 1),
  CONSTRAINT [CK_Employee_BirthDate] CHECK ([BirthDate]>='1930-02-01' AND [BirthDate]<=dateadd(year,(-18),getdate()))
)
ON [PRIMARY]
GO