﻿CREATE TABLE [fvn].[VAN_MASTER] (
  [SkValor] [int] IDENTITY,
  [FechaDatos] [date] NOT NULL,
  [StatusMaster] [varchar](15) NOT NULL DEFAULT ('CALCULAR'),
  [Anulado] [bit] NOT NULL DEFAULT (0),
  [IDDeclaracion] [int] NOT NULL DEFAULT (0),
  [SECDeclaracion] [int] NOT NULL DEFAULT (0),
  [Orden] [int] NOT NULL DEFAULT (0),
  [IDRespuesta] [int] NOT NULL DEFAULT (0),
  [LiteralRespuesta] [varchar](200) NOT NULL DEFAULT (''),
  [IdDeclarante] [smallint] NOT NULL DEFAULT (0),
  [ClaseDeclaracion] [varchar](2) NOT NULL DEFAULT (''),
  [ClaseInstrumento] [smallint] NOT NULL DEFAULT (0),
  [NIFEmisor] [varchar](9) NOT NULL DEFAULT (''),
  [CodigoISIN] [varchar](12) NOT NULL DEFAULT (''),
  [Epigrafe] [smallint] NOT NULL DEFAULT (0),
  [TitularMaster] [varchar](9) NOT NULL DEFAULT (''),
  [PaisContrapartida] [varchar](2) NOT NULL DEFAULT (''),
  [NombreISIN] [varchar](50) NOT NULL DEFAULT (''),
  [NumeroSaldoInicial] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroEntradas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroEntradasSinPrecio] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSalidas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSalidasSinPrecio] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSaldoFinal] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSaldoInicial] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteEntradas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSalidas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSaldoFinal] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroCupon] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteCupon] [numeric](18, 2) NOT NULL DEFAULT (0),
  CONSTRAINT [PK_VAN_MASTER] PRIMARY KEY NONCLUSTERED ([SkValor])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [CLAVE]
  ON [fvn].[VAN_MASTER] ([FechaDatos], [IdDeclarante], [ClaseDeclaracion], [ClaseInstrumento], [NIFEmisor], [CodigoISIN], [Epigrafe], [TitularMaster], [PaisContrapartida])
  ON [PRIMARY]
GO

CREATE INDEX [ID_DECLARACION]
  ON [fvn].[VAN_MASTER] ([IDDeclaracion], [SECDeclaracion])
  ON [PRIMARY]
GO

CREATE INDEX [ID_RESPUESTA]
  ON [fvn].[VAN_MASTER] ([IDRespuesta])
  ON [PRIMARY]
GO

CREATE INDEX [STATUS]
  ON [fvn].[VAN_MASTER] ([FechaDatos], [IdDeclarante], [StatusMaster])
  ON [PRIMARY]
GO