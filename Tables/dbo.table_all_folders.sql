﻿CREATE TABLE [dbo].[table_all_folders] (
  [column1] [int] IDENTITY,
  [column2] [varchar](50) NULL,
  [column3] [varchar](50) NULL,
  [column4] [varchar](50) NULL,
  CONSTRAINT [PK_table_all_folders_column1] PRIMARY KEY CLUSTERED ([column1]),
  CONSTRAINT [CK_table_all_folders1] CHECK ((1)>(1)),
  CONSTRAINT [CK_table_all_folders2] CHECK ((2)=(2))
)
ON [PRIMARY]
GO

CREATE INDEX [IDX_table_all_folders_column2]
  ON [dbo].[table_all_folders] ([column2])
  ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[trigger1]
ON [dbo].[table_all_folders]
AFTER INSERT
AS RAISERROR ('Notify Customer Relations', 16, 10);
GO