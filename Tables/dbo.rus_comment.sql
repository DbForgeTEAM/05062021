﻿CREATE TABLE [dbo].[rus_comment] (
  [id] [int] IDENTITY,
  [column2] [varchar](50) COLLATE Latin1_General_100_CI_AI NULL,
  CONSTRAINT [PK_rus_comment_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'Тест', 'SCHEMA', N'dbo', 'TABLE', N'rus_comment', 'COLUMN', N'id'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', N'тест ', 'SCHEMA', N'dbo', 'TABLE', N'rus_comment', 'COLUMN', N'column2'
GO