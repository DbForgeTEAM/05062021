﻿CREATE TABLE [dbo].[t_parent1] (
  [id] [int] IDENTITY,
  [column2] [varchar](50) NULL,
  CONSTRAINT [PK_t_parent_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO