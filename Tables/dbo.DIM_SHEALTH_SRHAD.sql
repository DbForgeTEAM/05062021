﻿CREATE TABLE [dbo].[DIM_SHEALTH_SRHAD] (
  [SHEALTH_SRHAD_SKEY] [int] IDENTITY,
  [SHEALTH_SRHAD_BKEY] [varchar](255) COLLATE Latin1_General_CI_AS NOT NULL,
  [INITIAL_CONTACT_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [INITIAL_CONTACT_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [INITIAL_CONTACT_NATIONAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_STATUS_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_STATUS_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_STATUS_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_MAIN_METHOD_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_MAIN_METHOD_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_MAIN_METHOD_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_1_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_1_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_1_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_2_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_2_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_OTHER_METHOD_2_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_1_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_2_LOCAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_1_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_1_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_1_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_2_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_2_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_2_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_3_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_3_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_3_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_4_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_4_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_4_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_5_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_5_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_5_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_6_LOCAL_DESC] [varchar](100) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_6_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [SRH_CARE_ACTIVITY_6_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [LOCATION_TYPE_DESC] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [LOCATION_TYPE_NATIONAL_CODE] [varchar](10) COLLATE Latin1_General_CI_AS NOT NULL,
  [LOCATION_TYPE_NATIONAL_DESC] [varchar](500) COLLATE Latin1_General_CI_AS NOT NULL,
  [CREATED_BY_STAFF_USER_NAME] [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
  [CREATED_BY_STAFF_FULL_NAME] [varchar](80) COLLATE Latin1_General_CI_AS NOT NULL,
  [CREATED_BY_STAFF_TYPE] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [AMENDED_BY_STAFF_USER_NAME] [varchar](20) COLLATE Latin1_General_CI_AS NOT NULL,
  [AMENDED_BY_STAFF_FULL_NAME] [varchar](80) COLLATE Latin1_General_CI_AS NOT NULL,
  [AMENDED_BY_STAFF_TYPE] [varchar](50) COLLATE Latin1_General_CI_AS NOT NULL,
  [DATA_FLOW_ID] [int] NOT NULL CONSTRAINT [DF_SHEALTH_SRHAD_DATA_FLOW_ID] DEFAULT (0),
  [INSERT_DTTM] [datetime] NOT NULL CONSTRAINT [DF_SHEALTH_SRHAD_INSERT_DTTM] DEFAULT (getdate()),
  [ORDERING] [int] NOT NULL CONSTRAINT [DF_SHEALTH_SRHAD_ORDERING] DEFAULT (9999),
  [SCD_TYPE1_HASH] AS (CONVERT([varchar](255),hashbytes('MD5',(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([INITIAL_CONTACT_LOCAL_DESC])),(0)),N''))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([INITIAL_CONTACT_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([INITIAL_CONTACT_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_STATUS_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_STATUS_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_STATUS_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_MAIN_METHOD_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_MAIN_METHOD_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_MAIN_METHOD_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_1_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_1_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_1_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_2_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_2_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_OTHER_METHOD_2_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_1_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_2_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_1_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_1_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_1_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_2_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_2_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_2_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_3_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_3_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_3_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_4_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_4_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_4_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_5_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_5_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_5_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_6_LOCAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_6_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([SRH_CARE_ACTIVITY_6_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([LOCATION_TYPE_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([LOCATION_TYPE_NATIONAL_CODE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([LOCATION_TYPE_NATIONAL_DESC])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CREATED_BY_STAFF_USER_NAME])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CREATED_BY_STAFF_FULL_NAME])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([CREATED_BY_STAFF_TYPE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([AMENDED_BY_STAFF_USER_NAME])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([AMENDED_BY_STAFF_FULL_NAME])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([AMENDED_BY_STAFF_TYPE])),(0)),N'')))+'~')+upper(coalesce(CONVERT([nvarchar](255),ltrim(rtrim([ORDERING])),(0)),N''))),(0))),
  CONSTRAINT [PK_SHEALTH_SRHAD] PRIMARY KEY CLUSTERED ([SHEALTH_SRHAD_SKEY])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'DeleteCount', '38610', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD'
GO

EXEC sys.sp_addextendedproperty N'InsertCount', '38633', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD'
GO

EXEC sys.sp_addextendedproperty N'LastUpdateDateTime', '31/01/2017 09:26', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Data items comprising the Sexual and Reproductive Health Activity Dataset (SRHADV2) which are not made available in the Shealth Appointments Fact table.', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD'
GO

EXEC sys.sp_addextendedproperty N'UpdateCount', '0', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Surrogate primary key', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SHEALTH_SRHAD_SKEY'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Surrogate primary key', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SHEALTH_SRHAD_SKEY'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Business key from source system (aka natural key)', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SHEALTH_SRHAD_BKEY'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Business key from source system (aka natural key)', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SHEALTH_SRHAD_BKEY'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Is this the patients first ever contact with this service?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INITIAL_CONTACT_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Type of Contraception Consultation local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Type of Contraception Consultation local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Type of Contraception Consultation national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Type of Contraception Consultation national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Type of Contraception Consultation national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Type of Contraception Consultation national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_STATUS_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Main method of contraception of the patient local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Main method of contraception of the patient local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Main method of contraception of the patient national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Main method of contraception of the patient national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Main method of contraception of the patient national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Main method of contraception of the patient national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_MAIN_METHOD_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 1 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 1 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 2 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 2 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Other method of contraception of the patient 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Other method of contraception of the patient 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_OTHER_METHOD_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 1 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 1 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 2 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 2 local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Emergency Contraception Method 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Emergency Contraception Method 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CONTRACEPTION_METHOD_POST_COITAL_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 1 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 1 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 1 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 1 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_1_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 2 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 2 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 2 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 2 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_2_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 3 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 3 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 3 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 3 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 3 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 3 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_3_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 4 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 4 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 4 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 4 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 4 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 4 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_4_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 5 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 5 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 5 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 5 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 5 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 5 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_5_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 6 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 6 description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_LOCAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 6 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 6 national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Sexual & Reproductive Health Care Activity received by patient 6 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Sexual & Reproductive Health Care Activity received by patient 6 national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'SRH_CARE_ACTIVITY_6_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Where the patient consultation/treatment took place local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Where the patient consultation/treatment took place local description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Where the patient consultation/treatment took place national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Where the patient consultation/treatment took place national code', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_NATIONAL_CODE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Where the patient consultation/treatment took place national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Where the patient consultation/treatment took place national description', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'LOCATION_TYPE_NATIONAL_DESC'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'User the record was created by', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_USER_NAME'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'User the record was created by', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_USER_NAME'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'The name of the user creating the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_FULL_NAME'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'The name of the user creating the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_FULL_NAME'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Job Role of the user creating the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_TYPE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Job Role of the user creating the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'CREATED_BY_STAFF_TYPE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'User the record was last amended by', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_USER_NAME'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'User the record was last amended by', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_USER_NAME'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'The name of the user last amending the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_FULL_NAME'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'The name of the user last amending the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_FULL_NAME'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Job Role of the user last amending the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_TYPE'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Job Role of the user last amending the record', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'AMENDED_BY_STAFF_TYPE'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'What process loaded this row?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'DATA_FLOW_ID'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'What process loaded this row?', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'DATA_FLOW_ID'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Row inserted date time', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INSERT_DTTM'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Row inserted date time', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'INSERT_DTTM'
GO

EXEC sys.sp_addextendedproperty N'ColumnDescription', 'Custom ordering of rows for reporting if not desirable to order by an attribute', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'ORDERING'
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Custom ordering of rows for reporting if not desirable to order by an attribute', 'SCHEMA', N'dbo', 'TABLE', N'DIM_SHEALTH_SRHAD', 'COLUMN', N'ORDERING'
GO