﻿CREATE TABLE [dbo].[datetime_table_copy] (
  [id] [int] IDENTITY,
  [c_smalldatetime] [smalldatetime] NULL,
  [c_datetime] [datetime] NULL,
  [c_datetime2_0] [datetime2](0) NULL,
  [c_datetime2_3] [datetime2](3) NULL,
  [c_datetime2_7] [datetime2] NULL,
  [c_time] [time] NULL,
  [c_time_0] [time](0) NULL,
  [c_time_3] [time](3) NULL,
  [c_time_7] [time] NULL,
  CONSTRAINT [PK_datetime_table_copy_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO