﻿CREATE TABLE [dbo].[all_date_types_] (
  [column1] [date] NULL,
  [column2] [datetime2] NULL,
  [column3] [datetime] NULL,
  [column4] [datetimeoffset] NULL,
  [column5] [time] NULL,
  [column6] [smalldatetime] NULL
)
ON [PRIMARY]
GO